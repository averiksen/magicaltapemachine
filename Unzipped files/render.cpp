/*
This is the code for a project done by Anders Vedel Eriksen, s194017, with the
purpose of creating a digital delay device inspired by vintage analog tape delay machines.
The code is supposed to be uploaded on a Bela development board.
Project was done under Institute of Health Tech, supervised by Ph.D Niels Overby
and Associate professor Bastian Epp.

16th of July, 2022
*/


#include <Bela.h>
#include <libraries/AudioFile/AudioFile.h>
#include <cmath>

//I/O
int device_on;
int lock[3] = {1, 1, 1};			//For locking the change of values on knobs.

int debouncer;						//Debouncing of button presses.
int on_state;
int on_prev_state;

int shift;							//For engaging secondary control options.
int shift_state;
int shift_prev_state;

//To remember previous positions of turn knobs
float saved_readings[2][3] = {{0.5, 0.5, 0.5},{0.5, 0.5, 0.5}};

//Delayline
float in[2];						//Audio input and output.
float out[2];

#define delay_buf_size (2*44100)	//2 seconds of delay buffer.
float delay_buf[2][delay_buf_size]; // Stereo delay buffer.
float delay_read_ptr;
int delay_write_ptr;
float delay_s;						//actual delay in samples.
float delay_desired;				//desired delay in samples.
float timeAlpha = 0.0001;			//parameter for smooth transition between delay times.
float delay_desired_vib;
float feedback_gain;				//Amount of feedback in delay line.
float drywetmix;					//Dry/wet mixing parameters.
float drymix;

float eta;							//value for use in interpolation.
float delay_in[2];					//input to delay line coming from feedback.
		
//First order lowpass filter
float lpf_out[2];
float lpf_in[2];
float lpf_in_n1[2];					//previous n-1 value.
float lpf_out_n1[2];

//Modulation og wavetables	
float sinetable[44100];				//sinusoidal wavetable.

float lfo;							//value of lfo at lfo_ptr.
float lfo_frequency = 8;			//size of incrementing steps.
float lfo_ptr;						//current index of wavetable.

float lfo_frequency_vib = 0.5;
float lfo_ptr_vib;
float vib_depth;

int lfo_n0;							//values used for linear interpolation.
int lfo_n1;
float lfo_dif;

float logtable[44100];				//logarithmic wavetable.
float log_lfo;
float log_frequency = 0.5;
float log_ptr;
float log_ptr_direction = 1;		//value for incrementing back and forth in wabetable.
int log_n0;							//values used for linear interpolation.
int log_n1;
float log_dif;

float tremolo;						//value to attenuate output signal by, following a wavetable.
float tremolo_mix;					//amount of attenuation.

//Phaser filter parameters
float phaser_w1 = 50;				//Cutoff frequencies of cascaded all pass filters.
float phaser_w2 = 2 * phaser_w1;	
float phaser_w3 = 2 * phaser_w2;
float phaser_w4 = 2 * phaser_w3;

float apf1x_n1 = 0;					//Filter coefficients.
float apf1y_n1 = 0;
float apf2y_n1 = 0;
float apf3y_n1 = 0;
float apf4y_n1 = 0;

float phaser_depth;					//Amount of filter mix.


bool setup(BelaContext *context, void *userData)
{

	//Fill wavetables
	for (int i = 0; i < 44100; i++) {
		sinetable[i] = (float)sin(2.f*M_PI*i*(1.f/44100.f));
		logtable[i] = pow(1.01, i*278.f/44100.f);
	}
	
	pinMode(context, 0, 0, INPUT);
	pinMode(context, 0, 1, INPUT);
	pinMode(context, 0, 2, INPUT);
	
	return true;
}




void render(BelaContext *context, void *userData)
{
	
	for (int frame_i = 0; frame_i < context->audioFrames; frame_i++) {
		
		//I/O
		if(++debouncer > 11025) {		//Debouncing - a button press can be registered only after 0.25 seconds since last button press.
			debouncer = 11025;
		}
		
		if(debouncer / 11025) {									//checking if on/off button is pressed.
			on_state = digitalRead(context, frame_i, 2);
			if(!on_state && on_prev_state) {
				device_on = !device_on;
				analogWrite(context, frame_i, 2, device_on);	//Switching LED to match state of button.	
				debouncer = 0;
			}
			on_prev_state = on_state;
		}
		
		if(debouncer / 11025) {									//Checking if shift button is pressed.
			shift_state = digitalRead(context, frame_i, 0);
			if(!shift_state && shift_prev_state) {
				for(int reading = 0; reading < 3; reading++) {	//saving current values og knobs before going into shift state, to be used when returning
					if (lock[reading]){							//to non-shift state.
						saved_readings[shift][reading] = analogRead(context, frame_i/2, reading);
					}
					lock[reading] = 0;							//Locking turn-knobs, so that read values won't be used when shifting.
				}
				
				shift = !shift;
				analogWrite(context, frame_i, 0, shift);
				debouncer = 0;
			}
			shift_prev_state = shift_state;
		}
		
		if(!shift) {											//Getting values from turn-knob to control parameters, here for non-shifted state.
			float reading2 = analogRead(context, frame_i/2, 2);	//Reading from knob on analog input 2. If reading is within 5% of previously saved value,
																//knob is unlocked.
			if ((reading2 >= (0.95 * saved_readings[shift][2])) && (reading2 <= (1.05 * saved_readings[shift][2]))) {
				lock[2] = 1;
			}
			if (lock[2]) {										//Parameter 'delay_desired' is controlled on knob 2.
				delay_desired = map(reading2, 0, 0.83, 0.05, 1.99) * 44100;
			}
			
			float reading1 = analogRead(context, frame_i/2, 1);
			if ((reading1 >= (0.95 * saved_readings[shift][1])) && (reading1 <= (1.05 * saved_readings[shift][1]))) {
				lock[1] = 1;
			}
			if (lock[1]) {										//Parameter 'feedback_gain' is controlled on knob 1.
				feedback_gain = map(reading1, 0, 0.83, 0, 1.25);
			}
			
			float reading0 = analogRead(context, frame_i/2, 0);
			if ((reading0 >= (0.95 * saved_readings[shift][0])) && (reading0 <= (1.05 * saved_readings[shift][0]))) {
				lock[0] = 1;
			}
			if (lock[0]) {										//Parameter 'drywetmix' is controlled on knob 0.
				drywetmix = map(reading0, 0, 0.83, 0, 1);
			}
		} else {												//Getting values from turn-knob to control parameters, here for shifted state.
			float reading2 = analogRead(context, frame_i/2, 2);
			if ((reading2 >= (0.95 * saved_readings[shift][2])) && (reading2 <= (1.05 * saved_readings[shift][2]))) {
				lock[2] = 1;
			}
			if (lock[2]) {
				vib_depth = map(reading2, 0, 0.83, 0, 1);
			}
			
			float reading1 = analogRead(context, frame_i/2, 1);
			if ((reading1 >= (0.95 * saved_readings[shift][1])) && (reading1 <= (1.05 * saved_readings[shift][1]))) {
				lock[1] = 1;
			}
			if (lock[1]) {
				tremolo_mix = map(reading1, 0, 0.83, 0, 1);
			}
			
			float reading0 = analogRead(context, frame_i/2, 0);
			if ((reading0 >= (0.95 * saved_readings[shift][0])) && (reading0 <= (1.05 * saved_readings[shift][0]))) {
				lock[0] = 1;
			}
			if (lock[0]) {
				phaser_depth = map(reading0, 0, 0.83, 0, 1);
			}
		}
		
		
		//Vibrato
		//LFO and linear interpolation
		lfo_dif = fmod(lfo_ptr_vib, 1);
		lfo_n0 = (int)lfo_ptr_vib;
		lfo = sinetable[lfo_n0] * (1-lfo_dif) + sinetable[lfo_n0 + 1] * lfo_dif;
		
		delay_desired_vib = delay_desired + lfo * vib_depth * 400;	//The vibrato allows up to 400 samples deviation from desired delay. Vibrato amount is
																	//controlled here.
		if (delay_desired_vib > 2 * 44100) {						//Delay can't exceed two seconds, or be negative.
			delay_desired_vib = 2 * 44100;
		} else if (delay_desired_vib < 0) {
			delay_desired_vib = 0;
		}
		
	
		
		for (int channel = 0; channel < context->audioOutChannels; channel++) { //A for loop does the computation for two channels (stereo).
			in[channel] = audioRead(context, frame_i, channel);					//Audio input.
			
			//Delay Write
			delay_in[channel] = device_on * in[channel] + feedback_gain * lpf_out[channel];	//Delay input is audio input plus feedback from delayline. If device
																							//is off, audio input is not used, but feedback rings out.
			delay_buf[channel][delay_write_ptr % delay_buf_size] = delay_in[channel];		//Delay input is placed in delay buffer at write pointer.
			
			//Delay read - Smooth transistion & fractional delay by linear interpolation
			delay_s = timeAlpha * delay_desired_vib + (1-timeAlpha) * delay_s;				//Smooth transition between delay times.
			delay_read_ptr = fmod(delay_write_ptr - delay_s + delay_buf_size, delay_buf_size);
			eta = 1 - fmod(delay_read_ptr, 1);
			
			//Lowpass Filter
			lpf_in[channel] = delay_buf[channel][(int)delay_read_ptr + 1] * (1.f-eta) + delay_buf[channel][(int)delay_read_ptr] * eta;

			//First order filter coefficients, cutoff frequency 5000 radians.
			float a0 = 0.05365;
			float a1 = 0.05365;
			float b1 = 0.8927;
		
			lpf_out[channel] = b1 * lpf_out_n1[channel] + a0 * lpf_in[channel] + a1 * lpf_in_n1[channel]; //Filteroutput, through to modulation and feedback
			lpf_out_n1[channel] = lpf_out[channel];
			lpf_in_n1[channel] = lpf_in[channel];
			
			//Tremolo & linear interpolation
			lfo_dif = fmod(lfo_ptr, 1);		//The tremolo follows the LFO, which is coupled to the sinusoidal wavetable
			lfo_n0 = (int)lfo_ptr;
			lfo_n1 = lfo_n0 + 1;
			lfo = sinetable[lfo_n0] * (1-lfo_dif) + sinetable[lfo_n1] * lfo_dif;
			
			tremolo = map(lfo, -1, 1, 1-tremolo_mix, 1);	//The tremolo impact is dampened by parameter 'tremolo_mix', and mapped in the interval 0 to 1.
			
			
			// Phaser 
			
			//Linear Interpolation of wavetable
			//The phaser follows a set of frequencies, which are scaled with values obtained by sweeping the logarithmic wavetable.
			log_dif = fmod(log_ptr, 1);
			log_n0 = (int)log_ptr;
			log_n1 = log_n0 + 1;
			log_lfo = logtable[log_n0] * (1 - log_dif) + logtable[log_n1] * log_dif;
			
			//Filtering - four cascaded all pass filters.

			float ap_a1 = 1.f-phaser_w1*log_lfo/44100.f;
			float apf1y = ap_a1 * apf1y_n1 + ap_a1 * lpf_out[channel] - apf1x_n1;
			
			float ap_a2 = 1.f-phaser_w2*log_lfo/44100.f;
			float apf2y = ap_a2 * apf2y_n1 + ap_a2 * apf1y - apf1y_n1;
			
			float ap_a3 = 1.f-phaser_w3*log_lfo/44100.f;
			float apf3y = ap_a3 * apf3y_n1 + ap_a3 * apf2y - apf2y_n1;
			
			float ap_a4 = 1.f-phaser_w4*log_lfo/44100.f;
			float apf4y = ap_a4 * apf4y_n1 + ap_a4 * apf3y - apf3y_n1;
			apf4y = apf4y * phaser_depth;	//Phaser effect amount is controlled here.
			
			apf1x_n1 = lpf_out[channel];
			apf1y_n1 = apf1y;
			apf2y_n1 = apf2y;
			apf3y_n1 = apf3y;
			apf4y_n1 = apf4y;
			
			//update output
			if(drywetmix > 0.5) {	//If the wet mix is heavier than the dry, the dry is attenuated.
				drymix = 1 - map(drywetmix, 0.5, 1, 0, 1);
			} else {
				drymix = 1;
			}
			
			//The final mix of the entire delay line.
			out[channel] = (1 * !device_on +  device_on * drymix) * in[channel] + drywetmix * (lpf_out[channel] + apf4y) * tremolo;
			
			
			
			
		} //Stereo loop end
		
		
		//Increment delay pointer
		delay_write_ptr++;
			if (delay_write_ptr >= delay_buf_size) {
				delay_write_ptr = 0;
			}
		
		lfo_ptr += lfo_frequency;
		if (lfo_ptr >= 44100) {
			lfo_ptr -= 44100;
		}
		
		lfo_ptr_vib += lfo_frequency_vib;
		if (lfo_ptr_vib >= 44100) {
			lfo_ptr_vib -= 44100;
		}
		
		log_ptr += log_ptr_direction * log_frequency;
		if (log_ptr >= 44100) {
			log_ptr_direction *= -1;
			log_ptr = 44100 - 2;
		} else if (log_ptr <= 0) {
			log_ptr_direction *= -1;
			log_ptr = 0;
		}
		
		//audioWrite
		for (int channel = 0; channel < context->audioOutChannels; channel++) {
			audioWrite(context, frame_i, channel, out[channel]);
		}
	
	}
}

void cleanup(BelaContext *context, void *userData)
{

}