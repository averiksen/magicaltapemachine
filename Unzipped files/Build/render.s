	.text
	.syntax unified
	.eabi_attribute	67, "2.09"	@ Tag_conformance
	.cpu	cortex-a8
	.eabi_attribute	6, 10	@ Tag_CPU_arch
	.eabi_attribute	7, 65	@ Tag_CPU_arch_profile
	.eabi_attribute	8, 1	@ Tag_ARM_ISA_use
	.eabi_attribute	9, 2	@ Tag_THUMB_ISA_use
	.fpu	neon
	.eabi_attribute	17, 1	@ Tag_ABI_PCS_GOT_use
	.eabi_attribute	20, 2	@ Tag_ABI_FP_denormal
	.eabi_attribute	23, 1	@ Tag_ABI_FP_number_model
	.eabi_attribute	34, 1	@ Tag_CPU_unaligned_access
	.eabi_attribute	24, 1	@ Tag_ABI_align_needed
	.eabi_attribute	25, 1	@ Tag_ABI_align_preserved
	.eabi_attribute	28, 1	@ Tag_ABI_VFP_args
	.eabi_attribute	38, 1	@ Tag_ABI_FP_16bit_format
	.eabi_attribute	18, 4	@ Tag_ABI_PCS_wchar_t
	.eabi_attribute	26, 2	@ Tag_ABI_enum_size
	.eabi_attribute	14, 0	@ Tag_ABI_PCS_R9_use
	.eabi_attribute	68, 1	@ Tag_Virtualization_use
	.file	"/root/Bela/projects/MagicalTapeMachineV0.5/build/render.bc"
	.file	1 "/root/Bela/projects/MagicalTapeMachineV0.5" "render.cpp"
	.file	2 "./include" "Bela.h"
	.file	3 "/usr/bin/../lib/gcc/arm-linux-gnueabihf/6.3.0/../../../../include/c++/6.3.0/debug" "debug.h"
	.file	4 "/usr/bin/../lib/gcc/arm-linux-gnueabihf/6.3.0/../../../../include/arm-linux-gnueabihf/c++/6.3.0/bits" "c++config.h"
	.file	5 "/usr/include" "wchar.h"
	.file	6 "/usr/lib/llvm-3.9/bin/../lib/clang/3.9.1/include" "stddef.h"
	.file	7 "/usr/include" "libio.h"
	.file	8 "/usr/include/arm-linux-gnueabihf/bits" "types.h"
	.file	9 "/usr/include" "stdio.h"
	.file	10 "/usr/lib/llvm-3.9/bin/../lib/clang/3.9.1/include" "stdarg.h"
	.file	11 "/usr/include" "stdint.h"
	.file	12 "/usr/bin/../lib/gcc/arm-linux-gnueabihf/6.3.0/../../../../include/c++/6.3.0/bits" "exception_ptr.h"
	.file	13 "/usr/include" "locale.h"
	.file	14 "/usr/include" "ctype.h"
	.file	15 "/usr/include" "stdlib.h"
	.file	16 "/usr/include/arm-linux-gnueabihf/bits" "stdlib-float.h"
	.file	17 "/usr/include/arm-linux-gnueabihf/bits" "stdlib-bsearch.h"
	.file	18 "/usr/bin/../lib/gcc/arm-linux-gnueabihf/6.3.0/../../../../include/c++/6.3.0" "cstdlib"
	.file	19 "/usr/include" "_G_config.h"
	.file	20 "/usr/include/arm-linux-gnueabihf/bits" "stdio.h"
	.file	21 "/usr/include/arm-linux-gnueabihf/bits" "math-finite.h"
	.file	22 "/usr/include/arm-linux-gnueabihf/bits" "mathcalls.h"
	.file	23 "/usr/include/arm-linux-gnueabihf/bits" "mathdef.h"
	.globl	setup
	.p2align	4
	.type	setup,%function
setup:                                  @ @setup
.Lfunc_begin0:
	.loc	1 99 0                  @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:99:0
	.fnstart
	.cfi_startproc
@ BB#0:                                 @ %min.iters.checked
	.save	{r4, r5, r6, r7, r11, lr}
	push	{r4, r5, r6, r7, r11, lr}
.Ltmp0:
	.cfi_def_cfa_offset 24
.Ltmp1:
	.cfi_offset lr, -4
.Ltmp2:
	.cfi_offset r11, -8
.Ltmp3:
	.cfi_offset r7, -12
.Ltmp4:
	.cfi_offset r6, -16
.Ltmp5:
	.cfi_offset r5, -20
.Ltmp6:
	.cfi_offset r4, -24
	.setfp	r11, sp, #16
	add	r11, sp, #16
.Ltmp7:
	.cfi_def_cfa r11, 8
	.vsave	{d8, d9, d10, d11, d12, d13, d14, d15}
	vpush	{d8, d9, d10, d11, d12, d13, d14, d15}
.Ltmp8:
	.cfi_offset d15, -32
.Ltmp9:
	.cfi_offset d14, -40
.Ltmp10:
	.cfi_offset d13, -48
.Ltmp11:
	.cfi_offset d12, -56
.Ltmp12:
	.cfi_offset d11, -64
.Ltmp13:
	.cfi_offset d10, -72
.Ltmp14:
	.cfi_offset d9, -80
.Ltmp15:
	.cfi_offset d8, -88
	.pad	#32
	sub	sp, sp, #32
	@DEBUG_VALUE: setup:context <- %R0
	@DEBUG_VALUE: setup:userData <- %R1
	mov	r4, r0
.Ltmp16:
	@DEBUG_VALUE: setup:context <- %R4
	adr	r0, .LCPI0_0
.Ltmp17:
	.loc	1 103 3 prologue_end    @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:3
	movw	r6, :lower16:sinetable
	.loc	1 104 3                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:3
	movw	r7, :lower16:logtable
	vld1.64	{d8, d9}, [r0:128]
.Ltmp18:
	@DEBUG_VALUE: i <- 0
	movw	r5, #44100
	.loc	1 103 3                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:3
	movt	r6, :upper16:sinetable
	.loc	1 104 3                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:3
	movt	r7, :upper16:logtable
.Ltmp19:
.LBB0_1:                                @ %vector.body
                                        @ =>This Inner Loop Header: Depth=1
	.loc	1 103 56                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:56
	vcvt.f64.s32	d16, s16
	.loc	1 103 57 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:57
	vldr	d17, .LCPI0_1
	vorr	d10, d17, d17
	vmul.f64	d0, d16, d10
	.loc	1 103 25                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:25
	bl	sin
	vstr	d0, [sp, #24]           @ 8-byte Spill
	.loc	1 103 56                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:56
	vcvt.f64.s32	d16, s17
	.loc	1 103 57                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:57
	vmul.f64	d0, d16, d10
	.loc	1 103 25                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:25
	bl	sin
	vstr	d0, [sp, #16]           @ 8-byte Spill
	.loc	1 103 56                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:56
	vcvt.f64.s32	d16, s18
	.loc	1 103 57                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:57
	vmul.f64	d0, d16, d10
	.loc	1 103 25                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:25
	bl	sin
	vstr	d0, [sp, #8]            @ 8-byte Spill
	.loc	1 103 56                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:56
	vcvt.f64.s32	d16, s19
	.loc	1 103 57                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:57
	vmul.f64	d0, d16, d10
	.loc	1 103 25                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:25
	bl	sin
	.loc	1 104 27 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:27
	vcvt.f32.s32	q8, q4
	.loc	1 103 25                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:25
	vstr	d0, [sp]                @ 8-byte Spill
	adr	r0, .LCPI0_2
	.loc	1 104 17                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:17
	vldr	d12, .LCPI0_3
	.loc	1 104 34 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:34
	vld1.64	{d18, d19}, [r0:128]
	.loc	1 104 17                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:17
	vorr	d0, d12, d12
	.loc	1 104 34                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:34
	vmul.f32	q5, q8, q9
	.loc	1 104 27                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:27
	vcvt.f64.f32	d1, s20
	.loc	1 104 17                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:17
	bl	pow
	.loc	1 104 27                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:27
	vcvt.f64.f32	d1, s21
	.loc	1 104 17                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:17
	vorr	d13, d0, d0
	vorr	d0, d12, d12
	bl	pow
	.loc	1 104 27                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:27
	vcvt.f64.f32	d1, s22
	.loc	1 104 17                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:17
	vorr	d14, d0, d0
	vorr	d0, d12, d12
	bl	pow
	.loc	1 104 27                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:27
	vcvt.f64.f32	d1, s23
	.loc	1 104 17                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:17
	vorr	d15, d0, d0
	vorr	d0, d12, d12
	bl	pow
	.loc	1 103 25 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:25
	vldr	d16, [sp]               @ 8-byte Reload
.Ltmp20:
	.loc	1 102 2 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:102:2
	subs	r5, r5, #4
.Ltmp21:
	.loc	1 104 17                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:17
	vcvt.f32.f64	s3, d0
	.loc	1 103 25                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:25
	vcvt.f32.f64	s7, d16
	vldr	d16, [sp, #8]           @ 8-byte Reload
	.loc	1 103 3 is_stmt 0       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:3
	vmov.32	r0, d8[0]
	.loc	1 104 17 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:17
	vcvt.f32.f64	s2, d15
	.loc	1 103 25                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:25
	vcvt.f32.f64	s6, d16
	vldr	d16, [sp, #16]          @ 8-byte Reload
	.loc	1 104 17                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:17
	vcvt.f32.f64	s1, d14
	.loc	1 103 3                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:3
	add	r1, r6, r0, lsl #2
	.loc	1 104 3                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:3
	add	r0, r7, r0, lsl #2
	.loc	1 103 25                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:25
	vcvt.f32.f64	s5, d16
	vldr	d16, [sp, #24]          @ 8-byte Reload
	.loc	1 104 17                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:17
	vcvt.f32.f64	s0, d13
	.loc	1 103 25                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:25
	vcvt.f32.f64	s4, d16
.Ltmp22:
	.loc	1 102 2 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:102:2
	vmov.i32	q8, #0x4
.Ltmp23:
	.loc	1 103 16                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:103:16
	vst1.32	{d2, d3}, [r1]
.Ltmp24:
	.loc	1 102 2 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:102:2
	vadd.i32	q4, q4, q8
.Ltmp25:
	.loc	1 104 15                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:104:15
	vst1.32	{d0, d1}, [r0]
.Ltmp26:
	.loc	1 102 2 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:102:2
	bne	.LBB0_1
.Ltmp27:
@ BB#2:                                 @ %middle.block
	@DEBUG_VALUE: pinMode:frame <- 0
	@DEBUG_VALUE: pinMode:mode <- 0
	@DEBUG_VALUE: pinMode:f <- 0
	.loc	2 1469 30 discriminator 1 @ ./include/Bela.h:1469:30
	ldr	r0, [r4, #52]
	.loc	2 1469 2 is_stmt 0 discriminator 1 @ ./include/Bela.h:1469:2
	cmp	r0, #0
	beq	.LBB0_11
.Ltmp28:
@ BB#3:                                 @ %.lr.ph.i
	.loc	2 1471 13 is_stmt 1     @ ./include/Bela.h:1471:13
	ldr	r0, [r4, #16]
	mov	r1, #0
.Ltmp29:
.LBB0_4:                                @ =>This Inner Loop Header: Depth=1
	.loc	2 1471 24 is_stmt 0     @ ./include/Bela.h:1471:24
	ldr	r2, [r0, r1, lsl #2]
	orr	r2, r2, #1
	str	r2, [r0, r1, lsl #2]
.Ltmp30:
	.loc	2 1469 46 is_stmt 1 discriminator 2 @ ./include/Bela.h:1469:46
	add	r1, r1, #1
.Ltmp31:
	@DEBUG_VALUE: pinMode:f <- %R1
	.loc	2 1469 30 is_stmt 0 discriminator 1 @ ./include/Bela.h:1469:30
	ldr	r2, [r4, #52]
	.loc	2 1469 2 discriminator 1 @ ./include/Bela.h:1469:2
	cmp	r1, r2
	blo	.LBB0_4
.Ltmp32:
@ BB#5:                                 @ %_ZL7pinModeP11BelaContextiii.exit
	@DEBUG_VALUE: pinMode:f <- %R1
	@DEBUG_VALUE: pinMode:frame <- 0
	@DEBUG_VALUE: pinMode:mode <- 0
	@DEBUG_VALUE: pinMode:f <- 0
	.loc	2 1469 2 discriminator 1 @ ./include/Bela.h:1469:2
	cmp	r2, #0
	beq	.LBB0_11
.Ltmp33:
@ BB#6:                                 @ %.lr.ph.i12.preheader
	@DEBUG_VALUE: pinMode:f <- %R1
	mov	r1, #0
.Ltmp34:
.LBB0_7:                                @ %.lr.ph.i12
                                        @ =>This Inner Loop Header: Depth=1
	.loc	2 1471 24 is_stmt 1     @ ./include/Bela.h:1471:24
	ldr	r2, [r0, r1, lsl #2]
	orr	r2, r2, #2
	str	r2, [r0, r1, lsl #2]
.Ltmp35:
	.loc	2 1469 46 discriminator 2 @ ./include/Bela.h:1469:46
	add	r1, r1, #1
.Ltmp36:
	@DEBUG_VALUE: pinMode:f <- %R1
	.loc	2 1469 30 is_stmt 0 discriminator 1 @ ./include/Bela.h:1469:30
	ldr	r2, [r4, #52]
	.loc	2 1469 2 discriminator 1 @ ./include/Bela.h:1469:2
	cmp	r1, r2
	blo	.LBB0_7
.Ltmp37:
@ BB#8:                                 @ %_ZL7pinModeP11BelaContextiii.exit14
	@DEBUG_VALUE: pinMode:f <- %R1
	@DEBUG_VALUE: pinMode:frame <- 0
	@DEBUG_VALUE: pinMode:mode <- 0
	@DEBUG_VALUE: pinMode:f <- 0
	.loc	2 1469 2 discriminator 1 @ ./include/Bela.h:1469:2
	cmp	r2, #0
	beq	.LBB0_11
.Ltmp38:
@ BB#9:                                 @ %.lr.ph.i9
	@DEBUG_VALUE: pinMode:f <- %R1
	.loc	2 1471 13 is_stmt 1     @ ./include/Bela.h:1471:13
	ldr	r0, [r4, #16]
	mov	r1, #0
.Ltmp39:
.LBB0_10:                               @ =>This Inner Loop Header: Depth=1
	.loc	2 1471 24 is_stmt 0     @ ./include/Bela.h:1471:24
	ldr	r2, [r0, r1, lsl #2]
	orr	r2, r2, #4
	str	r2, [r0, r1, lsl #2]
.Ltmp40:
	.loc	2 1469 46 is_stmt 1 discriminator 2 @ ./include/Bela.h:1469:46
	add	r1, r1, #1
.Ltmp41:
	@DEBUG_VALUE: pinMode:f <- %R1
	.loc	2 1469 30 is_stmt 0 discriminator 1 @ ./include/Bela.h:1469:30
	ldr	r2, [r4, #52]
	.loc	2 1469 2 discriminator 1 @ ./include/Bela.h:1469:2
	cmp	r1, r2
	blo	.LBB0_10
.Ltmp42:
.LBB0_11:                               @ %_ZL7pinModeP11BelaContextiii.exit11
	.loc	1 111 2 is_stmt 1       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:111:2
	mov	r0, #1
	sub	sp, r11, #80
	vpop	{d8, d9, d10, d11, d12, d13, d14, d15}
	pop	{r4, r5, r6, r7, r11, pc}
.Ltmp43:
	.p2align	4
@ BB#12:
.LCPI0_0:
	.long	0                       @ 0x0
	.long	1                       @ 0x1
	.long	2                       @ 0x2
	.long	3                       @ 0x3
.LCPI0_2:
	.long	1003393169              @ float 0.00630385475
	.long	1003393169              @ float 0.00630385475
	.long	1003393169              @ float 0.00630385475
	.long	1003393169              @ float 0.00630385475
.LCPI0_1:
	.long	1171396426              @ double 1.4247585469123254E-4
	.long	1059237042
.LCPI0_3:
	.long	3264175145              @ double 1.01
	.long	1072703733
.Lfunc_end0:
	.size	setup, .Lfunc_end0-setup
	.cfi_endproc
	.fnend

	.globl	render
	.p2align	3
	.type	render,%function
render:                                 @ @render
.Lfunc_begin1:
	.loc	1 118 0                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:118:0
	.fnstart
	.cfi_startproc
@ BB#0:
	.save	{r4, r5, r6, r7, r8, r9, r10, r11, lr}
	push	{r4, r5, r6, r7, r8, r9, r10, r11, lr}
.Ltmp44:
	.cfi_def_cfa_offset 36
.Ltmp45:
	.cfi_offset lr, -4
.Ltmp46:
	.cfi_offset r11, -8
.Ltmp47:
	.cfi_offset r10, -12
.Ltmp48:
	.cfi_offset r9, -16
.Ltmp49:
	.cfi_offset r8, -20
.Ltmp50:
	.cfi_offset r7, -24
.Ltmp51:
	.cfi_offset r6, -28
.Ltmp52:
	.cfi_offset r5, -32
.Ltmp53:
	.cfi_offset r4, -36
	.setfp	r11, sp, #28
	add	r11, sp, #28
.Ltmp54:
	.cfi_def_cfa r11, 8
	.pad	#4
	sub	sp, sp, #4
	.vsave	{d8, d9, d10, d11, d12, d13, d14, d15}
	vpush	{d8, d9, d10, d11, d12, d13, d14, d15}
.Ltmp55:
	.cfi_offset d15, -48
.Ltmp56:
	.cfi_offset d14, -56
.Ltmp57:
	.cfi_offset d13, -64
.Ltmp58:
	.cfi_offset d12, -72
.Ltmp59:
	.cfi_offset d11, -80
.Ltmp60:
	.cfi_offset d10, -88
.Ltmp61:
	.cfi_offset d9, -96
.Ltmp62:
	.cfi_offset d8, -104
	.pad	#72
	sub	sp, sp, #72
	@DEBUG_VALUE: render:context <- %R0
	@DEBUG_VALUE: render:userData <- %R1
	mov	r9, r0
.Ltmp63:
	@DEBUG_VALUE: frame_i <- 0
	@DEBUG_VALUE: render:context <- %R9
	.loc	1 120 43 prologue_end discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:120:43
	ldr	r0, [r9, #20]
	.loc	1 120 2 is_stmt 0 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:120:2
	cmp	r0, #0
	beq	.LBB1_102
.Ltmp64:
@ BB#1:                                 @ %.lr.ph210
	@DEBUG_VALUE: render:context <- %R9
	@DEBUG_VALUE: render:userData <- %R1
	mov	r0, #0
.Ltmp65:
	.loc	1 123 6 is_stmt 1       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:123:6
	movw	r7, :lower16:.L_MergedGlobals
.Ltmp66:
	.loc	1 141 10                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:141:10
	movw	r8, :lower16:.L_MergedGlobals.6
	str	r0, [sp, #8]            @ 4-byte Spill
	mov	r0, #0
.Ltmp67:
	.loc	1 157 78 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:78
	vldr	d10, .LCPI1_14
	.loc	1 157 27 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:27
	vldr	d11, .LCPI1_15
	mov	r10, #2
.Ltmp68:
	.file	24 "./include" "Utilities.h"
	.loc	24 73 44 is_stmt 1      @ ./include/Utilities.h:73:44
	vldr	s24, .LCPI1_16
.Ltmp69:
	.loc	1 123 6                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:123:6
	movt	r7, :upper16:.L_MergedGlobals
.Ltmp70:
	.loc	1 214 25                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:214:25
	vldr	s30, .LCPI1_17
.Ltmp71:
	.loc	1 141 10                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:141:10
	movt	r8, :upper16:.L_MergedGlobals.6
	str	r0, [sp, #4]            @ 4-byte Spill
	mov	r0, #0
.Ltmp72:
	.loc	1 120 2 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:120:2
	str	r0, [sp, #68]           @ 4-byte Spill
	str	r9, [sp, #60]           @ 4-byte Spill
.Ltmp73:
.LBB1_2:                                @ =>This Loop Header: Depth=1
                                        @     Child Loop BB1_10 Depth 2
                                        @     Child Loop BB1_13 Depth 2
                                        @     Child Loop BB1_31 Depth 2
                                        @     Child Loop BB1_34 Depth 2
                                        @     Child Loop BB1_82 Depth 2
                                        @     Child Loop BB1_97 Depth 2
                                        @     Child Loop BB1_100 Depth 2
	@DEBUG_VALUE: analogWrite:frame <- [%SP+68]
	.loc	1 123 6                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:123:6
	ldr	r0, [r7, #4]
.Ltmp74:
	.loc	1 127 6                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:127:6
	movw	r2, #22049
.Ltmp75:
	.loc	1 123 6                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:123:6
	add	r1, r0, #1
	.loc	1 123 18 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:123:18
	movw	r0, #11025
.Ltmp76:
	.loc	1 123 6                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:123:6
	cmp	r1, r0
.Ltmp77:
	.loc	1 127 6 is_stmt 1       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:127:6
	movw	r0, #11024
.Ltmp78:
	.loc	1 123 6                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:123:6
	movwgt	r1, #11025
.Ltmp79:
	.loc	1 127 6                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:127:6
	add	r0, r1, r0
.Ltmp80:
	.loc	1 124 14                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:124:14
	str	r1, [r7, #4]
.Ltmp81:
	.loc	1 127 6                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:127:6
	cmp	r0, r2
	blo	.LBB1_16
.Ltmp82:
@ BB#3:                                 @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: analogWrite:frame <- [%SP+68]
	ldr	r0, [r9, #16]
.Ltmp83:
	.loc	2 1438 12               @ ./include/Bela.h:1438:12
	ldr	r2, [sp, #68]           @ 4-byte Reload
	ldr	r0, [r0, r2, lsl #2]
.Ltmp84:
	.loc	1 134 20                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:134:20
	ubfx	r0, r0, #18, #1
	.loc	1 128 13                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:128:13
	str	r0, [r7, #8]
.Ltmp85:
	.loc	1 129 17                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:129:17
	cmp	r0, #0
	bne	.LBB1_15
.Ltmp86:
@ BB#4:                                 @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: analogWrite:frame <- [%SP+68]
	.loc	1 129 20 is_stmt 0 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:129:20
	ldr	r2, [r7, #12]
	cmp	r2, #0
	beq	.LBB1_15
.Ltmp87:
@ BB#5:                                 @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: analogWrite:frame <- [%SP+68]
	.loc	1 130 18 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:130:18
	ldr	r1, [r7]
	.loc	1 131 38                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:131:38
	vmov.f32	d1, #1.000000e+00
	vmov.i32	d0, #0x0
.Ltmp88:
	.loc	2 1424 2 discriminator 1 @ ./include/Bela.h:1424:2
	ldr	r2, [sp, #68]           @ 4-byte Reload
.Ltmp89:
	.loc	1 131 38                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:131:38
	cmp	r1, #0
	.loc	1 130 17                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:130:17
	mov	r1, #0
	.loc	1 131 38                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:131:38
	vmoveq.f32	s0, s2
.Ltmp90:
	@DEBUG_VALUE: analogWrite:value <- %S0
	@DEBUG_VALUE: analogWrite:channel <- 2
	.loc	1 130 17                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:130:17
	movweq	r1, #1
	.loc	1 130 15 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:130:15
	str	r1, [r7]
.Ltmp91:
	.loc	2 1424 30 is_stmt 1 discriminator 1 @ ./include/Bela.h:1424:30
	ldr	r1, [r9, #36]
	.loc	2 1424 2 is_stmt 0 discriminator 1 @ ./include/Bela.h:1424:2
	cmp	r1, r2
.Ltmp92:
	@DEBUG_VALUE: analogWrite:context <- %R9
	bls	.LBB1_14
.Ltmp93:
@ BB#6:                                 @ %.lr.ph.i186
                                        @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: analogWrite:context <- %R9
	@DEBUG_VALUE: analogWrite:value <- %S0
	@DEBUG_VALUE: analogWrite:frame <- [%SP+68]
	ldr	r3, [sp, #68]           @ 4-byte Reload
	.loc	1 131 5 is_stmt 1       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:131:5
	ldr	r12, [r9, #12]
	ldr	r2, [r9, #44]
.Ltmp94:
	.loc	2 1424 2 discriminator 1 @ ./include/Bela.h:1424:2
	sub	r6, r1, r3
	mov	r5, r3
	cmp	r6, #4
	blo	.LBB1_12
.Ltmp95:
@ BB#7:                                 @ %min.iters.checked262
                                        @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: analogWrite:context <- %R9
	@DEBUG_VALUE: analogWrite:value <- %S0
	@DEBUG_VALUE: analogWrite:frame <- [%SP+68]
	mov	lr, r6
	ldr	r5, [sp, #68]           @ 4-byte Reload
	bfc	lr, #0, #2
	cmp	lr, #0
	beq	.LBB1_12
.Ltmp96:
@ BB#8:                                 @ %vector.scevcheck268
                                        @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: analogWrite:context <- %R9
	@DEBUG_VALUE: analogWrite:value <- %S0
	@DEBUG_VALUE: analogWrite:frame <- [%SP+68]
	ldr	r5, [sp, #68]           @ 4-byte Reload
	cmp	r2, #1
	bne	.LBB1_12
.Ltmp97:
@ BB#9:                                 @ %vector.ph269
                                        @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: analogWrite:context <- %R9
	@DEBUG_VALUE: analogWrite:value <- %S0
	@DEBUG_VALUE: analogWrite:frame <- [%SP+68]
	ldr	r4, [sp, #4]            @ 4-byte Reload
	vdup.32	q8, d0[0]
	ldr	r3, [sp, #68]           @ 4-byte Reload
	add	r4, r1, r4
	add	r5, r3, lr
	add	r3, r12, r10, lsl #2
	bfc	r4, #0, #2
.Ltmp98:
.LBB1_10:                               @ %vector.body258
                                        @   Parent Loop BB1_2 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
	.loc	2 1411 67               @ ./include/Bela.h:1411:67
	vst1.32	{d16, d17}, [r3]!
.Ltmp99:
	.loc	2 1424 2 discriminator 1 @ ./include/Bela.h:1424:2
	subs	r4, r4, #4
	bne	.LBB1_10
@ BB#11:                                @ %middle.block259
                                        @   in Loop: Header=BB1_2 Depth=1
	cmp	r6, lr
	beq	.LBB1_14
.LBB1_12:                               @ %scalar.ph260.preheader
                                        @   in Loop: Header=BB1_2 Depth=1
.Ltmp100:
	.loc	2 1411 27               @ ./include/Bela.h:1411:27
	mul	r3, r5, r2
	sub	r1, r1, r5
	lsl	r2, r2, #2
	add	r3, r12, r3, lsl #2
	add	r3, r3, #8
.LBB1_13:                               @ %scalar.ph260
                                        @   Parent Loop BB1_2 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
	.loc	2 1411 67 is_stmt 0     @ ./include/Bela.h:1411:67
	vstr	s0, [r3]
.Ltmp101:
	.loc	2 1424 2 is_stmt 1 discriminator 1 @ ./include/Bela.h:1424:2
	add	r3, r3, r2
	subs	r1, r1, #1
	bne	.LBB1_13
.Ltmp102:
.LBB1_14:                               @ %_ZL11analogWriteP11BelaContextiif.exit188
                                        @   in Loop: Header=BB1_2 Depth=1
	mov	r1, #0
	.loc	1 132 15                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:132:15
	str	r1, [r7, #4]
.Ltmp103:
.LBB1_15:                               @   in Loop: Header=BB1_2 Depth=1
	.loc	1 134 18                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:134:18
	str	r0, [r7, #12]
.Ltmp104:
.LBB1_16:                               @   in Loop: Header=BB1_2 Depth=1
	.loc	1 127 6                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:127:6
	movw	r0, #11024
.Ltmp105:
	.loc	1 137 6                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:137:6
	add	r0, r1, r0
.Ltmp106:
	.loc	1 127 6                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:127:6
	movw	r1, #22049
	.loc	1 137 6                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:137:6
	cmp	r0, r1
	blo	.LBB1_37
@ BB#17:                                @   in Loop: Header=BB1_2 Depth=1
.Ltmp107:
	.loc	2 1438 12               @ ./include/Bela.h:1438:12
	ldr	r1, [sp, #68]           @ 4-byte Reload
	ldr	r0, [r9, #16]
	add	r0, r0, r1, lsl #2
	ldrh	r0, [r0, #2]
.Ltmp108:
	.loc	1 151 23                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:151:23
	ands	r0, r0, #1
	.loc	1 138 16                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:138:16
	str	r0, [r7, #20]
	bne	.LBB1_36
@ BB#18:                                @   in Loop: Header=BB1_2 Depth=1
.Ltmp109:
	.loc	1 139 23 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:139:23
	ldr	r1, [r7, #24]
	cmp	r1, #0
	beq	.LBB1_36
@ BB#19:                                @ %.preheader202
                                        @   in Loop: Header=BB1_2 Depth=1
.Ltmp110:
	.loc	1 142 67                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:67
	ldr	r1, [sp, #68]           @ 4-byte Reload
.Ltmp111:
	.loc	1 141 10                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:141:10
	ldr	r2, [r8, #112]
.Ltmp112:
	.loc	1 142 67                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:67
	add	r1, r1, r1, lsr #31
.Ltmp113:
	.loc	1 141 10                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:141:10
	cmp	r2, #0
.Ltmp114:
	.loc	1 142 67                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:67
	asr	r1, r1, #1
	beq	.LBB1_21
@ BB#20:                                @   in Loop: Header=BB1_2 Depth=1
	ldr	r3, [r9, #40]
	.loc	1 142 22 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:22
	ldr	r6, [r7, #16]
	ldr	r2, [r9, #8]
.Ltmp115:
	.loc	2 1400 33 is_stmt 1     @ ./include/Bela.h:1400:33
	mul	r3, r3, r1
.Ltmp116:
	.loc	1 142 7                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:7
	add	r6, r6, r6, lsl #1
.Ltmp117:
	.loc	2 1400 9                @ ./include/Bela.h:1400:9
	ldr	r2, [r2, r3, lsl #2]
.Ltmp118:
	.loc	1 142 7                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:7
	movw	r3, :lower16:.L_MergedGlobals.7
	movt	r3, :upper16:.L_MergedGlobals.7
	.loc	1 142 38 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:38
	str	r2, [r3, r6, lsl #2]
.Ltmp119:
.LBB1_21:                               @   in Loop: Header=BB1_2 Depth=1
	mov	r2, #0
	.loc	1 144 20 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:144:20
	str	r2, [r8, #112]
.Ltmp120:
	@DEBUG_VALUE: reading <- 1
	.loc	1 141 10                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:141:10
	ldr	r2, [r8, #116]
.Ltmp121:
	.loc	1 141 10 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:141:10
	cmp	r2, #0
	beq	.LBB1_23
@ BB#22:                                @   in Loop: Header=BB1_2 Depth=1
	ldr	r3, [r9, #40]
.Ltmp122:
	.loc	1 142 22 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:22
	ldr	r6, [r7, #16]
	ldr	r2, [r9, #8]
.Ltmp123:
	.loc	2 1400 33               @ ./include/Bela.h:1400:33
	mul	r3, r3, r1
	add	r2, r2, r3, lsl #2
.Ltmp124:
	.loc	1 142 7                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:7
	add	r3, r6, r6, lsl #1
	movw	r6, :lower16:.L_MergedGlobals.7
	movt	r6, :upper16:.L_MergedGlobals.7
	add	r3, r6, r3, lsl #2
.Ltmp125:
	.loc	2 1400 9                @ ./include/Bela.h:1400:9
	ldr	r2, [r2, #4]
.Ltmp126:
	.loc	1 142 38                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:38
	str	r2, [r3, #4]
.Ltmp127:
.LBB1_23:                               @   in Loop: Header=BB1_2 Depth=1
	mov	r2, #0
	.loc	1 144 20                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:144:20
	str	r2, [r8, #116]
	@DEBUG_VALUE: reading <- 1
.Ltmp128:
	.loc	1 141 10                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:141:10
	ldr	r2, [r8, #120]
.Ltmp129:
	.loc	1 141 10 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:141:10
	cmp	r2, #0
	beq	.LBB1_25
@ BB#24:                                @   in Loop: Header=BB1_2 Depth=1
	ldr	r3, [r9, #40]
.Ltmp130:
	.loc	1 142 7 is_stmt 1       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:7
	movw	r6, :lower16:.L_MergedGlobals.7
	ldr	r2, [r9, #8]
	movt	r6, :upper16:.L_MergedGlobals.7
.Ltmp131:
	.loc	2 1400 33               @ ./include/Bela.h:1400:33
	mul	r3, r3, r1
.Ltmp132:
	.loc	1 142 22                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:22
	ldr	r1, [r7, #16]
	add	r2, r2, r3, lsl #2
	.loc	1 142 7 is_stmt 0       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:7
	add	r3, r1, r1, lsl #1
	add	r3, r6, r3, lsl #2
.Ltmp133:
	.loc	2 1400 9 is_stmt 1      @ ./include/Bela.h:1400:9
	ldr	r2, [r2, #8]
.Ltmp134:
	.loc	1 142 38                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:38
	str	r2, [r3, #8]
	b	.LBB1_26
.Ltmp135:
.LBB1_25:                               @ %._crit_edge215
                                        @   in Loop: Header=BB1_2 Depth=1
	.loc	1 147 14                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:147:14
	ldr	r1, [r7, #16]
.Ltmp136:
.LBB1_26:                               @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: reading <- 1
	.loc	1 131 38                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:131:38
	vmov.f32	d1, #1.000000e+00
.Ltmp137:
	.loc	1 148 38                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:148:38
	cmp	r1, #0
	vmov.i32	d0, #0x0
	mov	r1, #0
.Ltmp138:
	.loc	1 144 20                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:144:20
	str	r1, [r8, #120]
.Ltmp139:
	.loc	1 147 13                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:147:13
	mov	r1, #0
	.loc	1 148 38                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:148:38
	vmoveq.f32	s0, s2
.Ltmp140:
	@DEBUG_VALUE: analogWrite:value <- %S0
	@DEBUG_VALUE: analogWrite:channel <- 0
	.loc	1 147 13                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:147:13
	movweq	r1, #1
	.loc	1 147 11 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:147:11
	str	r1, [r7, #16]
.Ltmp141:
	.loc	2 1424 30 is_stmt 1 discriminator 1 @ ./include/Bela.h:1424:30
	ldr	r1, [r9, #36]
	.loc	2 1424 2 is_stmt 0 discriminator 1 @ ./include/Bela.h:1424:2
	ldr	r2, [sp, #68]           @ 4-byte Reload
	cmp	r1, r2
.Ltmp142:
	@DEBUG_VALUE: analogWrite:context <- %R9
	bls	.LBB1_35
.Ltmp143:
@ BB#27:                                @ %.lr.ph.i
                                        @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: analogWrite:context <- %R9
	@DEBUG_VALUE: analogWrite:value <- %S0
	ldr	r2, [sp, #68]           @ 4-byte Reload
	.loc	1 148 5 is_stmt 1       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:148:5
	ldr	r12, [r9, #12]
	ldr	r3, [r9, #44]
.Ltmp144:
	.loc	2 1424 2 discriminator 1 @ ./include/Bela.h:1424:2
	sub	r6, r1, r2
	mov	r5, r2
	cmp	r6, #4
	blo	.LBB1_33
.Ltmp145:
@ BB#28:                                @ %min.iters.checked242
                                        @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: analogWrite:context <- %R9
	@DEBUG_VALUE: analogWrite:value <- %S0
	mov	lr, r6
	ldr	r5, [sp, #68]           @ 4-byte Reload
	bfc	lr, #0, #2
	cmp	lr, #0
	beq	.LBB1_33
.Ltmp146:
@ BB#29:                                @ %vector.scevcheck
                                        @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: analogWrite:context <- %R9
	@DEBUG_VALUE: analogWrite:value <- %S0
	ldr	r5, [sp, #68]           @ 4-byte Reload
	cmp	r3, #1
	bne	.LBB1_33
.Ltmp147:
@ BB#30:                                @ %vector.ph247
                                        @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: analogWrite:context <- %R9
	@DEBUG_VALUE: analogWrite:value <- %S0
	ldr	r4, [sp, #4]            @ 4-byte Reload
	vdup.32	q8, d0[0]
	ldr	r2, [sp, #68]           @ 4-byte Reload
	add	r4, r1, r4
	add	r5, r2, lr
	add	r2, r12, r2, lsl #2
	bfc	r4, #0, #2
.Ltmp148:
.LBB1_31:                               @ %vector.body238
                                        @   Parent Loop BB1_2 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
	.loc	2 1411 67               @ ./include/Bela.h:1411:67
	vst1.32	{d16, d17}, [r2]!
.Ltmp149:
	.loc	2 1424 2 discriminator 1 @ ./include/Bela.h:1424:2
	subs	r4, r4, #4
	bne	.LBB1_31
@ BB#32:                                @ %middle.block239
                                        @   in Loop: Header=BB1_2 Depth=1
	cmp	r6, lr
	beq	.LBB1_35
.LBB1_33:                               @ %scalar.ph240.preheader
                                        @   in Loop: Header=BB1_2 Depth=1
.Ltmp150:
	.loc	2 1411 27               @ ./include/Bela.h:1411:27
	mul	r2, r5, r3
	sub	r1, r1, r5
	lsl	r3, r3, #2
	add	r2, r12, r2, lsl #2
.LBB1_34:                               @ %scalar.ph240
                                        @   Parent Loop BB1_2 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
	.loc	2 1411 67 is_stmt 0     @ ./include/Bela.h:1411:67
	vstr	s0, [r2]
.Ltmp151:
	.loc	2 1424 2 is_stmt 1 discriminator 1 @ ./include/Bela.h:1424:2
	add	r2, r2, r3
	subs	r1, r1, #1
	bne	.LBB1_34
.Ltmp152:
.LBB1_35:                               @ %_ZL11analogWriteP11BelaContextiif.exit
                                        @   in Loop: Header=BB1_2 Depth=1
	mov	r1, #0
	.loc	1 149 15                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:149:15
	str	r1, [r7, #4]
.Ltmp153:
.LBB1_36:                               @   in Loop: Header=BB1_2 Depth=1
	.loc	1 151 21                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:151:21
	str	r0, [r7, #24]
.Ltmp154:
.LBB1_37:                               @   in Loop: Header=BB1_2 Depth=1
	.loc	1 173 44                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:173:44
	ldr	r5, [r7, #16]
.Ltmp155:
	.loc	1 142 7                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:7
	movw	r1, :lower16:.L_MergedGlobals.7
	movt	r1, :upper16:.L_MergedGlobals.7
	ldr	r2, [r9, #40]
.Ltmp156:
	.loc	1 157 18                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:18
	mov	r4, #0
	.loc	1 157 29 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:29
	add	r0, r5, r5, lsl #1
	add	r3, r1, r0, lsl #2
.Ltmp157:
	.loc	1 155 48 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:155:48
	ldr	r1, [sp, #68]           @ 4-byte Reload
	ldr	r0, [r9, #8]
.Ltmp158:
	.loc	1 157 29                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:29
	vldr	s0, [r3, #8]
.Ltmp159:
	.loc	1 155 48                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:155:48
	add	r1, r1, r1, lsr #31
.Ltmp160:
	.loc	1 157 29                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:29
	vcvt.f64.f32	d16, s0
.Ltmp161:
	.loc	1 155 48                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:155:48
	asr	r1, r1, #1
.Ltmp162:
	.loc	2 1400 33               @ ./include/Bela.h:1400:33
	mul	r6, r2, r1
.Ltmp163:
	.loc	1 157 78 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:78
	vmul.f64	d17, d16, d10
	add	r6, r0, r6, lsl #2
.Ltmp164:
	.loc	2 1400 9                @ ./include/Bela.h:1400:9
	vldr	s0, [r6, #8]
.Ltmp165:
	@DEBUG_VALUE: reading2 <- %S0
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading2 <- %S0
	@DEBUG_VALUE: map:x <- %S0
	.loc	1 157 69 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:69
	mov	r6, #0
	.loc	1 157 27 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:27
	vmul.f64	d16, d16, d11
	.loc	1 157 9                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:9
	vcvt.f64.f32	d18, s0
	.loc	1 157 69 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:69
	vcmpe.f64	d18, d17
	vmrs	APSR_nzcv, fpscr
	.loc	1 157 18                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:18
	vcmpe.f64	d18, d16
	.loc	1 157 69 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:69
	movwgt	r6, #1
	.loc	1 157 18                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:18
	vmrs	APSR_nzcv, fpscr
	movwlt	r4, #1
	.loc	1 157 56                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:56
	orr	r6, r4, r6
.Ltmp166:
	.loc	1 154 6 is_stmt 1       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:154:6
	cmp	r5, #0
	beq	.LBB1_40
.Ltmp167:
@ BB#38:                                @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading2 <- %S0
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading2 <- %S0
	.loc	1 181 56                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:181:56
	cmp	r6, #0
	beq	.LBB1_46
.Ltmp168:
@ BB#39:                                @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading2 <- %S0
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading2 <- %S0
	.loc	1 184 8                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:184:8
	ldr	r6, [r8, #120]
.Ltmp169:
	.loc	1 184 8 is_stmt 0       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:184:8
	cmp	r6, #0
	bne	.LBB1_47
	b	.LBB1_48
.Ltmp170:
.LBB1_40:                               @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading2 <- %S0
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading2 <- %S0
	.loc	1 157 56 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:56
	cmp	r6, #0
	beq	.LBB1_58
.Ltmp171:
@ BB#41:                                @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading2 <- %S0
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading2 <- %S0
	.loc	1 160 8                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:160:8
	ldr	r3, [r8, #120]
.Ltmp172:
	.loc	1 160 8 is_stmt 0       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:160:8
	cmp	r3, #0
	bne	.LBB1_59
	b	.LBB1_60
.Ltmp173:
	.p2align	3
@ BB#42:
.LCPI1_14:
	.long	3435973837              @ double 1.05
	.long	1072745676
	.p2align	3
@ BB#43:
.LCPI1_15:
	.long	1717986918              @ double 0.94999999999999996
	.long	1072588390
	.p2align	2
@ BB#44:
.LCPI1_16:
	.long	1067071365              @ float 1.20481932
	.p2align	2
@ BB#45:
.LCPI1_17:
	.long	1202471936              @ float 88200
.LBB1_46:                               @ %.thread194
                                        @   in Loop: Header=BB1_2 Depth=1
.Ltmp174:
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading2 <- %S0
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading2 <- %S0
	.loc	1 174 13 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:174:13
	mov	r2, #1
.Ltmp175:
	.loc	1 182 13                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:182:13
	str	r2, [r8, #120]
	ldr	r2, [r9, #40]
.Ltmp176:
.LBB1_47:                               @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading2 <- %S0
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading2 <- %S0
	@DEBUG_VALUE: map:in_min <- 0.000000e+00
	@DEBUG_VALUE: map:in_max <- 8.300000e-01
	@DEBUG_VALUE: map:out_min <- 0.000000e+00
	@DEBUG_VALUE: map:out_max <- 1.000000e+00
	.loc	24 73 44                @ ./include/Utilities.h:73:44
	vmul.f32	d0, d0, d12
.Ltmp177:
	.loc	1 185 15                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:185:15
	vstr	s0, [r7, #88]
.Ltmp178:
.LBB1_48:                               @   in Loop: Header=BB1_2 Depth=1
	.loc	1 189 29                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:189:29
	vldr	s0, [r3, #4]
.Ltmp179:
	.loc	2 1400 33               @ ./include/Bela.h:1400:33
	mul	r6, r2, r1
.Ltmp180:
	.loc	1 189 29                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:189:29
	vcvt.f64.f32	d16, s0
.Ltmp181:
	.loc	2 1400 9                @ ./include/Bela.h:1400:9
	add	r6, r0, r6, lsl #2
.Ltmp182:
	.loc	1 189 27                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:189:27
	vmul.f64	d18, d16, d11
.Ltmp183:
	.loc	2 1400 9                @ ./include/Bela.h:1400:9
	vldr	s0, [r6, #4]
.Ltmp184:
	@DEBUG_VALUE: reading1 <- %S0
	@DEBUG_VALUE: map:x <- %S0
	.loc	1 189 9                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:189:9
	vcvt.f64.f32	d17, s0
	.loc	1 189 56 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:189:56
	vcmpe.f64	d17, d18
	vmrs	APSR_nzcv, fpscr
	blt	.LBB1_51
.Ltmp185:
@ BB#49:                                @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading1 <- %S0
	.loc	1 189 78 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:189:78
	vmul.f64	d16, d16, d10
	vcmpe.f64	d17, d16
	vmrs	APSR_nzcv, fpscr
	bgt	.LBB1_51
.Ltmp186:
@ BB#50:                                @ %.thread196
                                        @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading1 <- %S0
	.loc	1 174 13 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:174:13
	mov	r2, #1
.Ltmp187:
	.loc	1 190 13                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:190:13
	str	r2, [r8, #116]
	ldr	r2, [r9, #40]
	b	.LBB1_52
.Ltmp188:
.LBB1_51:                               @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading1 <- %S0
	.loc	1 192 8                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:192:8
	ldr	r6, [r8, #116]
.Ltmp189:
	.loc	1 192 8 is_stmt 0       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:192:8
	cmp	r6, #0
	beq	.LBB1_53
.Ltmp190:
.LBB1_52:                               @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading1 <- %S0
	@DEBUG_VALUE: map:in_min <- 0.000000e+00
	@DEBUG_VALUE: map:in_max <- 8.300000e-01
	@DEBUG_VALUE: map:out_min <- 0.000000e+00
	@DEBUG_VALUE: map:out_max <- 1.000000e+00
	.loc	24 73 44 is_stmt 1      @ ./include/Utilities.h:73:44
	vmul.f32	d0, d0, d12
.Ltmp191:
	.loc	1 193 17                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:193:17
	vstr	s0, [r8, #12]
.Ltmp192:
.LBB1_53:                               @   in Loop: Header=BB1_2 Depth=1
	.loc	1 197 29                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:197:29
	vldr	s0, [r3]
.Ltmp193:
	.loc	2 1400 33               @ ./include/Bela.h:1400:33
	mul	r1, r2, r1
.Ltmp194:
	.loc	1 197 29                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:197:29
	vcvt.f64.f32	d16, s0
.Ltmp195:
	.loc	2 1400 9                @ ./include/Bela.h:1400:9
	add	r0, r0, r1, lsl #2
.Ltmp196:
	.loc	1 197 27                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:197:27
	vmul.f64	d18, d16, d11
.Ltmp197:
	.loc	2 1400 9                @ ./include/Bela.h:1400:9
	vldr	s0, [r0]
.Ltmp198:
	@DEBUG_VALUE: reading0 <- %S0
	@DEBUG_VALUE: map:x <- %S0
	.loc	1 197 9                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:197:9
	vcvt.f64.f32	d17, s0
	.loc	1 197 56 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:197:56
	vcmpe.f64	d17, d18
	vmrs	APSR_nzcv, fpscr
	blt	.LBB1_56
.Ltmp199:
@ BB#54:                                @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading0 <- %S0
	.loc	1 197 78 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:197:78
	vmul.f64	d16, d16, d10
	vcmpe.f64	d17, d16
	vmrs	APSR_nzcv, fpscr
	bgt	.LBB1_56
.Ltmp200:
@ BB#55:                                @ %.thread198
                                        @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading0 <- %S0
	.loc	1 174 13 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:174:13
	mov	r0, #1
.Ltmp201:
	.loc	1 198 13                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:198:13
	str	r0, [r8, #112]
	b	.LBB1_57
.Ltmp202:
.LBB1_56:                               @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading0 <- %S0
	.loc	1 200 8                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:200:8
	ldr	r0, [r8, #112]
.Ltmp203:
	.loc	1 200 8 is_stmt 0       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:200:8
	cmp	r0, #0
	beq	.LBB1_70
.Ltmp204:
.LBB1_57:                               @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading0 <- %S0
	@DEBUG_VALUE: map:in_min <- 0.000000e+00
	@DEBUG_VALUE: map:in_max <- 8.300000e-01
	@DEBUG_VALUE: map:out_min <- 0.000000e+00
	@DEBUG_VALUE: map:out_max <- 1.000000e+00
	.loc	24 73 44 is_stmt 1      @ ./include/Utilities.h:73:44
	vmul.f32	d0, d0, d12
.Ltmp205:
	.loc	1 201 18                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:201:18
	vstr	s0, [r8, #52]
	b	.LBB1_70
.Ltmp206:
.LBB1_58:                               @ %.thread
                                        @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading2 <- %S0
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading2 <- %S0
	.loc	1 174 13                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:174:13
	mov	r2, #1
.Ltmp207:
	.loc	1 158 13                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:158:13
	str	r2, [r8, #120]
	ldr	r2, [r9, #40]
.Ltmp208:
.LBB1_59:                               @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading2 <- %S0
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading2 <- %S0
	@DEBUG_VALUE: map:in_min <- 0.000000e+00
	@DEBUG_VALUE: map:in_max <- 8.300000e-01
	@DEBUG_VALUE: map:out_min <- 5.000000e-02
	@DEBUG_VALUE: map:out_max <- 1.990000e+00
	.loc	1 161 56                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:161:56
	vldr	s2, .LCPI1_18
	vmul.f32	d16, d0, d1
	vldr	s0, .LCPI1_19
.Ltmp209:
	vadd.f32	d0, d16, d0
	.loc	1 161 19 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:161:19
	vstr	s0, [r7, #40]
.Ltmp210:
.LBB1_60:                               @   in Loop: Header=BB1_2 Depth=1
	.loc	1 142 7 is_stmt 1       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:7
	movw	r3, :lower16:.L_MergedGlobals.7
	movt	r3, :upper16:.L_MergedGlobals.7
.Ltmp211:
	.loc	1 165 29                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:165:29
	vldr	s0, [r3, #4]
.Ltmp212:
	.loc	2 1400 33               @ ./include/Bela.h:1400:33
	mul	r3, r2, r1
.Ltmp213:
	.loc	1 165 29                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:165:29
	vcvt.f64.f32	d16, s0
.Ltmp214:
	.loc	2 1400 9                @ ./include/Bela.h:1400:9
	add	r3, r0, r3, lsl #2
.Ltmp215:
	.loc	1 165 27                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:165:27
	vmul.f64	d18, d16, d11
.Ltmp216:
	.loc	2 1400 9                @ ./include/Bela.h:1400:9
	vldr	s0, [r3, #4]
.Ltmp217:
	@DEBUG_VALUE: reading1 <- %S0
	@DEBUG_VALUE: map:x <- %S0
	.loc	1 165 9                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:165:9
	vcvt.f64.f32	d17, s0
	.loc	1 165 56 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:165:56
	vcmpe.f64	d17, d18
	vmrs	APSR_nzcv, fpscr
	blt	.LBB1_63
.Ltmp218:
@ BB#61:                                @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading1 <- %S0
	.loc	1 165 78 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:165:78
	vmul.f64	d16, d16, d10
	vcmpe.f64	d17, d16
	vmrs	APSR_nzcv, fpscr
	bgt	.LBB1_63
.Ltmp219:
@ BB#62:                                @ %.thread190
                                        @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading1 <- %S0
	.loc	1 174 13 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:174:13
	mov	r2, #1
.Ltmp220:
	.loc	1 166 13                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:166:13
	str	r2, [r8, #116]
	ldr	r2, [r9, #40]
	b	.LBB1_64
.Ltmp221:
.LBB1_63:                               @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading1 <- %S0
	.loc	1 168 8                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:168:8
	ldr	r3, [r8, #116]
.Ltmp222:
	.loc	1 168 8 is_stmt 0       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:168:8
	cmp	r3, #0
	beq	.LBB1_65
.Ltmp223:
.LBB1_64:                               @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading1 <- %S0
	@DEBUG_VALUE: map:in_min <- 0.000000e+00
	@DEBUG_VALUE: map:in_max <- 8.300000e-01
	@DEBUG_VALUE: map:out_min <- 0.000000e+00
	@DEBUG_VALUE: map:out_max <- 1.250000e+00
	.loc	24 73 44 is_stmt 1      @ ./include/Utilities.h:73:44
	vldr	s2, .LCPI1_20
	vmul.f32	d0, d0, d1
.Ltmp224:
	.loc	1 169 19                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:169:19
	vstr	s0, [r7, #52]
.Ltmp225:
.LBB1_65:                               @   in Loop: Header=BB1_2 Depth=1
	.loc	1 142 7                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:7
	movw	r3, :lower16:.L_MergedGlobals.7
.Ltmp226:
	.loc	2 1400 33               @ ./include/Bela.h:1400:33
	mul	r1, r2, r1
.Ltmp227:
	.loc	1 142 7                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:142:7
	movt	r3, :upper16:.L_MergedGlobals.7
.Ltmp228:
	.loc	1 173 29                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:173:29
	vldr	s0, [r3]
	vcvt.f64.f32	d16, s0
.Ltmp229:
	.loc	2 1400 9                @ ./include/Bela.h:1400:9
	add	r0, r0, r1, lsl #2
.Ltmp230:
	.loc	1 173 27                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:173:27
	vmul.f64	d18, d16, d11
.Ltmp231:
	.loc	2 1400 9                @ ./include/Bela.h:1400:9
	vldr	s0, [r0]
.Ltmp232:
	@DEBUG_VALUE: reading0 <- %S0
	@DEBUG_VALUE: map:x <- %S0
	.loc	1 173 9                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:173:9
	vcvt.f64.f32	d17, s0
	.loc	1 173 56 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:173:56
	vcmpe.f64	d17, d18
	vmrs	APSR_nzcv, fpscr
	blt	.LBB1_68
.Ltmp233:
@ BB#66:                                @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading0 <- %S0
	.loc	1 173 78 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:173:78
	vmul.f64	d16, d16, d10
	vcmpe.f64	d17, d16
	vmrs	APSR_nzcv, fpscr
	bgt	.LBB1_68
.Ltmp234:
@ BB#67:                                @ %.thread192
                                        @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading0 <- %S0
	.loc	1 174 13 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:174:13
	mov	r0, #1
	str	r0, [r8, #112]
	b	.LBB1_69
.Ltmp235:
.LBB1_68:                               @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading0 <- %S0
	.loc	1 176 8                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:176:8
	ldr	r0, [r8, #112]
.Ltmp236:
	.loc	1 176 8 is_stmt 0       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:176:8
	cmp	r0, #0
	beq	.LBB1_70
.Ltmp237:
.LBB1_69:                               @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: map:x <- %S0
	@DEBUG_VALUE: reading0 <- %S0
	@DEBUG_VALUE: map:in_min <- 0.000000e+00
	@DEBUG_VALUE: map:in_max <- 8.300000e-01
	@DEBUG_VALUE: map:out_min <- 0.000000e+00
	@DEBUG_VALUE: map:out_max <- 1.000000e+00
	.loc	24 73 44 is_stmt 1      @ ./include/Utilities.h:73:44
	vmul.f32	d0, d0, d12
.Ltmp238:
	.loc	1 177 15                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:177:15
	vstr	s0, [r7, #56]
.Ltmp239:
.LBB1_70:                               @   in Loop: Header=BB1_2 Depth=1
	.loc	1 208 18                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:208:18
	vldr	s18, [r7, #84]
	.loc	1 208 13 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:208:13
	vmov.f64	d1, #1.000000e+00
	.loc	1 208 18                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:208:18
	vcvt.f64.f32	d0, s18
	.loc	1 208 13                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:208:13
	bl	__fmod_finite
	.loc	1 209 17 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:209:17
	vcvt.s32.f32	d1, d9
	.loc	1 210 9                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:210:9
	movw	r1, :lower16:sinetable
	.loc	1 208 13                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:208:13
	vcvt.f32.f64	s0, d0
	.loc	1 210 9                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:210:9
	movt	r1, :upper16:sinetable
	.loc	1 209 17                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:209:17
	vmov	r0, s2
	.loc	1 208 11                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:208:11
	vstr	s0, [r7, #100]
.Ltmp240:
	.loc	1 131 38                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:131:38
	vmov.f32	d16, #1.000000e+00
.Ltmp241:
	.loc	1 212 43                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:212:43
	vldr	s6, .LCPI1_21
	.loc	1 210 31                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:210:31
	vsub.f32	d16, d16, d0
	.loc	1 209 10                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:209:10
	str	r0, [r7, #92]
	.loc	1 210 9                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:210:9
	add	r0, r1, r0, lsl #2
	vldr	s2, [r0]
	.loc	1 210 43 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:210:43
	vldr	s4, [r0, #4]
	.loc	1 210 27                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:210:27
	vmul.f32	d16, d1, d16
	.loc	1 210 65                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:210:65
	vmul.f32	d17, d2, d0
	.loc	1 210 41                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:210:41
	vadd.f32	d0, d17, d16
	.loc	1 212 43 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:212:43
	vmul.f32	d16, d0, d3
	.loc	1 210 7                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:210:7
	vstr	s0, [r7, #68]
	.loc	1 212 45                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:212:45
	vldr	s4, [r7, #88]
	.loc	1 212 23 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:212:23
	vldr	s2, [r7, #40]
	.loc	1 212 55                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:212:55
	vmul.f32	d16, d16, d2
	.loc	1 212 37                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:212:37
	vadd.f32	d8, d16, d1
	.loc	1 214 7 is_stmt 1       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:214:7
	vcmpe.f32	s16, s30
	.loc	1 212 21                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:212:21
	vstr	s16, [r7, #48]
	.loc	1 214 7                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:214:7
	vmrs	APSR_nzcv, fpscr
	ble	.LBB1_72
@ BB#71:                                @   in Loop: Header=BB1_2 Depth=1
	movw	r0, #17408
	vorr	d8, d15, d15
	movt	r0, #18348
	b	.LBB1_74
.LBB1_72:                               @   in Loop: Header=BB1_2 Depth=1
.Ltmp242:
	.loc	1 216 14 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:216:14
	vcmpe.f32	s16, #0
	vmrs	APSR_nzcv, fpscr
	bge	.LBB1_75
@ BB#73:                                @   in Loop: Header=BB1_2 Depth=1
	vmov.i32	d8, #0x0
	mov	r0, #0
.LBB1_74:                               @ %.preheader201
                                        @   in Loop: Header=BB1_2 Depth=1
.Ltmp243:
	.loc	1 217 22                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:217:22
	str	r0, [r7, #48]
.Ltmp244:
.LBB1_75:                               @ %.preheader201
                                        @   in Loop: Header=BB1_2 Depth=1
	.loc	1 222 44 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:222:44
	ldr	r0, [r9, #28]
	str	r0, [sp, #64]           @ 4-byte Spill
	.loc	1 222 3 is_stmt 0 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:222:3
	cmp	r0, #0
	beq	.LBB1_86
@ BB#76:                                @ %.lr.ph
                                        @   in Loop: Header=BB1_2 Depth=1
.Ltmp245:
	.loc	1 248 19 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:248:19
	vldr	s2, [r7, #76]
.Ltmp246:
	.loc	1 208 13                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:208:13
	vmov.f64	d12, #1.000000e+00
	str	r10, [sp]               @ 4-byte Spill
	ldr	r0, [r9]
.Ltmp247:
	.loc	1 226 24                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:226:24
	str	r0, [sp, #56]           @ 4-byte Spill
.Ltmp248:
	.loc	1 210 9                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:210:9
	movw	r0, :lower16:sinetable
	movt	r0, :upper16:sinetable
.Ltmp249:
	.loc	1 226 24                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:226:24
	ldr	r4, [r7]
	.loc	1 248 19                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:248:19
	vcvt.f64.f32	d0, s2
	.loc	1 249 18                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:249:18
	vcvt.s32.f32	d1, d1
	.loc	1 253 32                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:253:32
	vldr	s18, [r8, #12]
	.loc	1 260 19                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:260:19
	vldr	s22, [r7, #112]
	.loc	1 249 18                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:249:18
	vmov	r6, s2
	.loc	1 248 14                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:248:14
	vmov.f64	d1, d12
	.loc	1 251 10                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:251:10
	add	r0, r0, r6, lsl #2
	vldr	s20, [r0]
	.loc	1 248 14                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:248:14
	bl	__fmod_finite
	.loc	1 261 18                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:261:18
	vcvt.s32.f32	d1, d11
	.loc	1 263 14                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:263:14
	movw	r0, :lower16:logtable
	.loc	1 260 19                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:260:19
	vcvt.f64.f32	d16, s22
	.loc	1 263 14                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:263:14
	movt	r0, :upper16:logtable
	add	r5, r8, #104
	mov	r10, #0
	.loc	1 261 18                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:261:18
	vmov	r1, s2
	.loc	1 248 14                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:248:14
	vcvt.f32.f64	s22, d0
	.loc	1 260 14                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:260:14
	vmov.f64	d0, d16
	vmov.f64	d1, d12
	str	r1, [sp, #48]           @ 4-byte Spill
	.loc	1 263 14                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:263:14
	add	r2, r0, r1, lsl #2
	.loc	1 262 20                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:262:20
	add	r1, r1, #1
	str	r2, [sp, #44]           @ 4-byte Spill
	.loc	1 263 49                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:263:49
	add	r0, r0, r1, lsl #2
	str	r0, [sp, #36]           @ 4-byte Spill
	movw	r0, :lower16:delay_buf
	str	r1, [sp, #40]           @ 4-byte Spill
	movt	r0, :upper16:delay_buf
	add	r9, r0, #4
	.loc	1 260 14                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:260:14
	bl	__fmod_finite
.Ltmp250:
	.loc	24 73 22                @ ./include/Utilities.h:73:22
	vmov.f32	d16, #5.000000e-01
.Ltmp251:
	.loc	1 210 9                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:210:9
	movw	r0, :lower16:sinetable
.Ltmp252:
	.loc	1 131 38                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:131:38
	vmov.f32	d17, #1.000000e+00
	str	r6, [sp, #52]           @ 4-byte Spill
.Ltmp253:
	.loc	1 260 14                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:260:14
	vcvt.f32.f64	s24, d0
	.loc	1 250 20                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:250:20
	add	r6, r6, #1
.Ltmp254:
	@DEBUG_VALUE: map:out_min <- [%SP+16]
	.loc	1 210 9                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:210:9
	movt	r0, :upper16:sinetable
.Ltmp255:
	.loc	1 251 44                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:251:44
	add	r0, r0, r6, lsl #2
.Ltmp256:
	.loc	1 222 3 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:222:3
	str	r0, [sp, #12]           @ 4-byte Spill
.Ltmp257:
	.loc	24 73 22                @ ./include/Utilities.h:73:22
	vmul.f32	d16, d9, d16
.Ltmp258:
	.loc	1 263 36                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:263:36
	vsub.f32	d13, d17, d12
.Ltmp259:
	.loc	1 131 38                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:131:38
	vstr	d16, [sp, #24]          @ 8-byte Spill
.Ltmp260:
	.loc	1 253 31                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:253:31
	vsub.f32	d16, d17, d9
	.loc	1 251 32                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:251:32
	vstr	d16, [sp, #16]          @ 8-byte Spill
	vsub.f32	d16, d17, d11
	.loc	1 251 28 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:251:28
	vmul.f32	d14, d16, d10
	b	.LBB1_82
.Ltmp261:
	.p2align	2
@ BB#77:
.LCPI1_18:
	.long	1204376206              @ float 103077.109
	.p2align	2
@ BB#78:
.LCPI1_19:
	.long	1158270976              @ float 2205
	.p2align	2
@ BB#79:
.LCPI1_20:
	.long	1069598054              @ float 1.50602412
	.p2align	2
@ BB#80:
.LCPI1_21:
	.long	1137180672              @ float 400
.LBB1_81:                               @ %._crit_edge221
                                        @   in Loop: Header=BB1_82 Depth=2
.Ltmp262:
	@DEBUG_VALUE: channel <- %R10
	@DEBUG_VALUE: map:x <- %S4
	@DEBUG_VALUE: apf3y <- %S10
	.loc	1 222 33 is_stmt 1 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:222:33
	movw	r0, #25120
	add	r5, r5, #4
	movt	r0, #5
	add	r9, r9, r0
.Ltmp263:
	.loc	1 231 26                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:231:26
	vldr	s16, [r7, #48]
.Ltmp264:
.LBB1_82:                               @   Parent Loop BB1_2 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
	ldr	r0, [sp, #60]           @ 4-byte Reload
	.loc	1 226 24                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:226:24
	vmov	s2, r4
.Ltmp265:
	.loc	2 1378 9                @ ./include/Bela.h:1378:9
	ldr	r1, [sp, #68]           @ 4-byte Reload
.Ltmp266:
	.loc	1 226 24                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:226:24
	vcvt.f32.s32	d16, d1
	.loc	1 226 50 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:226:50
	vldr	s2, [r7, #52]
	vmov.f32	d10, #1.000000e+00
	ldr	r0, [r0, #24]
	vorr	d9, d15, d15
.Ltmp267:
	.loc	2 1378 9 is_stmt 1      @ ./include/Bela.h:1378:9
	mla	r0, r1, r0, r10
	ldr	r1, [sp, #56]           @ 4-byte Reload
	add	r0, r1, r0, lsl #2
.Ltmp268:
	.loc	1 228 39                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:228:39
	movw	r1, #61839
	movt	r1, #12173
.Ltmp269:
	.loc	2 1378 9                @ ./include/Bela.h:1378:9
	vldr	s0, [r0]
.Ltmp270:
	.loc	1 228 23                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:228:23
	ldr	r0, [r7, #32]
	.loc	1 223 16                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:223:16
	vstr	s0, [r5, #-48]
	.loc	1 226 34                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:226:34
	vmul.f32	d16, d16, d0
	.loc	1 226 66 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:226:66
	vldr	s4, [r5, #-24]
	.loc	1 228 39 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:228:39
	smmul	r1, r0, r1
	.loc	1 226 64                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:226:64
	vmul.f32	d17, d2, d1
	.loc	1 231 14                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:231:14
	vldr	s2, [r7, #44]
	.loc	1 226 48                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:226:48
	vadd.f32	d0, d17, d16
	.loc	1 228 39                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:228:39
	asr	r2, r1, #14
	add	r1, r2, r1, lsr #31
.Ltmp271:
	.loc	1 123 18                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:123:18
	movw	r2, #11025
.Ltmp272:
	.loc	1 231 48                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:231:48
	vsub.f32	d16, d10, d1
	.loc	1 228 39                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:228:39
	mul	r1, r1, r2
	.loc	1 231 24                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:231:24
	vmul.f32	d17, d8, d1
	.loc	1 226 22                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:226:22
	vstr	s0, [r5, #-32]
	.loc	1 228 39                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:228:39
	sub	r1, r0, r1, lsl #3
	.loc	1 228 52 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:228:52
	add	r1, r9, r1, lsl #2
	vstr	s0, [r1, #-4]
	.loc	1 231 62 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:231:62
	vldr	s0, [r7, #36]
	.loc	1 231 60 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:231:60
	vmul.f32	d16, d0, d16
	.loc	1 232 26 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:232:26
	vmov	s0, r0
	.loc	1 231 44                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:231:44
	vadd.f32	d1, d16, d17
	.loc	1 232 26                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:232:26
	vcvt.f32.s32	d16, d0
	.loc	1 232 42 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:232:42
	vsub.f32	d17, d15, d1
	.loc	1 231 12 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:231:12
	vstr	s2, [r7, #36]
	.loc	1 232 21                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:232:21
	vldr	d1, .LCPI1_8
	.loc	1 232 52 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:232:52
	vadd.f32	d0, d17, d16
	.loc	1 232 26                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:232:26
	vcvt.f64.f32	d0, s0
	.loc	1 232 21                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:232:21
	bl	__fmod_finite
	vcvt.f32.f64	s16, d0
.Ltmp273:
	.loc	1 208 13 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:208:13
	vmov.f64	d15, #1.000000e+00
.Ltmp274:
	.loc	1 233 19                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:233:19
	vcvt.f64.f32	d0, s16
	.loc	1 233 14 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:233:14
	vmov.f64	d1, d15
	.loc	1 232 19 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:232:19
	vstr	s16, [r7, #28]
	.loc	1 233 14                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:233:14
	bl	__fmod_finite
	.loc	1 236 46                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:236:46
	vcvt.s32.f32	d1, d8
	.loc	1 233 12                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:233:12
	vsub.f64	d16, d15, d0
	.loc	1 236 46                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:236:46
	vmov	r0, s2
	.loc	1 233 10                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:233:10
	vcvt.f32.f64	s0, d16
	.loc	1 236 72                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:236:72
	vsub.f32	d16, d10, d0
	.loc	1 233 8                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:233:8
	vstr	s0, [r7, #64]
	.loc	1 248 12                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:248:12
	vstr	s22, [r7, #100]
	.loc	1 236 22                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:236:22
	add	r0, r9, r0, lsl #2
	.loc	1 236 80 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:236:80
	vldr	s2, [r0, #-4]
	.loc	1 236 22                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:236:22
	vldr	s4, [r0]
	.loc	1 236 120               @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:236:120
	vmul.f32	d17, d1, d0
	.loc	1 249 11 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:249:11
	ldr	r0, [sp, #52]           @ 4-byte Reload
	.loc	1 236 66                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:236:66
	vmul.f32	d16, d16, d2
	.loc	1 249 11                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:249:11
	str	r0, [r7, #92]
	.loc	1 251 44                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:251:44
	ldr	r0, [sp, #12]           @ 4-byte Reload
	.loc	1 250 11                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:250:11
	str	r6, [r7, #96]
	.loc	1 236 78                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:236:78
	vadd.f32	d1, d16, d17
	.loc	1 236 20 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:236:20
	vstr	s2, [r5, #-16]
.Ltmp275:
	@DEBUG_VALUE: b1 <- 8.927000e-01
	@DEBUG_VALUE: a1 <- 5.365000e-02
	@DEBUG_VALUE: a0 <- 5.365000e-02
	.loc	1 243 78 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:243:78
	vldr	s4, [r5, #-8]
	.loc	1 243 28 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:243:28
	vldr	s0, [r5]
	vadd.f32	d16, d2, d1
	.loc	1 243 26                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:243:26
	vldr	s4, .LCPI1_9
	vmul.f32	d17, d0, d2
	vldr	s0, .LCPI1_10
	vmul.f32	d16, d16, d0
	.loc	1 294 86 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:86
	vadd.f32	d0, d16, d17
.Ltmp276:
	.loc	24 73 44                @ ./include/Utilities.h:73:44
	vldr	d17, [sp, #24]          @ 8-byte Reload
.Ltmp277:
	.loc	1 243 21                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:243:21
	vstr	s0, [r5, #-24]
	.loc	1 244 24                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:244:24
	vstr	s0, [r5]
	.loc	1 245 23                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:245:23
	vstr	s2, [r5, #-8]
	.loc	1 251 44                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:251:44
	vldr	s2, [r0]
	.loc	1 261 11                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:261:11
	ldr	r0, [sp, #48]           @ 4-byte Reload
	.loc	1 251 62                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:251:62
	vmul.f32	d16, d1, d11
	.loc	1 251 42 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:251:42
	vadd.f32	d1, d14, d16
.Ltmp278:
	@DEBUG_VALUE: map:out_max <- 1.000000e+00
	@DEBUG_VALUE: map:in_max <- 1.000000e+00
	@DEBUG_VALUE: map:in_min <- -1.000000e+00
	@DEBUG_VALUE: map:x <- %S2
	.loc	24 73 12 is_stmt 1      @ ./include/Utilities.h:73:12
	vadd.f32	d16, d1, d10
.Ltmp279:
	.loc	1 251 8                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:251:8
	vstr	s2, [r7, #68]
	.loc	1 261 11                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:261:11
	str	r0, [r7, #120]
	.loc	1 262 11                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:262:11
	ldr	r0, [sp, #40]           @ 4-byte Reload
.Ltmp280:
	.loc	24 73 44                @ ./include/Utilities.h:73:44
	vmul.f32	d16, d17, d16
.Ltmp281:
	.loc	1 294 114               @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:114
	vldr	d17, [sp, #16]          @ 8-byte Reload
	vadd.f32	d1, d17, d16
.Ltmp282:
	.loc	1 253 12                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:253:12
	vstr	s2, [r8, #8]
	.loc	1 260 12                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:260:12
	vstr	s24, [r8, #4]
	.loc	1 262 11                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:262:11
	str	r0, [r8]
	.loc	1 263 49                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:263:49
	ldr	r0, [sp, #36]           @ 4-byte Reload
	.loc	1 268 26                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:268:26
	vldr	s8, [r8, #36]
	.loc	1 270 22                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:270:22
	vldr	s12, [r8, #20]
	.loc	1 263 49                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:263:49
	vldr	s4, [r0]
	.loc	1 263 14 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:263:14
	ldr	r0, [sp, #44]           @ 4-byte Reload
	.loc	1 263 66                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:263:66
	vmul.f32	d16, d2, d12
	.loc	1 271 26 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:271:26
	vldr	s14, [r8, #40]
	.loc	1 273 22                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:273:22
	vldr	s10, [r8, #24]
	.loc	1 263 14                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:263:14
	vldr	s6, [r0]
	.loc	1 263 31 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:263:31
	vmul.f32	d17, d13, d3
	.loc	1 267 31 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:267:31
	vldr	s6, .LCPI1_11
	.loc	1 263 47                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:263:47
	vadd.f32	d2, d17, d16
	.loc	1 268 35                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:268:35
	vadd.f32	d17, d0, d4
	.loc	1 267 31                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:267:31
	vmul.f32	d16, d2, d3
	.loc	1 276 22                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:276:22
	vldr	s6, [r8, #28]
	.loc	1 263 12                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:263:12
	vstr	s4, [r7, #104]
	.loc	1 267 22                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:267:22
	vldr	s4, [r8, #16]
	.loc	1 267 39 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:267:39
	vmul.f32	d18, d16, d2
	.loc	1 268 64 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:268:64
	vldr	s4, [r8, #32]
	.loc	1 267 21                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:267:21
	vsub.f32	d18, d10, d18
.Ltmp283:
	@DEBUG_VALUE: ap_a1 <- undef
	.loc	1 268 35                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:268:35
	vmul.f32	d17, d18, d17
	.loc	1 270 39                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:270:39
	vmul.f32	d18, d16, d6
	.loc	1 274 26                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:274:26
	vldr	s12, [r8, #44]
	.loc	1 268 62                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:268:62
	vsub.f32	d2, d17, d2
.Ltmp284:
	@DEBUG_VALUE: apf1y <- %S4
	.loc	1 270 21                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:270:21
	vsub.f32	d18, d10, d18
.Ltmp285:
	@DEBUG_VALUE: ap_a2 <- undef
	.loc	1 271 35                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:271:35
	vadd.f32	d17, d7, d2
	vmul.f32	d17, d18, d17
	.loc	1 273 39                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:273:39
	vmul.f32	d18, d16, d5
	.loc	1 276 39                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:276:39
	vmul.f32	d16, d16, d3
	.loc	1 278 20                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:278:20
	vldr	s6, [r8, #52]
	.loc	1 271 51                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:271:51
	vsub.f32	d4, d17, d4
.Ltmp286:
	@DEBUG_VALUE: apf2y <- %S8
	.loc	1 273 21                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:273:21
	vsub.f32	d18, d10, d18
.Ltmp287:
	@DEBUG_VALUE: ap_a3 <- undef
	.loc	1 276 21                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:276:21
	vsub.f32	d16, d10, d16
.Ltmp288:
	@DEBUG_VALUE: ap_a4 <- undef
	.loc	1 274 35                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:274:35
	vadd.f32	d17, d4, d6
	vmul.f32	d17, d17, d18
	.loc	1 274 51 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:274:51
	vsub.f32	d5, d17, d7
.Ltmp289:
	@DEBUG_VALUE: apf3y <- %S10
	.loc	1 277 26 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:277:26
	vldr	s14, [r8, #48]
	.loc	1 280 13                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:280:13
	vstr	s0, [r8, #32]
	.loc	1 281 13                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:281:13
	vstr	s4, [r8, #36]
	.loc	1 282 13                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:282:13
	vstr	s8, [r8, #40]
.Ltmp290:
	.loc	24 73 22                @ ./include/Utilities.h:73:22
	vmov.f32	d4, #5.000000e-01
.Ltmp291:
	.loc	1 277 35                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:277:35
	vadd.f32	d17, d5, d7
	.loc	1 294 73                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:73
	vldr	s4, [r7, #56]
.Ltmp292:
	@DEBUG_VALUE: map:x <- %S4
	.loc	1 283 13                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:283:13
	vstr	s10, [r8, #44]
	.loc	1 287 7                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:287:7
	vcmpe.f32	s4, s8
	vmrs	APSR_nzcv, fpscr
	.loc	1 277 35                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:277:35
	vmul.f32	d16, d17, d16
	.loc	1 277 51 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:277:51
	vsub.f32	d16, d16, d6
.Ltmp293:
	@DEBUG_VALUE: apf4y <- undef
	.loc	1 278 18 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:278:18
	vmul.f32	d3, d16, d3
.Ltmp294:
	@DEBUG_VALUE: apf4y <- %S6
	.loc	1 284 13                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:284:13
	vstr	s6, [r8, #48]
	ble	.LBB1_84
.Ltmp295:
@ BB#83:                                @   in Loop: Header=BB1_82 Depth=2
	@DEBUG_VALUE: apf4y <- %S6
	@DEBUG_VALUE: map:x <- %S4
	@DEBUG_VALUE: apf3y <- %S10
	@DEBUG_VALUE: map:in_min <- 5.000000e-01
	@DEBUG_VALUE: map:in_max <- 1.000000e+00
	@DEBUG_VALUE: map:out_min <- 0.000000e+00
	@DEBUG_VALUE: map:out_max <- 1.000000e+00
	.loc	24 73 22                @ ./include/Utilities.h:73:22
	vmov.f32	d16, #5.000000e-01
.Ltmp296:
	.loc	1 131 38                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:131:38
	vmov.f32	d17, #1.000000e+00
.Ltmp297:
	.loc	24 73 12                @ ./include/Utilities.h:73:12
	vsub.f32	d16, d16, d2
	.loc	24 73 44 is_stmt 0      @ ./include/Utilities.h:73:44
	vadd.f32	d16, d16, d16
.Ltmp298:
	.loc	1 288 16 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:288:16
	vadd.f32	d10, d16, d17
.Ltmp299:
.LBB1_84:                               @   in Loop: Header=BB1_82 Depth=2
	@DEBUG_VALUE: apf4y <- %S6
	@DEBUG_VALUE: map:x <- %S4
	@DEBUG_VALUE: apf3y <- %S10
	.loc	1 288 12 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:288:12
	vstr	s20, [r7, #60]
.Ltmp300:
	.loc	1 294 103 is_stmt 1     @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:103
	vadd.f32	d17, d0, d3
	.loc	1 294 25 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:25
	ldr	r4, [r7]
.Ltmp301:
	.loc	1 131 38 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:131:38
	vmov.f32	d3, #1.000000e+00
.Ltmp302:
	.loc	1 294 20                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:20
	vmov.i32	d0, #0x0
.Ltmp303:
	.loc	1 222 69 discriminator 3 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:222:69
	add	r10, r10, #1
.Ltmp304:
	@DEBUG_VALUE: channel <- %R10
	.loc	1 294 20                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:20
	cmp	r4, #0
	vorr	d15, d9, d9
	.loc	1 294 37 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:37
	vmov	s8, r4
.Ltmp305:
	.loc	1 222 3 is_stmt 1 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:222:3
	ldr	r0, [sp, #64]           @ 4-byte Reload
.Ltmp306:
	.loc	1 294 37                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:37
	vcvt.f32.s32	d16, d4
	.loc	1 294 20 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:20
	vmoveq.f32	s0, s6
.Ltmp307:
	.loc	1 222 3 is_stmt 1 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:222:3
	cmp	r10, r0
.Ltmp308:
	.loc	1 294 83                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:83
	vmul.f32	d17, d17, d2
	.loc	1 294 47 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:47
	vmul.f32	d16, d16, d10
	.loc	1 294 112               @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:112
	vmul.f32	d17, d17, d1
	.loc	1 294 35                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:35
	vadd.f32	d16, d0, d16
	.loc	1 294 59                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:59
	vldr	s0, [r5, #-48]
	.loc	1 294 57                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:57
	vmul.f32	d16, d16, d0
	.loc	1 294 71                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:71
	vadd.f32	d0, d17, d16
	.loc	1 294 17                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:294:17
	vstr	s0, [r5, #-40]
	blo	.LBB1_81
.Ltmp309:
@ BB#85:                                @ %._crit_edge.loopexit
                                        @   in Loop: Header=BB1_2 Depth=1
	@DEBUG_VALUE: channel <- %R10
	@DEBUG_VALUE: map:x <- %S4
	@DEBUG_VALUE: apf3y <- %S10
	ldr	r9, [sp, #60]           @ 4-byte Reload
	ldr	r10, [sp]               @ 4-byte Reload
.Ltmp310:
	.loc	1 313 15 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:313:15
	vldr	s18, [r7, #84]
.Ltmp311:
	.loc	1 157 78 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:78
	vldr	d10, .LCPI1_0
	.loc	1 157 27 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:157:27
	vldr	d11, .LCPI1_1
.Ltmp312:
	.loc	24 73 44 is_stmt 1      @ ./include/Utilities.h:73:44
	vldr	s24, .LCPI1_2
.Ltmp313:
.LBB1_86:                               @ %._crit_edge
                                        @   in Loop: Header=BB1_2 Depth=1
	.loc	1 303 18                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:303:18
	ldr	r0, [r7, #32]
.Ltmp314:
	.loc	1 304 24                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:304:24
	movw	r2, #22662
	movt	r2, #1
.Ltmp315:
	.loc	1 309 15                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:309:15
	vldr	s4, .LCPI1_12
.Ltmp316:
	.loc	1 303 18                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:303:18
	add	r1, r0, #1
	.loc	1 304 8                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:304:8
	cmp	r0, r2
	movwgt	r1, #0
	vmov.f32	s6, s4
.Ltmp317:
	.loc	1 305 21                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:305:21
	str	r1, [r7, #32]
.Ltmp318:
	.loc	1 308 14                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:308:14
	vldr	s0, [r7, #72]
	.loc	1 308 11 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:308:11
	vldr	s2, [r7, #76]
	vadd.f32	d0, d1, d0
.Ltmp319:
	.loc	1 310 12 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:310:12
	vldr	s2, .LCPI1_13
	vorr	d16, d1, d1
	vadd.f32	d1, d0, d16
.Ltmp320:
	.loc	1 309 7                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:309:7
	vcmpe.f32	s0, s6
	vmrs	APSR_nzcv, fpscr
	vmovlt.f32	s2, s0
	.loc	1 308 11                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:308:11
	vstr	s2, [r7, #76]
	.loc	1 313 18                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:313:18
	vldr	s0, [r7, #80]
	.loc	1 313 15 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:313:15
	vadd.f32	d0, d9, d0
.Ltmp321:
	.loc	1 315 16 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:315:16
	vadd.f32	d1, d0, d16
.Ltmp322:
	.loc	1 314 7                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:314:7
	vcmpe.f32	s0, s6
	vmrs	APSR_nzcv, fpscr
	vmovlt.f32	s2, s0
	.loc	1 313 15                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:313:15
	vstr	s2, [r7, #84]
	.loc	1 318 34                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:318:34
	vldr	s2, [r7, #108]
.Ltmp323:
	.loc	1 323 22                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:323:22
	vldr	s0, [r7, #116]
.Ltmp324:
	.loc	1 318 11                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:318:11
	vldr	s4, [r7, #112]
	.loc	1 318 32 is_stmt 0      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:318:32
	vmul.f32	d16, d1, d0
	.loc	1 318 11                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:318:11
	vadd.f32	d1, d2, d16
	.loc	1 319 7 is_stmt 1       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:319:7
	vcmpe.f32	s2, s6
	.loc	1 318 11                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:318:11
	vstr	s2, [r7, #112]
	.loc	1 319 7                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:319:7
	vmrs	APSR_nzcv, fpscr
	bge	.LBB1_89
@ BB#87:                                @   in Loop: Header=BB1_2 Depth=1
.Ltmp325:
	.loc	1 322 14 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:322:14
	vcmpe.f32	s2, #0
	vmrs	APSR_nzcv, fpscr
	bgt	.LBB1_91
@ BB#88:                                @   in Loop: Header=BB1_2 Depth=1
.Ltmp326:
	.loc	1 323 22                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:323:22
	vneg.f32	d0, d0
	mov	r0, #0
	b	.LBB1_90
.Ltmp327:
.LBB1_89:                               @   in Loop: Header=BB1_2 Depth=1
	.loc	1 320 22                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:320:22
	vneg.f32	d0, d0
	.loc	1 321 12                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:321:12
	movw	r0, #16896
	movt	r0, #18220
.LBB1_90:                               @ %.preheader
                                        @   in Loop: Header=BB1_2 Depth=1
	.loc	1 320 22                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:320:22
	vstr	s0, [r7, #116]
.Ltmp328:
	.loc	1 324 12                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:324:12
	str	r0, [r7, #112]
.Ltmp329:
.LBB1_91:                               @ %.preheader
                                        @   in Loop: Header=BB1_2 Depth=1
	.loc	1 328 44 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:328:44
	ldr	r0, [r9, #28]
	.loc	1 328 3 is_stmt 0 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:328:3
	cmp	r0, #0
	beq	.LBB1_101
@ BB#92:                                @ %.lr.ph206
                                        @   in Loop: Header=BB1_2 Depth=1
	ldr	r1, [r9, #4]
	mov	r2, #0
	cmp	r0, #3
	bls	.LBB1_99
@ BB#93:                                @ %min.iters.checked
                                        @   in Loop: Header=BB1_2 Depth=1
	mov	r5, r0
	bfc	r5, #0, #2
	cmp	r5, #0
	beq	.LBB1_99
.Ltmp330:
@ BB#94:                                @ %vector.memcheck
                                        @   in Loop: Header=BB1_2 Depth=1
	.loc	1 123 6 is_stmt 1       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:123:6
	ldr	r3, [sp, #68]           @ 4-byte Reload
.Ltmp331:
	.loc	1 328 3 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:328:3
	add	r6, r8, r0, lsl #2
	add	r6, r6, #64
.Ltmp332:
	.loc	1 123 6                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:123:6
	lsl	r3, r3, #2
.Ltmp333:
	.loc	1 328 3 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:328:3
	mla	r3, r0, r3, r1
	cmp	r3, r6
	bhs	.LBB1_96
.Ltmp334:
@ BB#95:                                @ %vector.memcheck
                                        @   in Loop: Header=BB1_2 Depth=1
	.loc	1 123 6                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:123:6
	ldr	r6, [sp, #68]           @ 4-byte Reload
	mov	r3, #4
	add	r3, r3, r6, lsl #2
	add	r6, r8, #64
.Ltmp335:
	.loc	1 328 3 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:328:3
	mla	r3, r0, r3, r1
	cmp	r3, r6
	bhi	.LBB1_99
.LBB1_96:                               @ %vector.body.preheader
                                        @   in Loop: Header=BB1_2 Depth=1
	ldr	r2, [sp, #8]            @ 4-byte Reload
	add	r6, r8, #64
	.loc	1 328 69 is_stmt 0 discriminator 3 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:328:69
	mov	r3, r5
	.loc	1 328 3 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:328:3
	mla	r2, r0, r2, r1
.LBB1_97:                               @ %vector.body
                                        @   Parent Loop BB1_2 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
.Ltmp336:
	.loc	1 329 42 is_stmt 1      @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:329:42
	vld1.32	{d16, d17}, [r6]!
.Ltmp337:
	.loc	1 328 3 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:328:3
	subs	r3, r3, #4
.Ltmp338:
	.loc	2 1389 65               @ ./include/Bela.h:1389:65
	vst1.32	{d16, d17}, [r2]!
.Ltmp339:
	.loc	1 328 3 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:328:3
	bne	.LBB1_97
@ BB#98:                                @ %middle.block
                                        @   in Loop: Header=BB1_2 Depth=1
	mov	r2, r5
	cmp	r0, r5
	beq	.LBB1_101
.LBB1_99:                               @ %scalar.ph.preheader
                                        @   in Loop: Header=BB1_2 Depth=1
.Ltmp340:
	.loc	1 329 42                @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:329:42
	ldr	r3, [sp, #8]            @ 4-byte Reload
	mla	r1, r0, r3, r1
.LBB1_100:                              @ %scalar.ph
                                        @   Parent Loop BB1_2 Depth=1
                                        @ =>  This Inner Loop Header: Depth=2
	add	r3, r8, r2, lsl #2
	ldr	r3, [r3, #64]
.Ltmp341:
	.loc	2 1389 65               @ ./include/Bela.h:1389:65
	str	r3, [r1, r2, lsl #2]
.Ltmp342:
	.loc	1 328 69 discriminator 3 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:328:69
	add	r2, r2, #1
.Ltmp343:
	@DEBUG_VALUE: channel <- %R2
	.loc	1 328 3 is_stmt 0 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:328:3
	cmp	r2, r0
	blo	.LBB1_100
.Ltmp344:
.LBB1_101:                              @ %._crit_edge207
                                        @   in Loop: Header=BB1_2 Depth=1
	.loc	1 120 2 is_stmt 1 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:120:2
	ldr	r0, [sp, #8]            @ 4-byte Reload
	add	r10, r10, #1
	ldr	r1, [sp, #68]           @ 4-byte Reload
	add	r0, r0, #4
	str	r0, [sp, #8]            @ 4-byte Spill
	ldr	r0, [sp, #4]            @ 4-byte Reload
	mov	r2, r1
	sub	r0, r0, #1
	.loc	1 120 43 is_stmt 0 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:120:43
	str	r0, [sp, #4]            @ 4-byte Spill
	.loc	1 120 63 discriminator 3 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:120:63
	add	r2, r2, #1
.Ltmp345:
	@DEBUG_VALUE: frame_i <- %R2
	.loc	1 120 43 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:120:43
	ldr	r0, [r9, #20]
	mov	r1, r2
.Ltmp346:
	@DEBUG_VALUE: frame_i <- [%SP+68]
	.loc	1 120 2 discriminator 1 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:120:2
	cmp	r2, r0
	str	r1, [sp, #68]           @ 4-byte Spill
	blo	.LBB1_2
.Ltmp347:
.LBB1_102:                              @ %._crit_edge211
	.loc	1 333 1 is_stmt 1       @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:333:1
	sub	sp, r11, #96
	vpop	{d8, d9, d10, d11, d12, d13, d14, d15}
	add	sp, sp, #4
	pop	{r4, r5, r6, r7, r8, r9, r10, r11, pc}
.Ltmp348:
	.p2align	3
@ BB#103:
.LCPI1_0:
	.long	3435973837              @ double 1.05
	.long	1072745676
.LCPI1_1:
	.long	1717986918              @ double 0.94999999999999996
	.long	1072588390
.LCPI1_8:
	.long	0                       @ double 88200
	.long	1089833088
.LCPI1_2:
	.long	1067071365              @ float 1.20481932
.LCPI1_9:
	.long	1063553021              @ float 0.892700016
.LCPI1_10:
	.long	1029423130              @ float 0.0536499992
.LCPI1_11:
	.long	935212998               @ float 2.26757365E-5
.LCPI1_12:
	.long	1194083328              @ float 44100
.LCPI1_13:
	.long	3341566976              @ float -44100
.Lfunc_end1:
	.size	render, .Lfunc_end1-render
	.cfi_endproc
	.fnend

	.globl	cleanup
	.p2align	2
	.type	cleanup,%function
cleanup:                                @ @cleanup
.Lfunc_begin2:
	.loc	1 336 0                 @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:336:0
	.fnstart
	.cfi_startproc
@ BB#0:
	@DEBUG_VALUE: cleanup:context <- %R0
	@DEBUG_VALUE: cleanup:userData <- %R1
	.loc	1 338 1 prologue_end    @ /root/Bela/projects/MagicalTapeMachineV0.5/render.cpp:338:1
	bx	lr
.Ltmp349:
.Lfunc_end2:
	.size	cleanup, .Lfunc_end2-cleanup
	.cfi_endproc
	.fnend

	.type	delay_buf,%object       @ @delay_buf
	.bss
	.globl	delay_buf
	.p2align	2
delay_buf:
	.zero	705600
	.size	delay_buf, 705600

	.type	sinetable,%object       @ @sinetable
	.globl	sinetable
	.p2align	2
sinetable:
	.zero	176400
	.size	sinetable, 176400

	.type	logtable,%object        @ @logtable
	.globl	logtable
	.p2align	2
logtable:
	.zero	176400
	.size	logtable, 176400

	.type	.L_MergedGlobals,%object @ @_MergedGlobals
	.data
	.p2align	4
.L_MergedGlobals:
	.long	0                       @ 0x0
	.long	0                       @ 0x0
	.long	0                       @ 0x0
	.long	0                       @ 0x0
	.long	0                       @ 0x0
	.long	0                       @ 0x0
	.long	0                       @ 0x0
	.long	0                       @ float 0
	.long	0                       @ 0x0
	.long	0                       @ float 0
	.long	0                       @ float 0
	.long	953267991               @ float 9.99999974E-5
	.long	0                       @ float 0
	.long	0                       @ float 0
	.long	0                       @ float 0
	.long	0                       @ float 0
	.long	0                       @ float 0
	.long	0                       @ float 0
	.long	1090519040              @ float 8
	.long	0                       @ float 0
	.long	1056964608              @ float 0.5
	.long	0                       @ float 0
	.long	0                       @ float 0
	.long	0                       @ 0x0
	.long	0                       @ 0x0
	.long	0                       @ float 0
	.long	0                       @ float 0
	.long	1056964608              @ float 0.5
	.long	0                       @ float 0
	.long	1065353216              @ float 1
	.long	0                       @ 0x0
	.size	.L_MergedGlobals, 124

	.type	.L_MergedGlobals.6,%object @ @_MergedGlobals.6
	.p2align	4
.L_MergedGlobals.6:
	.long	0                       @ 0x0
	.long	0                       @ float 0
	.long	0                       @ float 0
	.long	0                       @ float 0
	.long	1112014848              @ float 50
	.long	1120403456              @ float 100
	.long	1128792064              @ float 200
	.long	1137180672              @ float 400
	.long	0                       @ float 0
	.long	0                       @ float 0
	.long	0                       @ float 0
	.long	0                       @ float 0
	.long	0                       @ float 0
	.long	0                       @ float 0
	.zero	8
	.zero	8
	.zero	8
	.zero	8
	.zero	8
	.zero	8
	.zero	8
	.long	1                       @ 0x1
	.long	1                       @ 0x1
	.long	1                       @ 0x1
	.size	.L_MergedGlobals.6, 124

	.type	.L_MergedGlobals.7,%object @ @_MergedGlobals.7
	.p2align	4
.L_MergedGlobals.7:
	.long	1056964608              @ float 0.5
	.long	1056964608              @ float 0.5
	.long	1056964608              @ float 0.5
	.long	1056964608              @ float 0.5
	.long	1056964608              @ float 0.5
	.long	1056964608              @ float 0.5
	.size	.L_MergedGlobals.7, 24

	.section	.debug_str,"MS",%progbits,1
.Linfo_string0:
	.asciz	"clang version 3.9.1-9 (tags/RELEASE_391/rc2)" @ string offset=0
.Linfo_string1:
	.asciz	"/root/Bela/projects/MagicalTapeMachineV0.5/build/render.cpp" @ string offset=45
.Linfo_string2:
	.asciz	"/root/Bela"            @ string offset=105
.Linfo_string3:
	.asciz	"device_on"             @ string offset=116
.Linfo_string4:
	.asciz	"int"                   @ string offset=126
.Linfo_string5:
	.asciz	"lock"                  @ string offset=130
.Linfo_string6:
	.asciz	"sizetype"              @ string offset=135
.Linfo_string7:
	.asciz	"debouncer"             @ string offset=144
.Linfo_string8:
	.asciz	"on_state"              @ string offset=154
.Linfo_string9:
	.asciz	"on_prev_state"         @ string offset=163
.Linfo_string10:
	.asciz	"shift"                 @ string offset=177
.Linfo_string11:
	.asciz	"shift_state"           @ string offset=183
.Linfo_string12:
	.asciz	"shift_prev_state"      @ string offset=195
.Linfo_string13:
	.asciz	"saved_readings"        @ string offset=212
.Linfo_string14:
	.asciz	"float"                 @ string offset=227
.Linfo_string15:
	.asciz	"in"                    @ string offset=233
.Linfo_string16:
	.asciz	"out"                   @ string offset=236
.Linfo_string17:
	.asciz	"delay_buf"             @ string offset=240
.Linfo_string18:
	.asciz	"delay_read_ptr"        @ string offset=250
.Linfo_string19:
	.asciz	"delay_write_ptr"       @ string offset=265
.Linfo_string20:
	.asciz	"delay_s"               @ string offset=281
.Linfo_string21:
	.asciz	"delay_desired"         @ string offset=289
.Linfo_string22:
	.asciz	"timeAlpha"             @ string offset=303
.Linfo_string23:
	.asciz	"delay_desired_vib"     @ string offset=313
.Linfo_string24:
	.asciz	"feedback_gain"         @ string offset=331
.Linfo_string25:
	.asciz	"drywetmix"             @ string offset=345
.Linfo_string26:
	.asciz	"drymix"                @ string offset=355
.Linfo_string27:
	.asciz	"eta"                   @ string offset=362
.Linfo_string28:
	.asciz	"delay_in"              @ string offset=366
.Linfo_string29:
	.asciz	"lpf_out"               @ string offset=375
.Linfo_string30:
	.asciz	"lpf_in"                @ string offset=383
.Linfo_string31:
	.asciz	"lpf_in_n1"             @ string offset=390
.Linfo_string32:
	.asciz	"lpf_out_n1"            @ string offset=400
.Linfo_string33:
	.asciz	"sinetable"             @ string offset=411
.Linfo_string34:
	.asciz	"lfo"                   @ string offset=421
.Linfo_string35:
	.asciz	"lfo_frequency"         @ string offset=425
.Linfo_string36:
	.asciz	"lfo_ptr"               @ string offset=439
.Linfo_string37:
	.asciz	"lfo_frequency_vib"     @ string offset=447
.Linfo_string38:
	.asciz	"lfo_ptr_vib"           @ string offset=465
.Linfo_string39:
	.asciz	"vib_depth"             @ string offset=477
.Linfo_string40:
	.asciz	"lfo_n0"                @ string offset=487
.Linfo_string41:
	.asciz	"lfo_n1"                @ string offset=494
.Linfo_string42:
	.asciz	"lfo_dif"               @ string offset=501
.Linfo_string43:
	.asciz	"logtable"              @ string offset=509
.Linfo_string44:
	.asciz	"log_lfo"               @ string offset=518
.Linfo_string45:
	.asciz	"log_frequency"         @ string offset=526
.Linfo_string46:
	.asciz	"log_ptr"               @ string offset=540
.Linfo_string47:
	.asciz	"log_ptr_direction"     @ string offset=548
.Linfo_string48:
	.asciz	"log_n0"                @ string offset=566
.Linfo_string49:
	.asciz	"log_n1"                @ string offset=573
.Linfo_string50:
	.asciz	"log_dif"               @ string offset=580
.Linfo_string51:
	.asciz	"tremolo"               @ string offset=588
.Linfo_string52:
	.asciz	"tremolo_mix"           @ string offset=596
.Linfo_string53:
	.asciz	"phaser_w1"             @ string offset=608
.Linfo_string54:
	.asciz	"phaser_w2"             @ string offset=618
.Linfo_string55:
	.asciz	"phaser_w3"             @ string offset=628
.Linfo_string56:
	.asciz	"phaser_w4"             @ string offset=638
.Linfo_string57:
	.asciz	"apf1x_n1"              @ string offset=648
.Linfo_string58:
	.asciz	"apf1y_n1"              @ string offset=657
.Linfo_string59:
	.asciz	"apf2y_n1"              @ string offset=666
.Linfo_string60:
	.asciz	"apf3y_n1"              @ string offset=675
.Linfo_string61:
	.asciz	"apf4y_n1"              @ string offset=684
.Linfo_string62:
	.asciz	"phaser_depth"          @ string offset=693
.Linfo_string63:
	.asciz	"INPUT"                 @ string offset=706
.Linfo_string64:
	.asciz	"OUTPUT"                @ string offset=712
.Linfo_string65:
	.asciz	"__gnu_debug"           @ string offset=719
.Linfo_string66:
	.asciz	"std"                   @ string offset=731
.Linfo_string67:
	.asciz	"__debug"               @ string offset=735
.Linfo_string68:
	.asciz	"__count"               @ string offset=743
.Linfo_string69:
	.asciz	"__value"               @ string offset=751
.Linfo_string70:
	.asciz	"__wch"                 @ string offset=759
.Linfo_string71:
	.asciz	"unsigned int"          @ string offset=765
.Linfo_string72:
	.asciz	"__wchb"                @ string offset=778
.Linfo_string73:
	.asciz	"char"                  @ string offset=785
.Linfo_string74:
	.asciz	"__mbstate_t"           @ string offset=790
.Linfo_string75:
	.asciz	"mbstate_t"             @ string offset=802
.Linfo_string76:
	.asciz	"wint_t"                @ string offset=812
.Linfo_string77:
	.asciz	"btowc"                 @ string offset=819
.Linfo_string78:
	.asciz	"fgetwc"                @ string offset=825
.Linfo_string79:
	.asciz	"_flags"                @ string offset=832
.Linfo_string80:
	.asciz	"_IO_read_ptr"          @ string offset=839
.Linfo_string81:
	.asciz	"_IO_read_end"          @ string offset=852
.Linfo_string82:
	.asciz	"_IO_read_base"         @ string offset=865
.Linfo_string83:
	.asciz	"_IO_write_base"        @ string offset=879
.Linfo_string84:
	.asciz	"_IO_write_ptr"         @ string offset=894
.Linfo_string85:
	.asciz	"_IO_write_end"         @ string offset=908
.Linfo_string86:
	.asciz	"_IO_buf_base"          @ string offset=922
.Linfo_string87:
	.asciz	"_IO_buf_end"           @ string offset=935
.Linfo_string88:
	.asciz	"_IO_save_base"         @ string offset=947
.Linfo_string89:
	.asciz	"_IO_backup_base"       @ string offset=961
.Linfo_string90:
	.asciz	"_IO_save_end"          @ string offset=977
.Linfo_string91:
	.asciz	"_markers"              @ string offset=990
.Linfo_string92:
	.asciz	"_IO_marker"            @ string offset=999
.Linfo_string93:
	.asciz	"_chain"                @ string offset=1010
.Linfo_string94:
	.asciz	"_fileno"               @ string offset=1017
.Linfo_string95:
	.asciz	"_flags2"               @ string offset=1025
.Linfo_string96:
	.asciz	"_old_offset"           @ string offset=1033
.Linfo_string97:
	.asciz	"long int"              @ string offset=1045
.Linfo_string98:
	.asciz	"__off_t"               @ string offset=1054
.Linfo_string99:
	.asciz	"_cur_column"           @ string offset=1062
.Linfo_string100:
	.asciz	"unsigned short"        @ string offset=1074
.Linfo_string101:
	.asciz	"_vtable_offset"        @ string offset=1089
.Linfo_string102:
	.asciz	"signed char"           @ string offset=1104
.Linfo_string103:
	.asciz	"_shortbuf"             @ string offset=1116
.Linfo_string104:
	.asciz	"_lock"                 @ string offset=1126
.Linfo_string105:
	.asciz	"_IO_lock_t"            @ string offset=1132
.Linfo_string106:
	.asciz	"_offset"               @ string offset=1143
.Linfo_string107:
	.asciz	"long long int"         @ string offset=1151
.Linfo_string108:
	.asciz	"__quad_t"              @ string offset=1165
.Linfo_string109:
	.asciz	"__off64_t"             @ string offset=1174
.Linfo_string110:
	.asciz	"__pad1"                @ string offset=1184
.Linfo_string111:
	.asciz	"__pad2"                @ string offset=1191
.Linfo_string112:
	.asciz	"__pad3"                @ string offset=1198
.Linfo_string113:
	.asciz	"__pad4"                @ string offset=1205
.Linfo_string114:
	.asciz	"__pad5"                @ string offset=1212
.Linfo_string115:
	.asciz	"size_t"                @ string offset=1219
.Linfo_string116:
	.asciz	"_mode"                 @ string offset=1226
.Linfo_string117:
	.asciz	"_unused2"              @ string offset=1232
.Linfo_string118:
	.asciz	"_IO_FILE"              @ string offset=1241
.Linfo_string119:
	.asciz	"__FILE"                @ string offset=1250
.Linfo_string120:
	.asciz	"fgetws"                @ string offset=1257
.Linfo_string121:
	.asciz	"wchar_t"               @ string offset=1264
.Linfo_string122:
	.asciz	"fputwc"                @ string offset=1272
.Linfo_string123:
	.asciz	"fputws"                @ string offset=1279
.Linfo_string124:
	.asciz	"fwide"                 @ string offset=1286
.Linfo_string125:
	.asciz	"fwprintf"              @ string offset=1292
.Linfo_string126:
	.asciz	"fwscanf"               @ string offset=1301
.Linfo_string127:
	.asciz	"getwc"                 @ string offset=1309
.Linfo_string128:
	.asciz	"getwchar"              @ string offset=1315
.Linfo_string129:
	.asciz	"mbrlen"                @ string offset=1324
.Linfo_string130:
	.asciz	"mbrtowc"               @ string offset=1331
.Linfo_string131:
	.asciz	"mbsinit"               @ string offset=1339
.Linfo_string132:
	.asciz	"mbsrtowcs"             @ string offset=1347
.Linfo_string133:
	.asciz	"putwc"                 @ string offset=1357
.Linfo_string134:
	.asciz	"putwchar"              @ string offset=1363
.Linfo_string135:
	.asciz	"swprintf"              @ string offset=1372
.Linfo_string136:
	.asciz	"swscanf"               @ string offset=1381
.Linfo_string137:
	.asciz	"ungetwc"               @ string offset=1389
.Linfo_string138:
	.asciz	"vfwprintf"             @ string offset=1397
.Linfo_string139:
	.asciz	"__ap"                  @ string offset=1407
.Linfo_string140:
	.asciz	"__va_list"             @ string offset=1412
.Linfo_string141:
	.asciz	"__builtin_va_list"     @ string offset=1422
.Linfo_string142:
	.asciz	"__gnuc_va_list"        @ string offset=1440
.Linfo_string143:
	.asciz	"vfwscanf"              @ string offset=1455
.Linfo_string144:
	.asciz	"vswprintf"             @ string offset=1464
.Linfo_string145:
	.asciz	"vswscanf"              @ string offset=1474
.Linfo_string146:
	.asciz	"vwprintf"              @ string offset=1483
.Linfo_string147:
	.asciz	"vwscanf"               @ string offset=1492
.Linfo_string148:
	.asciz	"wcrtomb"               @ string offset=1500
.Linfo_string149:
	.asciz	"wcscat"                @ string offset=1508
.Linfo_string150:
	.asciz	"wcscmp"                @ string offset=1515
.Linfo_string151:
	.asciz	"wcscoll"               @ string offset=1522
.Linfo_string152:
	.asciz	"wcscpy"                @ string offset=1530
.Linfo_string153:
	.asciz	"wcscspn"               @ string offset=1537
.Linfo_string154:
	.asciz	"wcsftime"              @ string offset=1545
.Linfo_string155:
	.asciz	"tm"                    @ string offset=1554
.Linfo_string156:
	.asciz	"wcslen"                @ string offset=1557
.Linfo_string157:
	.asciz	"wcsncat"               @ string offset=1564
.Linfo_string158:
	.asciz	"wcsncmp"               @ string offset=1572
.Linfo_string159:
	.asciz	"wcsncpy"               @ string offset=1580
.Linfo_string160:
	.asciz	"wcsrtombs"             @ string offset=1588
.Linfo_string161:
	.asciz	"wcsspn"                @ string offset=1598
.Linfo_string162:
	.asciz	"wcstod"                @ string offset=1605
.Linfo_string163:
	.asciz	"double"                @ string offset=1612
.Linfo_string164:
	.asciz	"wcstof"                @ string offset=1619
.Linfo_string165:
	.asciz	"wcstok"                @ string offset=1626
.Linfo_string166:
	.asciz	"wcstol"                @ string offset=1633
.Linfo_string167:
	.asciz	"wcstoul"               @ string offset=1640
.Linfo_string168:
	.asciz	"long unsigned int"     @ string offset=1648
.Linfo_string169:
	.asciz	"wcsxfrm"               @ string offset=1666
.Linfo_string170:
	.asciz	"wctob"                 @ string offset=1674
.Linfo_string171:
	.asciz	"wmemcmp"               @ string offset=1680
.Linfo_string172:
	.asciz	"wmemcpy"               @ string offset=1688
.Linfo_string173:
	.asciz	"wmemmove"              @ string offset=1696
.Linfo_string174:
	.asciz	"wmemset"               @ string offset=1705
.Linfo_string175:
	.asciz	"wprintf"               @ string offset=1713
.Linfo_string176:
	.asciz	"wscanf"                @ string offset=1721
.Linfo_string177:
	.asciz	"wcschr"                @ string offset=1728
.Linfo_string178:
	.asciz	"wcspbrk"               @ string offset=1735
.Linfo_string179:
	.asciz	"wcsrchr"               @ string offset=1743
.Linfo_string180:
	.asciz	"wcsstr"                @ string offset=1751
.Linfo_string181:
	.asciz	"wmemchr"               @ string offset=1758
.Linfo_string182:
	.asciz	"__gnu_cxx"             @ string offset=1766
.Linfo_string183:
	.asciz	"wcstold"               @ string offset=1776
.Linfo_string184:
	.asciz	"long double"           @ string offset=1784
.Linfo_string185:
	.asciz	"wcstoll"               @ string offset=1796
.Linfo_string186:
	.asciz	"wcstoull"              @ string offset=1804
.Linfo_string187:
	.asciz	"long long unsigned int" @ string offset=1813
.Linfo_string188:
	.asciz	"int8_t"                @ string offset=1836
.Linfo_string189:
	.asciz	"short"                 @ string offset=1843
.Linfo_string190:
	.asciz	"int16_t"               @ string offset=1849
.Linfo_string191:
	.asciz	"int32_t"               @ string offset=1857
.Linfo_string192:
	.asciz	"int64_t"               @ string offset=1865
.Linfo_string193:
	.asciz	"int_fast8_t"           @ string offset=1873
.Linfo_string194:
	.asciz	"int_fast16_t"          @ string offset=1885
.Linfo_string195:
	.asciz	"int_fast32_t"          @ string offset=1898
.Linfo_string196:
	.asciz	"int_fast64_t"          @ string offset=1911
.Linfo_string197:
	.asciz	"int_least8_t"          @ string offset=1924
.Linfo_string198:
	.asciz	"int_least16_t"         @ string offset=1937
.Linfo_string199:
	.asciz	"int_least32_t"         @ string offset=1951
.Linfo_string200:
	.asciz	"int_least64_t"         @ string offset=1965
.Linfo_string201:
	.asciz	"intmax_t"              @ string offset=1979
.Linfo_string202:
	.asciz	"intptr_t"              @ string offset=1988
.Linfo_string203:
	.asciz	"unsigned char"         @ string offset=1997
.Linfo_string204:
	.asciz	"uint8_t"               @ string offset=2011
.Linfo_string205:
	.asciz	"uint16_t"              @ string offset=2019
.Linfo_string206:
	.asciz	"uint32_t"              @ string offset=2028
.Linfo_string207:
	.asciz	"uint64_t"              @ string offset=2037
.Linfo_string208:
	.asciz	"uint_fast8_t"          @ string offset=2046
.Linfo_string209:
	.asciz	"uint_fast16_t"         @ string offset=2059
.Linfo_string210:
	.asciz	"uint_fast32_t"         @ string offset=2073
.Linfo_string211:
	.asciz	"uint_fast64_t"         @ string offset=2087
.Linfo_string212:
	.asciz	"uint_least8_t"         @ string offset=2101
.Linfo_string213:
	.asciz	"uint_least16_t"        @ string offset=2115
.Linfo_string214:
	.asciz	"uint_least32_t"        @ string offset=2130
.Linfo_string215:
	.asciz	"uint_least64_t"        @ string offset=2145
.Linfo_string216:
	.asciz	"uintmax_t"             @ string offset=2160
.Linfo_string217:
	.asciz	"uintptr_t"             @ string offset=2170
.Linfo_string218:
	.asciz	"__exception_ptr"       @ string offset=2180
.Linfo_string219:
	.asciz	"_M_exception_object"   @ string offset=2196
.Linfo_string220:
	.asciz	"exception_ptr"         @ string offset=2216
.Linfo_string221:
	.asciz	"_ZNSt15__exception_ptr13exception_ptr9_M_addrefEv" @ string offset=2230
.Linfo_string222:
	.asciz	"_M_addref"             @ string offset=2280
.Linfo_string223:
	.asciz	"_ZNSt15__exception_ptr13exception_ptr10_M_releaseEv" @ string offset=2290
.Linfo_string224:
	.asciz	"_M_release"            @ string offset=2342
.Linfo_string225:
	.asciz	"_ZNKSt15__exception_ptr13exception_ptr6_M_getEv" @ string offset=2353
.Linfo_string226:
	.asciz	"_M_get"                @ string offset=2401
.Linfo_string227:
	.asciz	"decltype(nullptr)"     @ string offset=2408
.Linfo_string228:
	.asciz	"nullptr_t"             @ string offset=2426
.Linfo_string229:
	.asciz	"_ZNSt15__exception_ptr13exception_ptraSERKS0_" @ string offset=2436
.Linfo_string230:
	.asciz	"operator="             @ string offset=2482
.Linfo_string231:
	.asciz	"_ZNSt15__exception_ptr13exception_ptraSEOS0_" @ string offset=2492
.Linfo_string232:
	.asciz	"~exception_ptr"        @ string offset=2537
.Linfo_string233:
	.asciz	"_ZNSt15__exception_ptr13exception_ptr4swapERS0_" @ string offset=2552
.Linfo_string234:
	.asciz	"swap"                  @ string offset=2600
.Linfo_string235:
	.asciz	"_ZNKSt15__exception_ptr13exception_ptrcvbEv" @ string offset=2605
.Linfo_string236:
	.asciz	"operator bool"         @ string offset=2649
.Linfo_string237:
	.asciz	"bool"                  @ string offset=2663
.Linfo_string238:
	.asciz	"_ZNKSt15__exception_ptr13exception_ptr20__cxa_exception_typeEv" @ string offset=2668
.Linfo_string239:
	.asciz	"__cxa_exception_type"  @ string offset=2731
.Linfo_string240:
	.asciz	"type_info"             @ string offset=2752
.Linfo_string241:
	.asciz	"_ZSt17rethrow_exceptionNSt15__exception_ptr13exception_ptrE" @ string offset=2762
.Linfo_string242:
	.asciz	"rethrow_exception"     @ string offset=2822
.Linfo_string243:
	.asciz	"ptrdiff_t"             @ string offset=2840
.Linfo_string244:
	.asciz	"lconv"                 @ string offset=2850
.Linfo_string245:
	.asciz	"setlocale"             @ string offset=2856
.Linfo_string246:
	.asciz	"localeconv"            @ string offset=2866
.Linfo_string247:
	.asciz	"isalnum"               @ string offset=2877
.Linfo_string248:
	.asciz	"isalpha"               @ string offset=2885
.Linfo_string249:
	.asciz	"iscntrl"               @ string offset=2893
.Linfo_string250:
	.asciz	"isdigit"               @ string offset=2901
.Linfo_string251:
	.asciz	"isgraph"               @ string offset=2909
.Linfo_string252:
	.asciz	"islower"               @ string offset=2917
.Linfo_string253:
	.asciz	"isprint"               @ string offset=2925
.Linfo_string254:
	.asciz	"ispunct"               @ string offset=2933
.Linfo_string255:
	.asciz	"isspace"               @ string offset=2941
.Linfo_string256:
	.asciz	"isupper"               @ string offset=2949
.Linfo_string257:
	.asciz	"isxdigit"              @ string offset=2957
.Linfo_string258:
	.asciz	"tolower"               @ string offset=2966
.Linfo_string259:
	.asciz	"toupper"               @ string offset=2974
.Linfo_string260:
	.asciz	"isblank"               @ string offset=2982
.Linfo_string261:
	.asciz	"div_t"                 @ string offset=2990
.Linfo_string262:
	.asciz	"quot"                  @ string offset=2996
.Linfo_string263:
	.asciz	"rem"                   @ string offset=3001
.Linfo_string264:
	.asciz	"ldiv_t"                @ string offset=3005
.Linfo_string265:
	.asciz	"abort"                 @ string offset=3012
.Linfo_string266:
	.asciz	"abs"                   @ string offset=3018
.Linfo_string267:
	.asciz	"atexit"                @ string offset=3022
.Linfo_string268:
	.asciz	"at_quick_exit"         @ string offset=3029
.Linfo_string269:
	.asciz	"atof"                  @ string offset=3043
.Linfo_string270:
	.asciz	"atoi"                  @ string offset=3048
.Linfo_string271:
	.asciz	"atol"                  @ string offset=3053
.Linfo_string272:
	.asciz	"bsearch"               @ string offset=3058
.Linfo_string273:
	.asciz	"__compar_fn_t"         @ string offset=3066
.Linfo_string274:
	.asciz	"calloc"                @ string offset=3080
.Linfo_string275:
	.asciz	"div"                   @ string offset=3087
.Linfo_string276:
	.asciz	"exit"                  @ string offset=3091
.Linfo_string277:
	.asciz	"free"                  @ string offset=3096
.Linfo_string278:
	.asciz	"getenv"                @ string offset=3101
.Linfo_string279:
	.asciz	"labs"                  @ string offset=3108
.Linfo_string280:
	.asciz	"ldiv"                  @ string offset=3113
.Linfo_string281:
	.asciz	"malloc"                @ string offset=3118
.Linfo_string282:
	.asciz	"mblen"                 @ string offset=3125
.Linfo_string283:
	.asciz	"mbstowcs"              @ string offset=3131
.Linfo_string284:
	.asciz	"mbtowc"                @ string offset=3140
.Linfo_string285:
	.asciz	"qsort"                 @ string offset=3147
.Linfo_string286:
	.asciz	"quick_exit"            @ string offset=3153
.Linfo_string287:
	.asciz	"rand"                  @ string offset=3164
.Linfo_string288:
	.asciz	"realloc"               @ string offset=3169
.Linfo_string289:
	.asciz	"srand"                 @ string offset=3177
.Linfo_string290:
	.asciz	"strtod"                @ string offset=3183
.Linfo_string291:
	.asciz	"strtol"                @ string offset=3190
.Linfo_string292:
	.asciz	"strtoul"               @ string offset=3197
.Linfo_string293:
	.asciz	"system"                @ string offset=3205
.Linfo_string294:
	.asciz	"wcstombs"              @ string offset=3212
.Linfo_string295:
	.asciz	"wctomb"                @ string offset=3221
.Linfo_string296:
	.asciz	"lldiv_t"               @ string offset=3228
.Linfo_string297:
	.asciz	"_Exit"                 @ string offset=3236
.Linfo_string298:
	.asciz	"llabs"                 @ string offset=3242
.Linfo_string299:
	.asciz	"lldiv"                 @ string offset=3248
.Linfo_string300:
	.asciz	"atoll"                 @ string offset=3254
.Linfo_string301:
	.asciz	"strtoll"               @ string offset=3260
.Linfo_string302:
	.asciz	"strtoull"              @ string offset=3268
.Linfo_string303:
	.asciz	"strtof"                @ string offset=3277
.Linfo_string304:
	.asciz	"strtold"               @ string offset=3284
.Linfo_string305:
	.asciz	"_ZN9__gnu_cxx3divExx"  @ string offset=3292
.Linfo_string306:
	.asciz	"FILE"                  @ string offset=3313
.Linfo_string307:
	.asciz	"_G_fpos_t"             @ string offset=3318
.Linfo_string308:
	.asciz	"fpos_t"                @ string offset=3328
.Linfo_string309:
	.asciz	"clearerr"              @ string offset=3335
.Linfo_string310:
	.asciz	"fclose"                @ string offset=3344
.Linfo_string311:
	.asciz	"feof"                  @ string offset=3351
.Linfo_string312:
	.asciz	"ferror"                @ string offset=3356
.Linfo_string313:
	.asciz	"fflush"                @ string offset=3363
.Linfo_string314:
	.asciz	"fgetc"                 @ string offset=3370
.Linfo_string315:
	.asciz	"fgetpos"               @ string offset=3376
.Linfo_string316:
	.asciz	"fgets"                 @ string offset=3384
.Linfo_string317:
	.asciz	"fopen"                 @ string offset=3390
.Linfo_string318:
	.asciz	"fprintf"               @ string offset=3396
.Linfo_string319:
	.asciz	"fputc"                 @ string offset=3404
.Linfo_string320:
	.asciz	"fputs"                 @ string offset=3410
.Linfo_string321:
	.asciz	"fread"                 @ string offset=3416
.Linfo_string322:
	.asciz	"freopen"               @ string offset=3422
.Linfo_string323:
	.asciz	"fscanf"                @ string offset=3430
.Linfo_string324:
	.asciz	"fseek"                 @ string offset=3437
.Linfo_string325:
	.asciz	"fsetpos"               @ string offset=3443
.Linfo_string326:
	.asciz	"ftell"                 @ string offset=3451
.Linfo_string327:
	.asciz	"fwrite"                @ string offset=3457
.Linfo_string328:
	.asciz	"getc"                  @ string offset=3464
.Linfo_string329:
	.asciz	"getchar"               @ string offset=3469
.Linfo_string330:
	.asciz	"gets"                  @ string offset=3477
.Linfo_string331:
	.asciz	"perror"                @ string offset=3482
.Linfo_string332:
	.asciz	"printf"                @ string offset=3489
.Linfo_string333:
	.asciz	"putc"                  @ string offset=3496
.Linfo_string334:
	.asciz	"putchar"               @ string offset=3501
.Linfo_string335:
	.asciz	"puts"                  @ string offset=3509
.Linfo_string336:
	.asciz	"remove"                @ string offset=3514
.Linfo_string337:
	.asciz	"rename"                @ string offset=3521
.Linfo_string338:
	.asciz	"rewind"                @ string offset=3528
.Linfo_string339:
	.asciz	"scanf"                 @ string offset=3535
.Linfo_string340:
	.asciz	"setbuf"                @ string offset=3541
.Linfo_string341:
	.asciz	"setvbuf"               @ string offset=3548
.Linfo_string342:
	.asciz	"sprintf"               @ string offset=3556
.Linfo_string343:
	.asciz	"sscanf"                @ string offset=3564
.Linfo_string344:
	.asciz	"tmpfile"               @ string offset=3571
.Linfo_string345:
	.asciz	"tmpnam"                @ string offset=3579
.Linfo_string346:
	.asciz	"ungetc"                @ string offset=3586
.Linfo_string347:
	.asciz	"vfprintf"              @ string offset=3593
.Linfo_string348:
	.asciz	"vprintf"               @ string offset=3602
.Linfo_string349:
	.asciz	"vsprintf"              @ string offset=3610
.Linfo_string350:
	.asciz	"snprintf"              @ string offset=3619
.Linfo_string351:
	.asciz	"vfscanf"               @ string offset=3628
.Linfo_string352:
	.asciz	"vscanf"                @ string offset=3636
.Linfo_string353:
	.asciz	"vsnprintf"             @ string offset=3643
.Linfo_string354:
	.asciz	"vsscanf"               @ string offset=3653
.Linfo_string355:
	.asciz	"__acos_finite"         @ string offset=3661
.Linfo_string356:
	.asciz	"acos"                  @ string offset=3675
.Linfo_string357:
	.asciz	"__asin_finite"         @ string offset=3680
.Linfo_string358:
	.asciz	"asin"                  @ string offset=3694
.Linfo_string359:
	.asciz	"atan"                  @ string offset=3699
.Linfo_string360:
	.asciz	"__atan2_finite"        @ string offset=3704
.Linfo_string361:
	.asciz	"atan2"                 @ string offset=3719
.Linfo_string362:
	.asciz	"ceil"                  @ string offset=3725
.Linfo_string363:
	.asciz	"cos"                   @ string offset=3730
.Linfo_string364:
	.asciz	"__cosh_finite"         @ string offset=3734
.Linfo_string365:
	.asciz	"cosh"                  @ string offset=3748
.Linfo_string366:
	.asciz	"__exp_finite"          @ string offset=3753
.Linfo_string367:
	.asciz	"exp"                   @ string offset=3766
.Linfo_string368:
	.asciz	"fabs"                  @ string offset=3770
.Linfo_string369:
	.asciz	"floor"                 @ string offset=3775
.Linfo_string370:
	.asciz	"__fmod_finite"         @ string offset=3781
.Linfo_string371:
	.asciz	"fmod"                  @ string offset=3795
.Linfo_string372:
	.asciz	"frexp"                 @ string offset=3800
.Linfo_string373:
	.asciz	"ldexp"                 @ string offset=3806
.Linfo_string374:
	.asciz	"__log_finite"          @ string offset=3812
.Linfo_string375:
	.asciz	"log"                   @ string offset=3825
.Linfo_string376:
	.asciz	"__log10_finite"        @ string offset=3829
.Linfo_string377:
	.asciz	"log10"                 @ string offset=3844
.Linfo_string378:
	.asciz	"modf"                  @ string offset=3850
.Linfo_string379:
	.asciz	"__pow_finite"          @ string offset=3855
.Linfo_string380:
	.asciz	"pow"                   @ string offset=3868
.Linfo_string381:
	.asciz	"sin"                   @ string offset=3872
.Linfo_string382:
	.asciz	"__sinh_finite"         @ string offset=3876
.Linfo_string383:
	.asciz	"sinh"                  @ string offset=3890
.Linfo_string384:
	.asciz	"__sqrt_finite"         @ string offset=3895
.Linfo_string385:
	.asciz	"sqrt"                  @ string offset=3909
.Linfo_string386:
	.asciz	"tan"                   @ string offset=3914
.Linfo_string387:
	.asciz	"tanh"                  @ string offset=3918
.Linfo_string388:
	.asciz	"double_t"              @ string offset=3923
.Linfo_string389:
	.asciz	"float_t"               @ string offset=3932
.Linfo_string390:
	.asciz	"__acosh_finite"        @ string offset=3940
.Linfo_string391:
	.asciz	"acosh"                 @ string offset=3955
.Linfo_string392:
	.asciz	"__acoshf_finite"       @ string offset=3961
.Linfo_string393:
	.asciz	"acoshf"                @ string offset=3977
.Linfo_string394:
	.asciz	"acoshl"                @ string offset=3984
.Linfo_string395:
	.asciz	"asinh"                 @ string offset=3991
.Linfo_string396:
	.asciz	"asinhf"                @ string offset=3997
.Linfo_string397:
	.asciz	"asinhl"                @ string offset=4004
.Linfo_string398:
	.asciz	"__atanh_finite"        @ string offset=4011
.Linfo_string399:
	.asciz	"atanh"                 @ string offset=4026
.Linfo_string400:
	.asciz	"__atanhf_finite"       @ string offset=4032
.Linfo_string401:
	.asciz	"atanhf"                @ string offset=4048
.Linfo_string402:
	.asciz	"atanhl"                @ string offset=4055
.Linfo_string403:
	.asciz	"cbrt"                  @ string offset=4062
.Linfo_string404:
	.asciz	"cbrtf"                 @ string offset=4067
.Linfo_string405:
	.asciz	"cbrtl"                 @ string offset=4073
.Linfo_string406:
	.asciz	"copysign"              @ string offset=4079
.Linfo_string407:
	.asciz	"copysignf"             @ string offset=4088
.Linfo_string408:
	.asciz	"copysignl"             @ string offset=4098
.Linfo_string409:
	.asciz	"erf"                   @ string offset=4108
.Linfo_string410:
	.asciz	"erff"                  @ string offset=4112
.Linfo_string411:
	.asciz	"erfl"                  @ string offset=4117
.Linfo_string412:
	.asciz	"erfc"                  @ string offset=4122
.Linfo_string413:
	.asciz	"erfcf"                 @ string offset=4127
.Linfo_string414:
	.asciz	"erfcl"                 @ string offset=4133
.Linfo_string415:
	.asciz	"__exp2_finite"         @ string offset=4139
.Linfo_string416:
	.asciz	"exp2"                  @ string offset=4153
.Linfo_string417:
	.asciz	"__exp2f_finite"        @ string offset=4158
.Linfo_string418:
	.asciz	"exp2f"                 @ string offset=4173
.Linfo_string419:
	.asciz	"exp2l"                 @ string offset=4179
.Linfo_string420:
	.asciz	"expm1"                 @ string offset=4185
.Linfo_string421:
	.asciz	"expm1f"                @ string offset=4191
.Linfo_string422:
	.asciz	"expm1l"                @ string offset=4198
.Linfo_string423:
	.asciz	"fdim"                  @ string offset=4205
.Linfo_string424:
	.asciz	"fdimf"                 @ string offset=4210
.Linfo_string425:
	.asciz	"fdiml"                 @ string offset=4216
.Linfo_string426:
	.asciz	"fma"                   @ string offset=4222
.Linfo_string427:
	.asciz	"fmaf"                  @ string offset=4226
.Linfo_string428:
	.asciz	"fmal"                  @ string offset=4231
.Linfo_string429:
	.asciz	"fmax"                  @ string offset=4236
.Linfo_string430:
	.asciz	"fmaxf"                 @ string offset=4241
.Linfo_string431:
	.asciz	"fmaxl"                 @ string offset=4247
.Linfo_string432:
	.asciz	"fmin"                  @ string offset=4253
.Linfo_string433:
	.asciz	"fminf"                 @ string offset=4258
.Linfo_string434:
	.asciz	"fminl"                 @ string offset=4264
.Linfo_string435:
	.asciz	"__hypot_finite"        @ string offset=4270
.Linfo_string436:
	.asciz	"hypot"                 @ string offset=4285
.Linfo_string437:
	.asciz	"__hypotf_finite"       @ string offset=4291
.Linfo_string438:
	.asciz	"hypotf"                @ string offset=4307
.Linfo_string439:
	.asciz	"hypotl"                @ string offset=4314
.Linfo_string440:
	.asciz	"ilogb"                 @ string offset=4321
.Linfo_string441:
	.asciz	"ilogbf"                @ string offset=4327
.Linfo_string442:
	.asciz	"ilogbl"                @ string offset=4334
.Linfo_string443:
	.asciz	"lgamma"                @ string offset=4341
.Linfo_string444:
	.asciz	"lgammaf"               @ string offset=4348
.Linfo_string445:
	.asciz	"lgammal"               @ string offset=4356
.Linfo_string446:
	.asciz	"llrint"                @ string offset=4364
.Linfo_string447:
	.asciz	"llrintf"               @ string offset=4371
.Linfo_string448:
	.asciz	"llrintl"               @ string offset=4379
.Linfo_string449:
	.asciz	"llround"               @ string offset=4387
.Linfo_string450:
	.asciz	"llroundf"              @ string offset=4395
.Linfo_string451:
	.asciz	"llroundl"              @ string offset=4404
.Linfo_string452:
	.asciz	"log1p"                 @ string offset=4413
.Linfo_string453:
	.asciz	"log1pf"                @ string offset=4419
.Linfo_string454:
	.asciz	"log1pl"                @ string offset=4426
.Linfo_string455:
	.asciz	"__log2_finite"         @ string offset=4433
.Linfo_string456:
	.asciz	"log2"                  @ string offset=4447
.Linfo_string457:
	.asciz	"__log2f_finite"        @ string offset=4452
.Linfo_string458:
	.asciz	"log2f"                 @ string offset=4467
.Linfo_string459:
	.asciz	"log2l"                 @ string offset=4473
.Linfo_string460:
	.asciz	"logb"                  @ string offset=4479
.Linfo_string461:
	.asciz	"logbf"                 @ string offset=4484
.Linfo_string462:
	.asciz	"logbl"                 @ string offset=4490
.Linfo_string463:
	.asciz	"lrint"                 @ string offset=4496
.Linfo_string464:
	.asciz	"lrintf"                @ string offset=4502
.Linfo_string465:
	.asciz	"lrintl"                @ string offset=4509
.Linfo_string466:
	.asciz	"lround"                @ string offset=4516
.Linfo_string467:
	.asciz	"lroundf"               @ string offset=4523
.Linfo_string468:
	.asciz	"lroundl"               @ string offset=4531
.Linfo_string469:
	.asciz	"nan"                   @ string offset=4539
.Linfo_string470:
	.asciz	"nanf"                  @ string offset=4543
.Linfo_string471:
	.asciz	"nanl"                  @ string offset=4548
.Linfo_string472:
	.asciz	"nearbyint"             @ string offset=4553
.Linfo_string473:
	.asciz	"nearbyintf"            @ string offset=4563
.Linfo_string474:
	.asciz	"nearbyintl"            @ string offset=4574
.Linfo_string475:
	.asciz	"nextafter"             @ string offset=4585
.Linfo_string476:
	.asciz	"nextafterf"            @ string offset=4595
.Linfo_string477:
	.asciz	"nextafterl"            @ string offset=4606
.Linfo_string478:
	.asciz	"nexttoward"            @ string offset=4617
.Linfo_string479:
	.asciz	"nexttowardf"           @ string offset=4628
.Linfo_string480:
	.asciz	"nexttowardl"           @ string offset=4640
.Linfo_string481:
	.asciz	"__remainder_finite"    @ string offset=4652
.Linfo_string482:
	.asciz	"remainder"             @ string offset=4671
.Linfo_string483:
	.asciz	"__remainderf_finite"   @ string offset=4681
.Linfo_string484:
	.asciz	"remainderf"            @ string offset=4701
.Linfo_string485:
	.asciz	"remainderl"            @ string offset=4712
.Linfo_string486:
	.asciz	"remquo"                @ string offset=4723
.Linfo_string487:
	.asciz	"remquof"               @ string offset=4730
.Linfo_string488:
	.asciz	"remquol"               @ string offset=4738
.Linfo_string489:
	.asciz	"rint"                  @ string offset=4746
.Linfo_string490:
	.asciz	"rintf"                 @ string offset=4751
.Linfo_string491:
	.asciz	"rintl"                 @ string offset=4757
.Linfo_string492:
	.asciz	"round"                 @ string offset=4763
.Linfo_string493:
	.asciz	"roundf"                @ string offset=4769
.Linfo_string494:
	.asciz	"roundl"                @ string offset=4776
.Linfo_string495:
	.asciz	"scalbln"               @ string offset=4783
.Linfo_string496:
	.asciz	"scalblnf"              @ string offset=4791
.Linfo_string497:
	.asciz	"scalblnl"              @ string offset=4800
.Linfo_string498:
	.asciz	"scalbn"                @ string offset=4809
.Linfo_string499:
	.asciz	"scalbnf"               @ string offset=4816
.Linfo_string500:
	.asciz	"scalbnl"               @ string offset=4824
.Linfo_string501:
	.asciz	"tgamma"                @ string offset=4832
.Linfo_string502:
	.asciz	"tgammaf"               @ string offset=4839
.Linfo_string503:
	.asciz	"tgammal"               @ string offset=4847
.Linfo_string504:
	.asciz	"trunc"                 @ string offset=4855
.Linfo_string505:
	.asciz	"truncf"                @ string offset=4861
.Linfo_string506:
	.asciz	"truncl"                @ string offset=4868
.Linfo_string507:
	.asciz	"_ZL7pinModeP11BelaContextiii" @ string offset=4875
.Linfo_string508:
	.asciz	"pinMode"               @ string offset=4904
.Linfo_string509:
	.asciz	"context"               @ string offset=4912
.Linfo_string510:
	.asciz	"audioIn"               @ string offset=4920
.Linfo_string511:
	.asciz	"audioOut"              @ string offset=4928
.Linfo_string512:
	.asciz	"analogIn"              @ string offset=4937
.Linfo_string513:
	.asciz	"analogOut"             @ string offset=4946
.Linfo_string514:
	.asciz	"digital"               @ string offset=4956
.Linfo_string515:
	.asciz	"audioFrames"           @ string offset=4964
.Linfo_string516:
	.asciz	"audioInChannels"       @ string offset=4976
.Linfo_string517:
	.asciz	"audioOutChannels"      @ string offset=4992
.Linfo_string518:
	.asciz	"audioSampleRate"       @ string offset=5009
.Linfo_string519:
	.asciz	"analogFrames"          @ string offset=5025
.Linfo_string520:
	.asciz	"analogInChannels"      @ string offset=5038
.Linfo_string521:
	.asciz	"analogOutChannels"     @ string offset=5055
.Linfo_string522:
	.asciz	"analogSampleRate"      @ string offset=5073
.Linfo_string523:
	.asciz	"digitalFrames"         @ string offset=5090
.Linfo_string524:
	.asciz	"digitalChannels"       @ string offset=5104
.Linfo_string525:
	.asciz	"digitalSampleRate"     @ string offset=5120
.Linfo_string526:
	.asciz	"audioFramesElapsed"    @ string offset=5138
.Linfo_string527:
	.asciz	"multiplexerChannels"   @ string offset=5157
.Linfo_string528:
	.asciz	"multiplexerStartingChannel" @ string offset=5177
.Linfo_string529:
	.asciz	"multiplexerAnalogIn"   @ string offset=5204
.Linfo_string530:
	.asciz	"audioExpanderEnabled"  @ string offset=5224
.Linfo_string531:
	.asciz	"flags"                 @ string offset=5245
.Linfo_string532:
	.asciz	"projectName"           @ string offset=5251
.Linfo_string533:
	.asciz	"underrunCount"         @ string offset=5263
.Linfo_string534:
	.asciz	"BelaContext"           @ string offset=5277
.Linfo_string535:
	.asciz	"frame"                 @ string offset=5289
.Linfo_string536:
	.asciz	"channel"               @ string offset=5295
.Linfo_string537:
	.asciz	"mode"                  @ string offset=5303
.Linfo_string538:
	.asciz	"f"                     @ string offset=5308
.Linfo_string539:
	.asciz	"_ZL3mapfffff"          @ string offset=5310
.Linfo_string540:
	.asciz	"map"                   @ string offset=5323
.Linfo_string541:
	.asciz	"x"                     @ string offset=5327
.Linfo_string542:
	.asciz	"in_min"                @ string offset=5329
.Linfo_string543:
	.asciz	"in_max"                @ string offset=5336
.Linfo_string544:
	.asciz	"out_min"               @ string offset=5343
.Linfo_string545:
	.asciz	"out_max"               @ string offset=5351
.Linfo_string546:
	.asciz	"_ZL11digitalReadP11BelaContextii" @ string offset=5359
.Linfo_string547:
	.asciz	"digitalRead"           @ string offset=5392
.Linfo_string548:
	.asciz	"_ZL11analogWriteP11BelaContextiif" @ string offset=5404
.Linfo_string549:
	.asciz	"analogWrite"           @ string offset=5438
.Linfo_string550:
	.asciz	"value"                 @ string offset=5450
.Linfo_string551:
	.asciz	"_ZL15analogWriteOnceP11BelaContextiif" @ string offset=5456
.Linfo_string552:
	.asciz	"analogWriteOnce"       @ string offset=5494
.Linfo_string553:
	.asciz	"_ZL10analogReadP11BelaContextii" @ string offset=5510
.Linfo_string554:
	.asciz	"analogRead"            @ string offset=5542
.Linfo_string555:
	.asciz	"_ZL9audioReadP11BelaContextii" @ string offset=5553
.Linfo_string556:
	.asciz	"audioRead"             @ string offset=5583
.Linfo_string557:
	.asciz	"_ZL10audioWriteP11BelaContextiif" @ string offset=5593
.Linfo_string558:
	.asciz	"audioWrite"            @ string offset=5626
.Linfo_string559:
	.asciz	"setup"                 @ string offset=5637
.Linfo_string560:
	.asciz	"render"                @ string offset=5643
.Linfo_string561:
	.asciz	"cleanup"               @ string offset=5650
.Linfo_string562:
	.asciz	"userData"              @ string offset=5658
.Linfo_string563:
	.asciz	"i"                     @ string offset=5667
.Linfo_string564:
	.asciz	"frame_i"               @ string offset=5669
.Linfo_string565:
	.asciz	"reading"               @ string offset=5677
.Linfo_string566:
	.asciz	"reading2"              @ string offset=5685
.Linfo_string567:
	.asciz	"reading1"              @ string offset=5694
.Linfo_string568:
	.asciz	"reading0"              @ string offset=5703
.Linfo_string569:
	.asciz	"apf3y"                 @ string offset=5712
.Linfo_string570:
	.asciz	"b1"                    @ string offset=5718
.Linfo_string571:
	.asciz	"a1"                    @ string offset=5721
.Linfo_string572:
	.asciz	"a0"                    @ string offset=5724
.Linfo_string573:
	.asciz	"ap_a1"                 @ string offset=5727
.Linfo_string574:
	.asciz	"apf1y"                 @ string offset=5733
.Linfo_string575:
	.asciz	"ap_a2"                 @ string offset=5739
.Linfo_string576:
	.asciz	"apf2y"                 @ string offset=5745
.Linfo_string577:
	.asciz	"ap_a3"                 @ string offset=5751
.Linfo_string578:
	.asciz	"ap_a4"                 @ string offset=5757
.Linfo_string579:
	.asciz	"apf4y"                 @ string offset=5763
	.section	.debug_loc,"",%progbits
.Ldebug_loc0:
	.long	.Lfunc_begin0-.Lfunc_begin0
	.long	.Ltmp16-.Lfunc_begin0
	.short	1                       @ Loc expr size
	.byte	80                      @ DW_OP_reg0
	.long	.Ltmp16-.Lfunc_begin0
	.long	.Ltmp19-.Lfunc_begin0
	.short	1                       @ Loc expr size
	.byte	84                      @ DW_OP_reg4
	.long	0
	.long	0
.Ldebug_loc1:
	.long	.Lfunc_begin0-.Lfunc_begin0
	.long	.Ltmp19-.Lfunc_begin0
	.short	1                       @ Loc expr size
	.byte	81                      @ DW_OP_reg1
	.long	0
	.long	0
.Ldebug_loc2:
	.long	.Ltmp27-.Lfunc_begin0
	.long	.Ltmp31-.Lfunc_begin0
	.short	3                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	0                       @ 0
	.byte	159                     @ DW_OP_stack_value
	.long	.Ltmp31-.Lfunc_begin0
	.long	.Ltmp34-.Lfunc_begin0
	.short	1                       @ Loc expr size
	.byte	81                      @ DW_OP_reg1
	.long	0
	.long	0
.Ldebug_loc3:
	.long	.Ltmp32-.Lfunc_begin0
	.long	.Ltmp36-.Lfunc_begin0
	.short	3                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	0                       @ 0
	.byte	159                     @ DW_OP_stack_value
	.long	.Ltmp36-.Lfunc_begin0
	.long	.Ltmp39-.Lfunc_begin0
	.short	1                       @ Loc expr size
	.byte	81                      @ DW_OP_reg1
	.long	0
	.long	0
.Ldebug_loc4:
	.long	.Ltmp37-.Lfunc_begin0
	.long	.Ltmp41-.Lfunc_begin0
	.short	3                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	0                       @ 0
	.byte	159                     @ DW_OP_stack_value
	.long	.Ltmp41-.Lfunc_begin0
	.long	.Ltmp42-.Lfunc_begin0
	.short	1                       @ Loc expr size
	.byte	81                      @ DW_OP_reg1
	.long	0
	.long	0
.Ldebug_loc5:
	.long	.Lfunc_begin1-.Lfunc_begin0
	.long	.Ltmp63-.Lfunc_begin0
	.short	1                       @ Loc expr size
	.byte	80                      @ DW_OP_reg0
	.long	.Ltmp63-.Lfunc_begin0
	.long	.Ltmp73-.Lfunc_begin0
	.short	1                       @ Loc expr size
	.byte	89                      @ DW_OP_reg9
	.long	0
	.long	0
.Ldebug_loc6:
	.long	.Lfunc_begin1-.Lfunc_begin0
	.long	.Ltmp73-.Lfunc_begin0
	.short	1                       @ Loc expr size
	.byte	81                      @ DW_OP_reg1
	.long	0
	.long	0
.Ldebug_loc7:
	.long	.Ltmp63-.Lfunc_begin0
	.long	.Ltmp345-.Lfunc_begin0
	.short	3                       @ Loc expr size
	.byte	17                      @ DW_OP_consts
	.byte	0                       @ 0
	.byte	159                     @ DW_OP_stack_value
	.long	.Ltmp345-.Lfunc_begin0
	.long	.Ltmp346-.Lfunc_begin0
	.short	1                       @ Loc expr size
	.byte	82                      @ DW_OP_reg2
	.long	.Ltmp346-.Lfunc_begin0
	.long	.Ltmp347-.Lfunc_begin0
	.short	3                       @ Loc expr size
	.byte	125                     @ DW_OP_breg13
	.byte	196                     @ 68
	.byte	0                       @ 
	.long	0
	.long	0
.Ldebug_loc8:
	.long	.Ltmp73-.Lfunc_begin0
	.long	.Ltmp98-.Lfunc_begin0
	.short	3                       @ Loc expr size
	.byte	125                     @ DW_OP_breg13
	.byte	196                     @ 68
	.byte	0                       @ 
	.long	0
	.long	0
.Ldebug_loc9:
	.long	.Ltmp90-.Lfunc_begin0
	.long	.Ltmp98-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc10:
	.long	.Ltmp92-.Lfunc_begin0
	.long	.Ltmp98-.Lfunc_begin0
	.short	1                       @ Loc expr size
	.byte	89                      @ DW_OP_reg9
	.long	0
	.long	0
.Ldebug_loc11:
	.long	.Ltmp140-.Lfunc_begin0
	.long	.Ltmp148-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc12:
	.long	.Ltmp142-.Lfunc_begin0
	.long	.Ltmp148-.Lfunc_begin0
	.short	1                       @ Loc expr size
	.byte	89                      @ DW_OP_reg9
	.long	0
	.long	0
.Ldebug_loc13:
	.long	.Ltmp165-.Lfunc_begin0
	.long	.Ltmp173-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	.Ltmp174-.Lfunc_begin0
	.long	.Ltmp177-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	.Ltmp206-.Lfunc_begin0
	.long	.Ltmp209-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc14:
	.long	.Ltmp165-.Lfunc_begin0
	.long	.Ltmp173-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	.Ltmp174-.Lfunc_begin0
	.long	.Ltmp177-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	.Ltmp206-.Lfunc_begin0
	.long	.Ltmp209-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc15:
	.long	.Ltmp165-.Lfunc_begin0
	.long	.Ltmp173-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	.Ltmp174-.Lfunc_begin0
	.long	.Ltmp177-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	.Ltmp206-.Lfunc_begin0
	.long	.Ltmp209-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc16:
	.long	.Ltmp176-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	3                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	0                       @ 0
	.byte	159                     @ DW_OP_stack_value
	.long	0
	.long	0
.Ldebug_loc17:
	.long	.Ltmp176-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	225                     @ 1062501089
	.byte	245                     @ DW_OP_stack_value
	.byte	209                     @ 
	.byte	250                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc18:
	.long	.Ltmp176-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	3                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	0                       @ 0
	.byte	159                     @ DW_OP_stack_value
	.long	0
	.long	0
.Ldebug_loc19:
	.long	.Ltmp176-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	128                     @ 1065353216
	.byte	128                     @ DW_OP_stack_value
	.byte	128                     @ 
	.byte	252                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc20:
	.long	.Ltmp184-.Lfunc_begin0
	.long	.Ltmp191-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc21:
	.long	.Ltmp184-.Lfunc_begin0
	.long	.Ltmp191-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc22:
	.long	.Ltmp190-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	3                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	0                       @ 0
	.byte	159                     @ DW_OP_stack_value
	.long	0
	.long	0
.Ldebug_loc23:
	.long	.Ltmp190-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	225                     @ 1062501089
	.byte	245                     @ DW_OP_stack_value
	.byte	209                     @ 
	.byte	250                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc24:
	.long	.Ltmp190-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	3                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	0                       @ 0
	.byte	159                     @ DW_OP_stack_value
	.long	0
	.long	0
.Ldebug_loc25:
	.long	.Ltmp190-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	128                     @ 1065353216
	.byte	128                     @ DW_OP_stack_value
	.byte	128                     @ 
	.byte	252                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc26:
	.long	.Ltmp198-.Lfunc_begin0
	.long	.Ltmp205-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc27:
	.long	.Ltmp198-.Lfunc_begin0
	.long	.Ltmp205-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc28:
	.long	.Ltmp204-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	3                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	0                       @ 0
	.byte	159                     @ DW_OP_stack_value
	.long	0
	.long	0
.Ldebug_loc29:
	.long	.Ltmp204-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	225                     @ 1062501089
	.byte	245                     @ DW_OP_stack_value
	.byte	209                     @ 
	.byte	250                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc30:
	.long	.Ltmp204-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	3                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	0                       @ 0
	.byte	159                     @ DW_OP_stack_value
	.long	0
	.long	0
.Ldebug_loc31:
	.long	.Ltmp204-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	128                     @ 1065353216
	.byte	128                     @ DW_OP_stack_value
	.byte	128                     @ 
	.byte	252                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc32:
	.long	.Ltmp217-.Lfunc_begin0
	.long	.Ltmp224-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc33:
	.long	.Ltmp217-.Lfunc_begin0
	.long	.Ltmp224-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc34:
	.long	.Ltmp223-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	3                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	0                       @ 0
	.byte	159                     @ DW_OP_stack_value
	.long	0
	.long	0
.Ldebug_loc35:
	.long	.Ltmp223-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	225                     @ 1062501089
	.byte	245                     @ DW_OP_stack_value
	.byte	209                     @ 
	.byte	250                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc36:
	.long	.Ltmp223-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	3                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	0                       @ 0
	.byte	159                     @ DW_OP_stack_value
	.long	0
	.long	0
.Ldebug_loc37:
	.long	.Ltmp223-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	128                     @ 1067450368
	.byte	128                     @ DW_OP_stack_value
	.byte	128                     @ 
	.byte	253                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc38:
	.long	.Ltmp232-.Lfunc_begin0
	.long	.Ltmp238-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc39:
	.long	.Ltmp232-.Lfunc_begin0
	.long	.Ltmp238-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	128                     @ 256
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc40:
	.long	.Ltmp237-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	3                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	0                       @ 0
	.byte	159                     @ DW_OP_stack_value
	.long	0
	.long	0
.Ldebug_loc41:
	.long	.Ltmp237-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	225                     @ 1062501089
	.byte	245                     @ DW_OP_stack_value
	.byte	209                     @ 
	.byte	250                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc42:
	.long	.Ltmp237-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	3                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	0                       @ 0
	.byte	159                     @ DW_OP_stack_value
	.long	0
	.long	0
.Ldebug_loc43:
	.long	.Ltmp237-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	128                     @ 1065353216
	.byte	128                     @ DW_OP_stack_value
	.byte	128                     @ 
	.byte	252                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc44:
	.long	.Ltmp254-.Lfunc_begin0
	.long	.Ltmp261-.Lfunc_begin0
	.short	2                       @ Loc expr size
	.byte	125                     @ DW_OP_breg13
	.byte	16                      @ 16
	.long	0
	.long	0
.Ldebug_loc45:
	.long	.Ltmp262-.Lfunc_begin0
	.long	.Ltmp264-.Lfunc_begin0
	.short	1                       @ Loc expr size
	.byte	90                      @ DW_OP_reg10
	.long	.Ltmp304-.Lfunc_begin0
	.long	.Ltmp310-.Lfunc_begin0
	.short	1                       @ Loc expr size
	.byte	90                      @ DW_OP_reg10
	.long	0
	.long	0
.Ldebug_loc46:
	.long	.Ltmp262-.Lfunc_begin0
	.long	.Ltmp264-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	130                     @ 258
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	.Ltmp292-.Lfunc_begin0
	.long	.Ltmp313-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	130                     @ 258
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc47:
	.long	.Ltmp262-.Lfunc_begin0
	.long	.Ltmp264-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	133                     @ 261
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	.Ltmp289-.Lfunc_begin0
	.long	.Ltmp313-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	133                     @ 261
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc48:
	.long	.Ltmp275-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	253                     @ 1063553021
	.byte	143                     @ DW_OP_stack_value
	.byte	146                     @ 
	.byte	251                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc49:
	.long	.Ltmp275-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	154                     @ 1029423130
	.byte	128                     @ DW_OP_stack_value
	.byte	239                     @ 
	.byte	234                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc50:
	.long	.Ltmp275-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	154                     @ 1029423130
	.byte	128                     @ DW_OP_stack_value
	.byte	239                     @ 
	.byte	234                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc51:
	.long	.Ltmp278-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	128                     @ 1065353216
	.byte	128                     @ DW_OP_stack_value
	.byte	128                     @ 
	.byte	252                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc52:
	.long	.Ltmp278-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	128                     @ 1065353216
	.byte	128                     @ DW_OP_stack_value
	.byte	128                     @ 
	.byte	252                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc53:
	.long	.Ltmp278-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	128                     @ 3212836864
	.byte	128                     @ DW_OP_stack_value
	.byte	128                     @ 
	.byte	252                     @ 
	.byte	11                      @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc54:
	.long	.Ltmp278-.Lfunc_begin0
	.long	.Ltmp282-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	129                     @ 257
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc55:
	.long	.Ltmp284-.Lfunc_begin0
	.long	.Ltmp292-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	130                     @ 258
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc56:
	.long	.Ltmp286-.Lfunc_begin0
	.long	.Ltmp291-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	132                     @ 260
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc57:
	.long	.Ltmp294-.Lfunc_begin0
	.long	.Ltmp302-.Lfunc_begin0
	.short	5                       @ Loc expr size
	.byte	144                     @ super-register DW_OP_regx
	.byte	131                     @ 259
	.byte	2                       @ DW_OP_piece
	.byte	147                     @ 4
	.byte	4                       @ 
	.long	0
	.long	0
.Ldebug_loc58:
	.long	.Ltmp295-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	128                     @ 1056964608
	.byte	128                     @ DW_OP_stack_value
	.byte	128                     @ 
	.byte	248                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc59:
	.long	.Ltmp295-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	128                     @ 1065353216
	.byte	128                     @ DW_OP_stack_value
	.byte	128                     @ 
	.byte	252                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc60:
	.long	.Ltmp295-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	3                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	0                       @ 0
	.byte	159                     @ DW_OP_stack_value
	.long	0
	.long	0
.Ldebug_loc61:
	.long	.Ltmp295-.Lfunc_begin0
	.long	.Lfunc_end1-.Lfunc_begin0
	.short	7                       @ Loc expr size
	.byte	16                      @ DW_OP_constu
	.byte	128                     @ 1065353216
	.byte	128                     @ DW_OP_stack_value
	.byte	128                     @ 
	.byte	252                     @ 
	.byte	3                       @ 
	.byte	159                     @ 
	.long	0
	.long	0
.Ldebug_loc62:
	.long	.Ltmp343-.Lfunc_begin0
	.long	.Ltmp344-.Lfunc_begin0
	.short	1                       @ Loc expr size
	.byte	82                      @ DW_OP_reg2
	.long	0
	.long	0
	.section	.debug_abbrev,"",%progbits
.Lsection_abbrev:
	.byte	1                       @ Abbreviation Code
	.byte	17                      @ DW_TAG_compile_unit
	.byte	1                       @ DW_CHILDREN_yes
	.byte	37                      @ DW_AT_producer
	.byte	14                      @ DW_FORM_strp
	.byte	19                      @ DW_AT_language
	.byte	5                       @ DW_FORM_data2
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	16                      @ DW_AT_stmt_list
	.byte	23                      @ DW_FORM_sec_offset
	.byte	27                      @ DW_AT_comp_dir
	.byte	14                      @ DW_FORM_strp
	.byte	17                      @ DW_AT_low_pc
	.byte	1                       @ DW_FORM_addr
	.byte	18                      @ DW_AT_high_pc
	.byte	6                       @ DW_FORM_data4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	2                       @ Abbreviation Code
	.byte	52                      @ DW_TAG_variable
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	2                       @ DW_AT_location
	.byte	24                      @ DW_FORM_exprloc
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	3                       @ Abbreviation Code
	.byte	36                      @ DW_TAG_base_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	62                      @ DW_AT_encoding
	.byte	11                      @ DW_FORM_data1
	.byte	11                      @ DW_AT_byte_size
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	4                       @ Abbreviation Code
	.byte	1                       @ DW_TAG_array_type
	.byte	1                       @ DW_CHILDREN_yes
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	5                       @ Abbreviation Code
	.byte	33                      @ DW_TAG_subrange_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	55                      @ DW_AT_count
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	6                       @ Abbreviation Code
	.byte	36                      @ DW_TAG_base_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	11                      @ DW_AT_byte_size
	.byte	11                      @ DW_FORM_data1
	.byte	62                      @ DW_AT_encoding
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	7                       @ Abbreviation Code
	.byte	33                      @ DW_TAG_subrange_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	55                      @ DW_AT_count
	.byte	6                       @ DW_FORM_data4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	8                       @ Abbreviation Code
	.byte	33                      @ DW_TAG_subrange_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	55                      @ DW_AT_count
	.byte	5                       @ DW_FORM_data2
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	9                       @ Abbreviation Code
	.byte	4                       @ DW_TAG_enumeration_type
	.byte	1                       @ DW_CHILDREN_yes
	.byte	11                      @ DW_AT_byte_size
	.byte	11                      @ DW_FORM_data1
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	5                       @ DW_FORM_data2
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	10                      @ Abbreviation Code
	.byte	40                      @ DW_TAG_enumerator
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	28                      @ DW_AT_const_value
	.byte	13                      @ DW_FORM_sdata
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	11                      @ Abbreviation Code
	.byte	57                      @ DW_TAG_namespace
	.byte	1                       @ DW_CHILDREN_yes
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	12                      @ Abbreviation Code
	.byte	58                      @ DW_TAG_imported_module
	.byte	0                       @ DW_CHILDREN_no
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	24                      @ DW_AT_import
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	13                      @ Abbreviation Code
	.byte	57                      @ DW_TAG_namespace
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	14                      @ Abbreviation Code
	.byte	8                       @ DW_TAG_imported_declaration
	.byte	0                       @ DW_CHILDREN_no
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	24                      @ DW_AT_import
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	15                      @ Abbreviation Code
	.byte	8                       @ DW_TAG_imported_declaration
	.byte	0                       @ DW_CHILDREN_no
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	5                       @ DW_FORM_data2
	.byte	24                      @ DW_AT_import
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	16                      @ Abbreviation Code
	.byte	2                       @ DW_TAG_class_type
	.byte	1                       @ DW_CHILDREN_yes
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	11                      @ DW_AT_byte_size
	.byte	11                      @ DW_FORM_data1
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	17                      @ Abbreviation Code
	.byte	13                      @ DW_TAG_member
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	56                      @ DW_AT_data_member_location
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	18                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	1                       @ DW_CHILDREN_yes
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	99                      @ DW_AT_explicit
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	19                      @ Abbreviation Code
	.byte	5                       @ DW_TAG_formal_parameter
	.byte	0                       @ DW_CHILDREN_no
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	52                      @ DW_AT_artificial
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	20                      @ Abbreviation Code
	.byte	5                       @ DW_TAG_formal_parameter
	.byte	0                       @ DW_CHILDREN_no
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	21                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	1                       @ DW_CHILDREN_yes
	.byte	110                     @ DW_AT_linkage_name
	.byte	14                      @ DW_FORM_strp
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	22                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	1                       @ DW_CHILDREN_yes
	.byte	110                     @ DW_AT_linkage_name
	.byte	14                      @ DW_FORM_strp
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	23                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	1                       @ DW_CHILDREN_yes
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	50                      @ DW_AT_accessibility
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	24                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	1                       @ DW_CHILDREN_yes
	.byte	110                     @ DW_AT_linkage_name
	.byte	14                      @ DW_FORM_strp
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	50                      @ DW_AT_accessibility
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	25                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	1                       @ DW_CHILDREN_yes
	.byte	110                     @ DW_AT_linkage_name
	.byte	14                      @ DW_FORM_strp
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	50                      @ DW_AT_accessibility
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	26                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	1                       @ DW_CHILDREN_yes
	.byte	110                     @ DW_AT_linkage_name
	.byte	14                      @ DW_FORM_strp
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	50                      @ DW_AT_accessibility
	.byte	11                      @ DW_FORM_data1
	.byte	99                      @ DW_AT_explicit
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	27                      @ Abbreviation Code
	.byte	22                      @ DW_TAG_typedef
	.byte	0                       @ DW_CHILDREN_no
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	28                      @ Abbreviation Code
	.byte	2                       @ DW_TAG_class_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	29                      @ Abbreviation Code
	.byte	19                      @ DW_TAG_structure_type
	.byte	1                       @ DW_CHILDREN_yes
	.byte	11                      @ DW_AT_byte_size
	.byte	11                      @ DW_FORM_data1
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	30                      @ Abbreviation Code
	.byte	23                      @ DW_TAG_union_type
	.byte	1                       @ DW_CHILDREN_yes
	.byte	11                      @ DW_AT_byte_size
	.byte	11                      @ DW_FORM_data1
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	31                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	1                       @ DW_CHILDREN_yes
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	5                       @ DW_FORM_data2
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	32                      @ Abbreviation Code
	.byte	15                      @ DW_TAG_pointer_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	33                      @ Abbreviation Code
	.byte	19                      @ DW_TAG_structure_type
	.byte	1                       @ DW_CHILDREN_yes
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	11                      @ DW_AT_byte_size
	.byte	11                      @ DW_FORM_data1
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	34                      @ Abbreviation Code
	.byte	13                      @ DW_TAG_member
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	5                       @ DW_FORM_data2
	.byte	56                      @ DW_AT_data_member_location
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	35                      @ Abbreviation Code
	.byte	19                      @ DW_TAG_structure_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	11                      @ DW_AT_byte_size
	.byte	11                      @ DW_FORM_data1
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	36                      @ Abbreviation Code
	.byte	22                      @ DW_TAG_typedef
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	37                      @ Abbreviation Code
	.byte	15                      @ DW_TAG_pointer_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	38                      @ Abbreviation Code
	.byte	55                      @ DW_TAG_restrict_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	39                      @ Abbreviation Code
	.byte	38                      @ DW_TAG_const_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	40                      @ Abbreviation Code
	.byte	24                      @ DW_TAG_unspecified_parameters
	.byte	0                       @ DW_CHILDREN_no
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	41                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	5                       @ DW_FORM_data2
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	42                      @ Abbreviation Code
	.byte	22                      @ DW_TAG_typedef
	.byte	0                       @ DW_CHILDREN_no
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	43                      @ Abbreviation Code
	.byte	57                      @ DW_TAG_namespace
	.byte	1                       @ DW_CHILDREN_yes
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	44                      @ Abbreviation Code
	.byte	19                      @ DW_TAG_structure_type
	.byte	1                       @ DW_CHILDREN_yes
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	11                      @ DW_AT_byte_size
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	45                      @ Abbreviation Code
	.byte	13                      @ DW_TAG_member
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	56                      @ DW_AT_data_member_location
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	46                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	1                       @ DW_CHILDREN_yes
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	47                      @ Abbreviation Code
	.byte	19                      @ DW_TAG_structure_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	48                      @ Abbreviation Code
	.byte	16                      @ DW_TAG_reference_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	49                      @ Abbreviation Code
	.byte	59                      @ DW_TAG_unspecified_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	50                      @ Abbreviation Code
	.byte	66                      @ DW_TAG_rvalue_reference_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	51                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	52                      @ Abbreviation Code
	.byte	19                      @ DW_TAG_structure_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	11                      @ DW_AT_byte_size
	.byte	11                      @ DW_FORM_data1
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	53                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	5                       @ DW_FORM_data2
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	54                      @ Abbreviation Code
	.byte	21                      @ DW_TAG_subroutine_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	55                      @ Abbreviation Code
	.byte	38                      @ DW_TAG_const_type
	.byte	0                       @ DW_CHILDREN_no
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	56                      @ Abbreviation Code
	.byte	22                      @ DW_TAG_typedef
	.byte	0                       @ DW_CHILDREN_no
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	5                       @ DW_FORM_data2
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	57                      @ Abbreviation Code
	.byte	21                      @ DW_TAG_subroutine_type
	.byte	1                       @ DW_CHILDREN_yes
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	58                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	1                       @ DW_CHILDREN_yes
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	5                       @ DW_FORM_data2
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	59                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	1                       @ DW_CHILDREN_yes
	.byte	110                     @ DW_AT_linkage_name
	.byte	14                      @ DW_FORM_strp
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	5                       @ DW_FORM_data2
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	60                      @ DW_AT_declaration
	.byte	25                      @ DW_FORM_flag_present
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	60                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	1                       @ DW_CHILDREN_yes
	.byte	110                     @ DW_AT_linkage_name
	.byte	14                      @ DW_FORM_strp
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	5                       @ DW_FORM_data2
	.byte	32                      @ DW_AT_inline
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	61                      @ Abbreviation Code
	.byte	5                       @ DW_TAG_formal_parameter
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	5                       @ DW_FORM_data2
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	62                      @ Abbreviation Code
	.byte	52                      @ DW_TAG_variable
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	5                       @ DW_FORM_data2
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	63                      @ Abbreviation Code
	.byte	19                      @ DW_TAG_structure_type
	.byte	1                       @ DW_CHILDREN_yes
	.byte	11                      @ DW_AT_byte_size
	.byte	5                       @ DW_FORM_data2
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	64                      @ Abbreviation Code
	.byte	13                      @ DW_TAG_member
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	5                       @ DW_FORM_data2
	.byte	56                      @ DW_AT_data_member_location
	.byte	5                       @ DW_FORM_data2
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	65                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	1                       @ DW_CHILDREN_yes
	.byte	17                      @ DW_AT_low_pc
	.byte	1                       @ DW_FORM_addr
	.byte	18                      @ DW_AT_high_pc
	.byte	6                       @ DW_FORM_data4
	.byte	64                      @ DW_AT_frame_base
	.byte	24                      @ DW_FORM_exprloc
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	66                      @ Abbreviation Code
	.byte	5                       @ DW_TAG_formal_parameter
	.byte	0                       @ DW_CHILDREN_no
	.byte	2                       @ DW_AT_location
	.byte	23                      @ DW_FORM_sec_offset
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	67                      @ Abbreviation Code
	.byte	11                      @ DW_TAG_lexical_block
	.byte	1                       @ DW_CHILDREN_yes
	.byte	17                      @ DW_AT_low_pc
	.byte	1                       @ DW_FORM_addr
	.byte	18                      @ DW_AT_high_pc
	.byte	6                       @ DW_FORM_data4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	68                      @ Abbreviation Code
	.byte	52                      @ DW_TAG_variable
	.byte	0                       @ DW_CHILDREN_no
	.byte	28                      @ DW_AT_const_value
	.byte	13                      @ DW_FORM_sdata
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	69                      @ Abbreviation Code
	.byte	29                      @ DW_TAG_inlined_subroutine
	.byte	1                       @ DW_CHILDREN_yes
	.byte	49                      @ DW_AT_abstract_origin
	.byte	19                      @ DW_FORM_ref4
	.byte	85                      @ DW_AT_ranges
	.byte	23                      @ DW_FORM_sec_offset
	.byte	88                      @ DW_AT_call_file
	.byte	11                      @ DW_FORM_data1
	.byte	89                      @ DW_AT_call_line
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	70                      @ Abbreviation Code
	.byte	5                       @ DW_TAG_formal_parameter
	.byte	0                       @ DW_CHILDREN_no
	.byte	28                      @ DW_AT_const_value
	.byte	13                      @ DW_FORM_sdata
	.byte	49                      @ DW_AT_abstract_origin
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	71                      @ Abbreviation Code
	.byte	52                      @ DW_TAG_variable
	.byte	0                       @ DW_CHILDREN_no
	.byte	2                       @ DW_AT_location
	.byte	23                      @ DW_FORM_sec_offset
	.byte	49                      @ DW_AT_abstract_origin
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	72                      @ Abbreviation Code
	.byte	29                      @ DW_TAG_inlined_subroutine
	.byte	1                       @ DW_CHILDREN_yes
	.byte	49                      @ DW_AT_abstract_origin
	.byte	19                      @ DW_FORM_ref4
	.byte	17                      @ DW_AT_low_pc
	.byte	1                       @ DW_FORM_addr
	.byte	18                      @ DW_AT_high_pc
	.byte	6                       @ DW_FORM_data4
	.byte	88                      @ DW_AT_call_file
	.byte	11                      @ DW_FORM_data1
	.byte	89                      @ DW_AT_call_line
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	73                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	1                       @ DW_CHILDREN_yes
	.byte	110                     @ DW_AT_linkage_name
	.byte	14                      @ DW_FORM_strp
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	32                      @ DW_AT_inline
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	74                      @ Abbreviation Code
	.byte	5                       @ DW_TAG_formal_parameter
	.byte	0                       @ DW_CHILDREN_no
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	75                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	1                       @ DW_CHILDREN_yes
	.byte	110                     @ DW_AT_linkage_name
	.byte	14                      @ DW_FORM_strp
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	5                       @ DW_FORM_data2
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	32                      @ DW_AT_inline
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	76                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	1                       @ DW_CHILDREN_yes
	.byte	17                      @ DW_AT_low_pc
	.byte	1                       @ DW_FORM_addr
	.byte	18                      @ DW_AT_high_pc
	.byte	6                       @ DW_FORM_data4
	.byte	64                      @ DW_AT_frame_base
	.byte	24                      @ DW_FORM_exprloc
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	77                      @ Abbreviation Code
	.byte	52                      @ DW_TAG_variable
	.byte	0                       @ DW_CHILDREN_no
	.byte	2                       @ DW_AT_location
	.byte	23                      @ DW_FORM_sec_offset
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	11                      @ DW_FORM_data1
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	78                      @ Abbreviation Code
	.byte	11                      @ DW_TAG_lexical_block
	.byte	1                       @ DW_CHILDREN_yes
	.byte	85                      @ DW_AT_ranges
	.byte	23                      @ DW_FORM_sec_offset
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	79                      @ Abbreviation Code
	.byte	29                      @ DW_TAG_inlined_subroutine
	.byte	0                       @ DW_CHILDREN_no
	.byte	49                      @ DW_AT_abstract_origin
	.byte	19                      @ DW_FORM_ref4
	.byte	85                      @ DW_AT_ranges
	.byte	23                      @ DW_FORM_sec_offset
	.byte	88                      @ DW_AT_call_file
	.byte	11                      @ DW_FORM_data1
	.byte	89                      @ DW_AT_call_line
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	80                      @ Abbreviation Code
	.byte	5                       @ DW_TAG_formal_parameter
	.byte	0                       @ DW_CHILDREN_no
	.byte	2                       @ DW_AT_location
	.byte	23                      @ DW_FORM_sec_offset
	.byte	49                      @ DW_AT_abstract_origin
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	81                      @ Abbreviation Code
	.byte	29                      @ DW_TAG_inlined_subroutine
	.byte	0                       @ DW_CHILDREN_no
	.byte	49                      @ DW_AT_abstract_origin
	.byte	19                      @ DW_FORM_ref4
	.byte	85                      @ DW_AT_ranges
	.byte	23                      @ DW_FORM_sec_offset
	.byte	88                      @ DW_AT_call_file
	.byte	11                      @ DW_FORM_data1
	.byte	89                      @ DW_AT_call_line
	.byte	5                       @ DW_FORM_data2
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	82                      @ Abbreviation Code
	.byte	29                      @ DW_TAG_inlined_subroutine
	.byte	0                       @ DW_CHILDREN_no
	.byte	49                      @ DW_AT_abstract_origin
	.byte	19                      @ DW_FORM_ref4
	.byte	17                      @ DW_AT_low_pc
	.byte	1                       @ DW_FORM_addr
	.byte	18                      @ DW_AT_high_pc
	.byte	6                       @ DW_FORM_data4
	.byte	88                      @ DW_AT_call_file
	.byte	11                      @ DW_FORM_data1
	.byte	89                      @ DW_AT_call_line
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	83                      @ Abbreviation Code
	.byte	29                      @ DW_TAG_inlined_subroutine
	.byte	0                       @ DW_CHILDREN_no
	.byte	49                      @ DW_AT_abstract_origin
	.byte	19                      @ DW_FORM_ref4
	.byte	85                      @ DW_AT_ranges
	.byte	23                      @ DW_FORM_sec_offset
	.byte	88                      @ DW_AT_call_file
	.byte	11                      @ DW_FORM_data1
	.byte	89                      @ DW_AT_call_line
	.byte	11                      @ DW_FORM_data1
	.ascii	"\266B"                 @ DW_AT_GNU_discriminator
	.byte	11                      @ DW_FORM_data1
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	84                      @ Abbreviation Code
	.byte	52                      @ DW_TAG_variable
	.byte	0                       @ DW_CHILDREN_no
	.byte	2                       @ DW_AT_location
	.byte	23                      @ DW_FORM_sec_offset
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	5                       @ DW_FORM_data2
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	85                      @ Abbreviation Code
	.byte	29                      @ DW_TAG_inlined_subroutine
	.byte	1                       @ DW_CHILDREN_yes
	.byte	49                      @ DW_AT_abstract_origin
	.byte	19                      @ DW_FORM_ref4
	.byte	17                      @ DW_AT_low_pc
	.byte	1                       @ DW_FORM_addr
	.byte	18                      @ DW_AT_high_pc
	.byte	6                       @ DW_FORM_data4
	.byte	88                      @ DW_AT_call_file
	.byte	11                      @ DW_FORM_data1
	.byte	89                      @ DW_AT_call_line
	.byte	5                       @ DW_FORM_data2
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	86                      @ Abbreviation Code
	.byte	46                      @ DW_TAG_subprogram
	.byte	1                       @ DW_CHILDREN_yes
	.byte	17                      @ DW_AT_low_pc
	.byte	1                       @ DW_FORM_addr
	.byte	18                      @ DW_AT_high_pc
	.byte	6                       @ DW_FORM_data4
	.byte	64                      @ DW_AT_frame_base
	.byte	24                      @ DW_FORM_exprloc
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	5                       @ DW_FORM_data2
	.byte	63                      @ DW_AT_external
	.byte	25                      @ DW_FORM_flag_present
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	87                      @ Abbreviation Code
	.byte	5                       @ DW_TAG_formal_parameter
	.byte	0                       @ DW_CHILDREN_no
	.byte	2                       @ DW_AT_location
	.byte	24                      @ DW_FORM_exprloc
	.byte	3                       @ DW_AT_name
	.byte	14                      @ DW_FORM_strp
	.byte	58                      @ DW_AT_decl_file
	.byte	11                      @ DW_FORM_data1
	.byte	59                      @ DW_AT_decl_line
	.byte	5                       @ DW_FORM_data2
	.byte	73                      @ DW_AT_type
	.byte	19                      @ DW_FORM_ref4
	.byte	0                       @ EOM(1)
	.byte	0                       @ EOM(2)
	.byte	0                       @ EOM(3)
	.section	.debug_info,"",%progbits
.Lsection_info:
.Lcu_begin0:
	.long	14089                   @ Length of Unit
	.short	4                       @ DWARF version number
	.long	.Lsection_abbrev        @ Offset Into Abbrev. Section
	.byte	4                       @ Address Size (in bytes)
	.byte	1                       @ Abbrev [1] 0xb:0x3702 DW_TAG_compile_unit
	.long	.Linfo_string0          @ DW_AT_producer
	.short	4                       @ DW_AT_language
	.long	.Linfo_string1          @ DW_AT_name
	.long	.Lline_table_start0     @ DW_AT_stmt_list
	.long	.Linfo_string2          @ DW_AT_comp_dir
	.long	.Lfunc_begin0           @ DW_AT_low_pc
	.long	.Lfunc_end2-.Lfunc_begin0 @ DW_AT_high_pc
	.byte	2                       @ Abbrev [2] 0x26:0x14 DW_TAG_variable
	.long	.Linfo_string3          @ DW_AT_name
	.long	58                      @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	17                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	0
	.byte	34
	.byte	3                       @ Abbrev [3] 0x3a:0x7 DW_TAG_base_type
	.long	.Linfo_string4          @ DW_AT_name
	.byte	5                       @ DW_AT_encoding
	.byte	4                       @ DW_AT_byte_size
	.byte	2                       @ Abbrev [2] 0x41:0x14 DW_TAG_variable
	.long	.Linfo_string5          @ DW_AT_name
	.long	85                      @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	18                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	112
	.byte	34
	.byte	4                       @ Abbrev [4] 0x55:0xc DW_TAG_array_type
	.long	58                      @ DW_AT_type
	.byte	5                       @ Abbrev [5] 0x5a:0x6 DW_TAG_subrange_type
	.long	97                      @ DW_AT_type
	.byte	3                       @ DW_AT_count
	.byte	0                       @ End Of Children Mark
	.byte	6                       @ Abbrev [6] 0x61:0x7 DW_TAG_base_type
	.long	.Linfo_string6          @ DW_AT_name
	.byte	8                       @ DW_AT_byte_size
	.byte	7                       @ DW_AT_encoding
	.byte	2                       @ Abbrev [2] 0x68:0x14 DW_TAG_variable
	.long	.Linfo_string7          @ DW_AT_name
	.long	58                      @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	20                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	4
	.byte	34
	.byte	2                       @ Abbrev [2] 0x7c:0x14 DW_TAG_variable
	.long	.Linfo_string8          @ DW_AT_name
	.long	58                      @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	21                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	8
	.byte	34
	.byte	2                       @ Abbrev [2] 0x90:0x14 DW_TAG_variable
	.long	.Linfo_string9          @ DW_AT_name
	.long	58                      @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	22                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	12
	.byte	34
	.byte	2                       @ Abbrev [2] 0xa4:0x14 DW_TAG_variable
	.long	.Linfo_string10         @ DW_AT_name
	.long	58                      @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	24                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	16
	.byte	34
	.byte	2                       @ Abbrev [2] 0xb8:0x14 DW_TAG_variable
	.long	.Linfo_string11         @ DW_AT_name
	.long	58                      @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	25                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	20
	.byte	34
	.byte	2                       @ Abbrev [2] 0xcc:0x14 DW_TAG_variable
	.long	.Linfo_string12         @ DW_AT_name
	.long	58                      @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	26                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	24
	.byte	34
	.byte	2                       @ Abbrev [2] 0xe0:0x14 DW_TAG_variable
	.long	.Linfo_string13         @ DW_AT_name
	.long	244                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	29                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.7
	.byte	16
	.byte	0
	.byte	34
	.byte	4                       @ Abbrev [4] 0xf4:0x12 DW_TAG_array_type
	.long	262                     @ DW_AT_type
	.byte	5                       @ Abbrev [5] 0xf9:0x6 DW_TAG_subrange_type
	.long	97                      @ DW_AT_type
	.byte	2                       @ DW_AT_count
	.byte	5                       @ Abbrev [5] 0xff:0x6 DW_TAG_subrange_type
	.long	97                      @ DW_AT_type
	.byte	3                       @ DW_AT_count
	.byte	0                       @ End Of Children Mark
	.byte	3                       @ Abbrev [3] 0x106:0x7 DW_TAG_base_type
	.long	.Linfo_string14         @ DW_AT_name
	.byte	4                       @ DW_AT_encoding
	.byte	4                       @ DW_AT_byte_size
	.byte	2                       @ Abbrev [2] 0x10d:0x14 DW_TAG_variable
	.long	.Linfo_string15         @ DW_AT_name
	.long	289                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	32                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	56
	.byte	34
	.byte	4                       @ Abbrev [4] 0x121:0xc DW_TAG_array_type
	.long	262                     @ DW_AT_type
	.byte	5                       @ Abbrev [5] 0x126:0x6 DW_TAG_subrange_type
	.long	97                      @ DW_AT_type
	.byte	2                       @ DW_AT_count
	.byte	0                       @ End Of Children Mark
	.byte	2                       @ Abbrev [2] 0x12d:0x14 DW_TAG_variable
	.long	.Linfo_string16         @ DW_AT_name
	.long	289                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	33                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	64
	.byte	34
	.byte	2                       @ Abbrev [2] 0x141:0x11 DW_TAG_variable
	.long	.Linfo_string17         @ DW_AT_name
	.long	338                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	36                      @ DW_AT_decl_line
	.byte	5                       @ DW_AT_location
	.byte	3
	.long	delay_buf
	.byte	4                       @ Abbrev [4] 0x152:0x15 DW_TAG_array_type
	.long	262                     @ DW_AT_type
	.byte	5                       @ Abbrev [5] 0x157:0x6 DW_TAG_subrange_type
	.long	97                      @ DW_AT_type
	.byte	2                       @ DW_AT_count
	.byte	7                       @ Abbrev [7] 0x15d:0x9 DW_TAG_subrange_type
	.long	97                      @ DW_AT_type
	.long	88200                   @ DW_AT_count
	.byte	0                       @ End Of Children Mark
	.byte	2                       @ Abbrev [2] 0x167:0x14 DW_TAG_variable
	.long	.Linfo_string18         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	37                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	28
	.byte	34
	.byte	2                       @ Abbrev [2] 0x17b:0x14 DW_TAG_variable
	.long	.Linfo_string19         @ DW_AT_name
	.long	58                      @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	38                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	32
	.byte	34
	.byte	2                       @ Abbrev [2] 0x18f:0x14 DW_TAG_variable
	.long	.Linfo_string20         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	39                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	36
	.byte	34
	.byte	2                       @ Abbrev [2] 0x1a3:0x14 DW_TAG_variable
	.long	.Linfo_string21         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	40                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	40
	.byte	34
	.byte	2                       @ Abbrev [2] 0x1b7:0x14 DW_TAG_variable
	.long	.Linfo_string22         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	41                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	44
	.byte	34
	.byte	2                       @ Abbrev [2] 0x1cb:0x14 DW_TAG_variable
	.long	.Linfo_string23         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	42                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	48
	.byte	34
	.byte	2                       @ Abbrev [2] 0x1df:0x14 DW_TAG_variable
	.long	.Linfo_string24         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	43                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	52
	.byte	34
	.byte	2                       @ Abbrev [2] 0x1f3:0x14 DW_TAG_variable
	.long	.Linfo_string25         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	44                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	56
	.byte	34
	.byte	2                       @ Abbrev [2] 0x207:0x14 DW_TAG_variable
	.long	.Linfo_string26         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	45                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	60
	.byte	34
	.byte	2                       @ Abbrev [2] 0x21b:0x14 DW_TAG_variable
	.long	.Linfo_string27         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	47                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	64
	.byte	34
	.byte	2                       @ Abbrev [2] 0x22f:0x14 DW_TAG_variable
	.long	.Linfo_string28         @ DW_AT_name
	.long	289                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	48                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	72
	.byte	34
	.byte	2                       @ Abbrev [2] 0x243:0x14 DW_TAG_variable
	.long	.Linfo_string29         @ DW_AT_name
	.long	289                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	51                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	80
	.byte	34
	.byte	2                       @ Abbrev [2] 0x257:0x14 DW_TAG_variable
	.long	.Linfo_string30         @ DW_AT_name
	.long	289                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	52                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	88
	.byte	34
	.byte	2                       @ Abbrev [2] 0x26b:0x14 DW_TAG_variable
	.long	.Linfo_string31         @ DW_AT_name
	.long	289                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	53                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	96
	.byte	34
	.byte	2                       @ Abbrev [2] 0x27f:0x14 DW_TAG_variable
	.long	.Linfo_string32         @ DW_AT_name
	.long	289                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	54                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	104
	.byte	34
	.byte	2                       @ Abbrev [2] 0x293:0x11 DW_TAG_variable
	.long	.Linfo_string33         @ DW_AT_name
	.long	676                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	57                      @ DW_AT_decl_line
	.byte	5                       @ DW_AT_location
	.byte	3
	.long	sinetable
	.byte	4                       @ Abbrev [4] 0x2a4:0xd DW_TAG_array_type
	.long	262                     @ DW_AT_type
	.byte	8                       @ Abbrev [8] 0x2a9:0x7 DW_TAG_subrange_type
	.long	97                      @ DW_AT_type
	.short	44100                   @ DW_AT_count
	.byte	0                       @ End Of Children Mark
	.byte	2                       @ Abbrev [2] 0x2b1:0x14 DW_TAG_variable
	.long	.Linfo_string34         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	59                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	68
	.byte	34
	.byte	2                       @ Abbrev [2] 0x2c5:0x14 DW_TAG_variable
	.long	.Linfo_string35         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	60                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	72
	.byte	34
	.byte	2                       @ Abbrev [2] 0x2d9:0x14 DW_TAG_variable
	.long	.Linfo_string36         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	61                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	76
	.byte	34
	.byte	2                       @ Abbrev [2] 0x2ed:0x14 DW_TAG_variable
	.long	.Linfo_string37         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	63                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	80
	.byte	34
	.byte	2                       @ Abbrev [2] 0x301:0x14 DW_TAG_variable
	.long	.Linfo_string38         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	64                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	84
	.byte	34
	.byte	2                       @ Abbrev [2] 0x315:0x14 DW_TAG_variable
	.long	.Linfo_string39         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	65                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	88
	.byte	34
	.byte	2                       @ Abbrev [2] 0x329:0x14 DW_TAG_variable
	.long	.Linfo_string40         @ DW_AT_name
	.long	58                      @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	67                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	92
	.byte	34
	.byte	2                       @ Abbrev [2] 0x33d:0x14 DW_TAG_variable
	.long	.Linfo_string41         @ DW_AT_name
	.long	58                      @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	68                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	96
	.byte	34
	.byte	2                       @ Abbrev [2] 0x351:0x14 DW_TAG_variable
	.long	.Linfo_string42         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	69                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	100
	.byte	34
	.byte	2                       @ Abbrev [2] 0x365:0x11 DW_TAG_variable
	.long	.Linfo_string43         @ DW_AT_name
	.long	676                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	71                      @ DW_AT_decl_line
	.byte	5                       @ DW_AT_location
	.byte	3
	.long	logtable
	.byte	2                       @ Abbrev [2] 0x376:0x14 DW_TAG_variable
	.long	.Linfo_string44         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	72                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	104
	.byte	34
	.byte	2                       @ Abbrev [2] 0x38a:0x14 DW_TAG_variable
	.long	.Linfo_string45         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	73                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	108
	.byte	34
	.byte	2                       @ Abbrev [2] 0x39e:0x14 DW_TAG_variable
	.long	.Linfo_string46         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	74                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	112
	.byte	34
	.byte	2                       @ Abbrev [2] 0x3b2:0x14 DW_TAG_variable
	.long	.Linfo_string47         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	75                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	116
	.byte	34
	.byte	2                       @ Abbrev [2] 0x3c6:0x14 DW_TAG_variable
	.long	.Linfo_string48         @ DW_AT_name
	.long	58                      @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	76                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals
	.byte	16
	.byte	120
	.byte	34
	.byte	2                       @ Abbrev [2] 0x3da:0x14 DW_TAG_variable
	.long	.Linfo_string49         @ DW_AT_name
	.long	58                      @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	77                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	0
	.byte	34
	.byte	2                       @ Abbrev [2] 0x3ee:0x14 DW_TAG_variable
	.long	.Linfo_string50         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	78                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	4
	.byte	34
	.byte	2                       @ Abbrev [2] 0x402:0x14 DW_TAG_variable
	.long	.Linfo_string51         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	80                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	8
	.byte	34
	.byte	2                       @ Abbrev [2] 0x416:0x14 DW_TAG_variable
	.long	.Linfo_string52         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	81                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	12
	.byte	34
	.byte	2                       @ Abbrev [2] 0x42a:0x14 DW_TAG_variable
	.long	.Linfo_string53         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	84                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	16
	.byte	34
	.byte	2                       @ Abbrev [2] 0x43e:0x14 DW_TAG_variable
	.long	.Linfo_string54         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	85                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	20
	.byte	34
	.byte	2                       @ Abbrev [2] 0x452:0x14 DW_TAG_variable
	.long	.Linfo_string55         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	86                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	24
	.byte	34
	.byte	2                       @ Abbrev [2] 0x466:0x14 DW_TAG_variable
	.long	.Linfo_string56         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	87                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	28
	.byte	34
	.byte	2                       @ Abbrev [2] 0x47a:0x14 DW_TAG_variable
	.long	.Linfo_string57         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	89                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	32
	.byte	34
	.byte	2                       @ Abbrev [2] 0x48e:0x14 DW_TAG_variable
	.long	.Linfo_string58         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	90                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	36
	.byte	34
	.byte	2                       @ Abbrev [2] 0x4a2:0x14 DW_TAG_variable
	.long	.Linfo_string59         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	91                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	40
	.byte	34
	.byte	2                       @ Abbrev [2] 0x4b6:0x14 DW_TAG_variable
	.long	.Linfo_string60         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	92                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	44
	.byte	34
	.byte	2                       @ Abbrev [2] 0x4ca:0x14 DW_TAG_variable
	.long	.Linfo_string61         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	93                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	48
	.byte	34
	.byte	2                       @ Abbrev [2] 0x4de:0x14 DW_TAG_variable
	.long	.Linfo_string62         @ DW_AT_name
	.long	262                     @ DW_AT_type
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_decl_file
	.byte	95                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_location
	.byte	3
	.long	.L_MergedGlobals.6
	.byte	16
	.byte	52
	.byte	34
	.byte	9                       @ Abbrev [9] 0x4f2:0x12 DW_TAG_enumeration_type
	.byte	4                       @ DW_AT_byte_size
	.byte	2                       @ DW_AT_decl_file
	.short	1077                    @ DW_AT_decl_line
	.byte	10                      @ Abbrev [10] 0x4f7:0x6 DW_TAG_enumerator
	.long	.Linfo_string63         @ DW_AT_name
	.byte	0                       @ DW_AT_const_value
	.byte	10                      @ Abbrev [10] 0x4fd:0x6 DW_TAG_enumerator
	.long	.Linfo_string64         @ DW_AT_name
	.byte	1                       @ DW_AT_const_value
	.byte	0                       @ End Of Children Mark
	.byte	11                      @ Abbrev [11] 0x504:0xf DW_TAG_namespace
	.long	.Linfo_string65         @ DW_AT_name
	.byte	3                       @ DW_AT_decl_file
	.byte	56                      @ DW_AT_decl_line
	.byte	12                      @ Abbrev [12] 0x50b:0x7 DW_TAG_imported_module
	.byte	3                       @ DW_AT_decl_file
	.byte	58                      @ DW_AT_decl_line
	.long	1306                    @ DW_AT_import
	.byte	0                       @ End Of Children Mark
	.byte	11                      @ Abbrev [11] 0x513:0xb29 DW_TAG_namespace
	.long	.Linfo_string66         @ DW_AT_name
	.byte	4                       @ DW_AT_decl_file
	.byte	199                     @ DW_AT_decl_line
	.byte	13                      @ Abbrev [13] 0x51a:0x7 DW_TAG_namespace
	.long	.Linfo_string67         @ DW_AT_name
	.byte	3                       @ DW_AT_decl_file
	.byte	50                      @ DW_AT_decl_line
	.byte	14                      @ Abbrev [14] 0x521:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	64                      @ DW_AT_decl_line
	.long	4156                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x528:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	139                     @ DW_AT_decl_line
	.long	4262                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x52f:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	141                     @ DW_AT_decl_line
	.long	4273                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x536:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	142                     @ DW_AT_decl_line
	.long	4291                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x53d:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	143                     @ DW_AT_decl_line
	.long	4832                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x544:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	144                     @ DW_AT_decl_line
	.long	4882                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x54b:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	145                     @ DW_AT_decl_line
	.long	4905                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x552:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	146                     @ DW_AT_decl_line
	.long	4943                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x559:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	147                     @ DW_AT_decl_line
	.long	4966                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x560:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	148                     @ DW_AT_decl_line
	.long	4990                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x567:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	149                     @ DW_AT_decl_line
	.long	5014                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x56e:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	150                     @ DW_AT_decl_line
	.long	5032                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x575:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	151                     @ DW_AT_decl_line
	.long	5044                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x57c:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	152                     @ DW_AT_decl_line
	.long	5097                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x583:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	153                     @ DW_AT_decl_line
	.long	5130                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x58a:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	154                     @ DW_AT_decl_line
	.long	5158                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x591:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	155                     @ DW_AT_decl_line
	.long	5201                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x598:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	156                     @ DW_AT_decl_line
	.long	5224                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x59f:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	158                     @ DW_AT_decl_line
	.long	5242                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x5a6:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	160                     @ DW_AT_decl_line
	.long	5271                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x5ad:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	161                     @ DW_AT_decl_line
	.long	5295                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x5b4:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	162                     @ DW_AT_decl_line
	.long	5318                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x5bb:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	164                     @ DW_AT_decl_line
	.long	5389                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x5c2:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	167                     @ DW_AT_decl_line
	.long	5417                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x5c9:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	170                     @ DW_AT_decl_line
	.long	5450                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x5d0:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	172                     @ DW_AT_decl_line
	.long	5478                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x5d7:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	174                     @ DW_AT_decl_line
	.long	5501                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x5de:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	176                     @ DW_AT_decl_line
	.long	5524                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x5e5:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	177                     @ DW_AT_decl_line
	.long	5557                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x5ec:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	178                     @ DW_AT_decl_line
	.long	5579                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x5f3:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	179                     @ DW_AT_decl_line
	.long	5601                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x5fa:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	180                     @ DW_AT_decl_line
	.long	5623                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x601:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	181                     @ DW_AT_decl_line
	.long	5645                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x608:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	182                     @ DW_AT_decl_line
	.long	5667                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x60f:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	183                     @ DW_AT_decl_line
	.long	5720                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x616:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	184                     @ DW_AT_decl_line
	.long	5738                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x61d:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	185                     @ DW_AT_decl_line
	.long	5765                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x624:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	186                     @ DW_AT_decl_line
	.long	5792                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x62b:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	187                     @ DW_AT_decl_line
	.long	5819                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x632:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	188                     @ DW_AT_decl_line
	.long	5862                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x639:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	189                     @ DW_AT_decl_line
	.long	5885                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x640:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	191                     @ DW_AT_decl_line
	.long	5925                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x647:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	193                     @ DW_AT_decl_line
	.long	5948                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x64e:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	194                     @ DW_AT_decl_line
	.long	5976                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x655:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	195                     @ DW_AT_decl_line
	.long	6004                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x65c:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	196                     @ DW_AT_decl_line
	.long	6039                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x663:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	197                     @ DW_AT_decl_line
	.long	6066                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x66a:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	198                     @ DW_AT_decl_line
	.long	6084                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x671:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	199                     @ DW_AT_decl_line
	.long	6112                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x678:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	200                     @ DW_AT_decl_line
	.long	6140                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x67f:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	201                     @ DW_AT_decl_line
	.long	6168                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x686:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	202                     @ DW_AT_decl_line
	.long	6196                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x68d:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	203                     @ DW_AT_decl_line
	.long	6215                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x694:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	204                     @ DW_AT_decl_line
	.long	6234                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x69b:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	205                     @ DW_AT_decl_line
	.long	6256                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x6a2:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	206                     @ DW_AT_decl_line
	.long	6279                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x6a9:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	207                     @ DW_AT_decl_line
	.long	6301                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x6b0:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	208                     @ DW_AT_decl_line
	.long	6324                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x6b7:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	264                     @ DW_AT_decl_line
	.long	6521                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x6bf:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	265                     @ DW_AT_decl_line
	.long	6551                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x6c7:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	266                     @ DW_AT_decl_line
	.long	6579                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x6cf:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	280                     @ DW_AT_decl_line
	.long	5925                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x6d7:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	283                     @ DW_AT_decl_line
	.long	5389                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x6df:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	286                     @ DW_AT_decl_line
	.long	5450                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x6e7:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	289                     @ DW_AT_decl_line
	.long	5501                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x6ef:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	293                     @ DW_AT_decl_line
	.long	6521                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x6f7:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	294                     @ DW_AT_decl_line
	.long	6551                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x6ff:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	295                     @ DW_AT_decl_line
	.long	6579                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x707:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	48                      @ DW_AT_decl_line
	.long	6614                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x70e:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	49                      @ DW_AT_decl_line
	.long	6625                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x715:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	50                      @ DW_AT_decl_line
	.long	6643                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x71c:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	51                      @ DW_AT_decl_line
	.long	6654                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x723:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	53                      @ DW_AT_decl_line
	.long	6665                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x72a:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	54                      @ DW_AT_decl_line
	.long	6676                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x731:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	55                      @ DW_AT_decl_line
	.long	6687                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x738:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	56                      @ DW_AT_decl_line
	.long	6698                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x73f:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	58                      @ DW_AT_decl_line
	.long	6709                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x746:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	59                      @ DW_AT_decl_line
	.long	6720                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x74d:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	60                      @ DW_AT_decl_line
	.long	6731                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x754:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	61                      @ DW_AT_decl_line
	.long	6742                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x75b:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	63                      @ DW_AT_decl_line
	.long	6753                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x762:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	64                      @ DW_AT_decl_line
	.long	6764                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x769:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	66                      @ DW_AT_decl_line
	.long	6775                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x770:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	67                      @ DW_AT_decl_line
	.long	6793                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x777:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	68                      @ DW_AT_decl_line
	.long	6804                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x77e:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	69                      @ DW_AT_decl_line
	.long	6815                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x785:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	71                      @ DW_AT_decl_line
	.long	6826                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x78c:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	72                      @ DW_AT_decl_line
	.long	6837                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x793:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	73                      @ DW_AT_decl_line
	.long	6848                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x79a:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	74                      @ DW_AT_decl_line
	.long	6859                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x7a1:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	76                      @ DW_AT_decl_line
	.long	6870                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x7a8:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	77                      @ DW_AT_decl_line
	.long	6881                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x7af:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	78                      @ DW_AT_decl_line
	.long	6892                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x7b6:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	79                      @ DW_AT_decl_line
	.long	6903                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x7bd:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	81                      @ DW_AT_decl_line
	.long	6914                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x7c4:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	82                      @ DW_AT_decl_line
	.long	6925                    @ DW_AT_import
	.byte	11                      @ Abbrev [11] 0x7cb:0x13b DW_TAG_namespace
	.long	.Linfo_string218        @ DW_AT_name
	.byte	12                      @ DW_AT_decl_file
	.byte	55                      @ DW_AT_decl_line
	.byte	16                      @ Abbrev [16] 0x7d2:0x12c DW_TAG_class_type
	.long	.Linfo_string220        @ DW_AT_name
	.byte	4                       @ DW_AT_byte_size
	.byte	12                      @ DW_AT_decl_file
	.byte	79                      @ DW_AT_decl_line
	.byte	17                      @ Abbrev [17] 0x7da:0xc DW_TAG_member
	.long	.Linfo_string219        @ DW_AT_name
	.long	4808                    @ DW_AT_type
	.byte	12                      @ DW_AT_decl_file
	.byte	81                      @ DW_AT_decl_line
	.byte	0                       @ DW_AT_data_member_location
	.byte	18                      @ Abbrev [18] 0x7e6:0x12 DW_TAG_subprogram
	.long	.Linfo_string220        @ DW_AT_name
	.byte	12                      @ DW_AT_decl_file
	.byte	83                      @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
                                        @ DW_AT_explicit
	.byte	19                      @ Abbrev [19] 0x7ed:0x5 DW_TAG_formal_parameter
	.long	6936                    @ DW_AT_type
                                        @ DW_AT_artificial
	.byte	20                      @ Abbrev [20] 0x7f2:0x5 DW_TAG_formal_parameter
	.long	4808                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	21                      @ Abbrev [21] 0x7f8:0x11 DW_TAG_subprogram
	.long	.Linfo_string221        @ DW_AT_linkage_name
	.long	.Linfo_string222        @ DW_AT_name
	.byte	12                      @ DW_AT_decl_file
	.byte	85                      @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	19                      @ Abbrev [19] 0x803:0x5 DW_TAG_formal_parameter
	.long	6936                    @ DW_AT_type
                                        @ DW_AT_artificial
	.byte	0                       @ End Of Children Mark
	.byte	21                      @ Abbrev [21] 0x809:0x11 DW_TAG_subprogram
	.long	.Linfo_string223        @ DW_AT_linkage_name
	.long	.Linfo_string224        @ DW_AT_name
	.byte	12                      @ DW_AT_decl_file
	.byte	86                      @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	19                      @ Abbrev [19] 0x814:0x5 DW_TAG_formal_parameter
	.long	6936                    @ DW_AT_type
                                        @ DW_AT_artificial
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x81a:0x15 DW_TAG_subprogram
	.long	.Linfo_string225        @ DW_AT_linkage_name
	.long	.Linfo_string226        @ DW_AT_name
	.byte	12                      @ DW_AT_decl_file
	.byte	88                      @ DW_AT_decl_line
	.long	4808                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	19                      @ Abbrev [19] 0x829:0x5 DW_TAG_formal_parameter
	.long	6941                    @ DW_AT_type
                                        @ DW_AT_artificial
	.byte	0                       @ End Of Children Mark
	.byte	23                      @ Abbrev [23] 0x82f:0xe DW_TAG_subprogram
	.long	.Linfo_string220        @ DW_AT_name
	.byte	12                      @ DW_AT_decl_file
	.byte	94                      @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_accessibility
                                        @ DW_ACCESS_public
	.byte	19                      @ Abbrev [19] 0x837:0x5 DW_TAG_formal_parameter
	.long	6936                    @ DW_AT_type
                                        @ DW_AT_artificial
	.byte	0                       @ End Of Children Mark
	.byte	23                      @ Abbrev [23] 0x83d:0x13 DW_TAG_subprogram
	.long	.Linfo_string220        @ DW_AT_name
	.byte	12                      @ DW_AT_decl_file
	.byte	96                      @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_accessibility
                                        @ DW_ACCESS_public
	.byte	19                      @ Abbrev [19] 0x845:0x5 DW_TAG_formal_parameter
	.long	6936                    @ DW_AT_type
                                        @ DW_AT_artificial
	.byte	20                      @ Abbrev [20] 0x84a:0x5 DW_TAG_formal_parameter
	.long	6951                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	23                      @ Abbrev [23] 0x850:0x13 DW_TAG_subprogram
	.long	.Linfo_string220        @ DW_AT_name
	.byte	12                      @ DW_AT_decl_file
	.byte	99                      @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_accessibility
                                        @ DW_ACCESS_public
	.byte	19                      @ Abbrev [19] 0x858:0x5 DW_TAG_formal_parameter
	.long	6936                    @ DW_AT_type
                                        @ DW_AT_artificial
	.byte	20                      @ Abbrev [20] 0x85d:0x5 DW_TAG_formal_parameter
	.long	2310                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	23                      @ Abbrev [23] 0x863:0x13 DW_TAG_subprogram
	.long	.Linfo_string220        @ DW_AT_name
	.byte	12                      @ DW_AT_decl_file
	.byte	103                     @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_accessibility
                                        @ DW_ACCESS_public
	.byte	19                      @ Abbrev [19] 0x86b:0x5 DW_TAG_formal_parameter
	.long	6936                    @ DW_AT_type
                                        @ DW_AT_artificial
	.byte	20                      @ Abbrev [20] 0x870:0x5 DW_TAG_formal_parameter
	.long	6961                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	24                      @ Abbrev [24] 0x876:0x1b DW_TAG_subprogram
	.long	.Linfo_string229        @ DW_AT_linkage_name
	.long	.Linfo_string230        @ DW_AT_name
	.byte	12                      @ DW_AT_decl_file
	.byte	116                     @ DW_AT_decl_line
	.long	6966                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_accessibility
                                        @ DW_ACCESS_public
	.byte	19                      @ Abbrev [19] 0x886:0x5 DW_TAG_formal_parameter
	.long	6936                    @ DW_AT_type
                                        @ DW_AT_artificial
	.byte	20                      @ Abbrev [20] 0x88b:0x5 DW_TAG_formal_parameter
	.long	6951                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	24                      @ Abbrev [24] 0x891:0x1b DW_TAG_subprogram
	.long	.Linfo_string231        @ DW_AT_linkage_name
	.long	.Linfo_string230        @ DW_AT_name
	.byte	12                      @ DW_AT_decl_file
	.byte	120                     @ DW_AT_decl_line
	.long	6966                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_accessibility
                                        @ DW_ACCESS_public
	.byte	19                      @ Abbrev [19] 0x8a1:0x5 DW_TAG_formal_parameter
	.long	6936                    @ DW_AT_type
                                        @ DW_AT_artificial
	.byte	20                      @ Abbrev [20] 0x8a6:0x5 DW_TAG_formal_parameter
	.long	6961                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	23                      @ Abbrev [23] 0x8ac:0xe DW_TAG_subprogram
	.long	.Linfo_string232        @ DW_AT_name
	.byte	12                      @ DW_AT_decl_file
	.byte	127                     @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_accessibility
                                        @ DW_ACCESS_public
	.byte	19                      @ Abbrev [19] 0x8b4:0x5 DW_TAG_formal_parameter
	.long	6936                    @ DW_AT_type
                                        @ DW_AT_artificial
	.byte	0                       @ End Of Children Mark
	.byte	25                      @ Abbrev [25] 0x8ba:0x17 DW_TAG_subprogram
	.long	.Linfo_string233        @ DW_AT_linkage_name
	.long	.Linfo_string234        @ DW_AT_name
	.byte	12                      @ DW_AT_decl_file
	.byte	130                     @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_accessibility
                                        @ DW_ACCESS_public
	.byte	19                      @ Abbrev [19] 0x8c6:0x5 DW_TAG_formal_parameter
	.long	6936                    @ DW_AT_type
                                        @ DW_AT_artificial
	.byte	20                      @ Abbrev [20] 0x8cb:0x5 DW_TAG_formal_parameter
	.long	6966                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	26                      @ Abbrev [26] 0x8d1:0x16 DW_TAG_subprogram
	.long	.Linfo_string235        @ DW_AT_linkage_name
	.long	.Linfo_string236        @ DW_AT_name
	.byte	12                      @ DW_AT_decl_file
	.byte	142                     @ DW_AT_decl_line
	.long	6971                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_accessibility
                                        @ DW_ACCESS_public
                                        @ DW_AT_explicit
	.byte	19                      @ Abbrev [19] 0x8e1:0x5 DW_TAG_formal_parameter
	.long	6941                    @ DW_AT_type
                                        @ DW_AT_artificial
	.byte	0                       @ End Of Children Mark
	.byte	24                      @ Abbrev [24] 0x8e7:0x16 DW_TAG_subprogram
	.long	.Linfo_string238        @ DW_AT_linkage_name
	.long	.Linfo_string239        @ DW_AT_name
	.byte	12                      @ DW_AT_decl_file
	.byte	151                     @ DW_AT_decl_line
	.long	6978                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	1                       @ DW_AT_accessibility
                                        @ DW_ACCESS_public
	.byte	19                      @ Abbrev [19] 0x8f7:0x5 DW_TAG_formal_parameter
	.long	6941                    @ DW_AT_type
                                        @ DW_AT_artificial
	.byte	0                       @ End Of Children Mark
	.byte	0                       @ End Of Children Mark
	.byte	14                      @ Abbrev [14] 0x8fe:0x7 DW_TAG_imported_declaration
	.byte	12                      @ DW_AT_decl_file
	.byte	73                      @ DW_AT_decl_line
	.long	2333                    @ DW_AT_import
	.byte	0                       @ End Of Children Mark
	.byte	27                      @ Abbrev [27] 0x906:0xb DW_TAG_typedef
	.long	6956                    @ DW_AT_type
	.long	.Linfo_string228        @ DW_AT_name
	.byte	4                       @ DW_AT_decl_file
	.byte	205                     @ DW_AT_decl_line
	.byte	28                      @ Abbrev [28] 0x911:0x5 DW_TAG_class_type
	.long	.Linfo_string240        @ DW_AT_name
                                        @ DW_AT_declaration
	.byte	14                      @ Abbrev [14] 0x916:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	60                      @ DW_AT_decl_line
	.long	2002                    @ DW_AT_import
	.byte	21                      @ Abbrev [21] 0x91d:0x11 DW_TAG_subprogram
	.long	.Linfo_string241        @ DW_AT_linkage_name
	.long	.Linfo_string242        @ DW_AT_name
	.byte	12                      @ DW_AT_decl_file
	.byte	69                      @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x928:0x5 DW_TAG_formal_parameter
	.long	2002                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	27                      @ Abbrev [27] 0x92e:0xb DW_TAG_typedef
	.long	4236                    @ DW_AT_type
	.long	.Linfo_string115        @ DW_AT_name
	.byte	4                       @ DW_AT_decl_file
	.byte	201                     @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x939:0xb DW_TAG_typedef
	.long	58                      @ DW_AT_type
	.long	.Linfo_string243        @ DW_AT_name
	.byte	4                       @ DW_AT_decl_file
	.byte	202                     @ DW_AT_decl_line
	.byte	14                      @ Abbrev [14] 0x944:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	53                      @ DW_AT_decl_line
	.long	6988                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x94b:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	54                      @ DW_AT_decl_line
	.long	6994                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x952:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	55                      @ DW_AT_decl_line
	.long	7016                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x959:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	64                      @ DW_AT_decl_line
	.long	7032                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x960:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	65                      @ DW_AT_decl_line
	.long	7049                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x967:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	66                      @ DW_AT_decl_line
	.long	7066                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x96e:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	67                      @ DW_AT_decl_line
	.long	7083                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x975:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	68                      @ DW_AT_decl_line
	.long	7100                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x97c:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	69                      @ DW_AT_decl_line
	.long	7117                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x983:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	70                      @ DW_AT_decl_line
	.long	7134                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x98a:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	71                      @ DW_AT_decl_line
	.long	7151                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x991:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	72                      @ DW_AT_decl_line
	.long	7168                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x998:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	73                      @ DW_AT_decl_line
	.long	7185                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x99f:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	74                      @ DW_AT_decl_line
	.long	7202                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x9a6:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	75                      @ DW_AT_decl_line
	.long	7219                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x9ad:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	76                      @ DW_AT_decl_line
	.long	7236                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x9b4:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	87                      @ DW_AT_decl_line
	.long	7253                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x9bb:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	124                     @ DW_AT_decl_line
	.long	7270                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x9c2:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	125                     @ DW_AT_decl_line
	.long	7283                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x9c9:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	127                     @ DW_AT_decl_line
	.long	7323                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x9d0:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	128                     @ DW_AT_decl_line
	.long	7331                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x9d7:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	129                     @ DW_AT_decl_line
	.long	7349                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x9de:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	132                     @ DW_AT_decl_line
	.long	7373                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x9e5:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	135                     @ DW_AT_decl_line
	.long	7391                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x9ec:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	136                     @ DW_AT_decl_line
	.long	7408                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x9f3:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	137                     @ DW_AT_decl_line
	.long	7425                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x9fa:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	138                     @ DW_AT_decl_line
	.long	7442                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa01:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	139                     @ DW_AT_decl_line
	.long	7518                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa08:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	140                     @ DW_AT_decl_line
	.long	7541                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa0f:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	141                     @ DW_AT_decl_line
	.long	7564                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa16:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	142                     @ DW_AT_decl_line
	.long	7578                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa1d:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	143                     @ DW_AT_decl_line
	.long	7592                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa24:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	144                     @ DW_AT_decl_line
	.long	7610                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa2b:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	145                     @ DW_AT_decl_line
	.long	7628                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa32:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	146                     @ DW_AT_decl_line
	.long	7651                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa39:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	148                     @ DW_AT_decl_line
	.long	7669                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa40:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	149                     @ DW_AT_decl_line
	.long	7692                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa47:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	150                     @ DW_AT_decl_line
	.long	7720                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa4e:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	152                     @ DW_AT_decl_line
	.long	7748                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa55:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	155                     @ DW_AT_decl_line
	.long	7777                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa5c:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	158                     @ DW_AT_decl_line
	.long	7791                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa63:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	159                     @ DW_AT_decl_line
	.long	7803                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa6a:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	160                     @ DW_AT_decl_line
	.long	7826                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa71:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	161                     @ DW_AT_decl_line
	.long	7840                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa78:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	162                     @ DW_AT_decl_line
	.long	7872                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa7f:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	163                     @ DW_AT_decl_line
	.long	7899                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa86:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	164                     @ DW_AT_decl_line
	.long	7926                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa8d:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	166                     @ DW_AT_decl_line
	.long	7944                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xa94:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	167                     @ DW_AT_decl_line
	.long	7972                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xa9b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	260                     @ DW_AT_decl_line
	.long	7995                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xaa3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	262                     @ DW_AT_decl_line
	.long	8035                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xaab:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	264                     @ DW_AT_decl_line
	.long	8049                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xab3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	265                     @ DW_AT_decl_line
	.long	6459                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xabb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	266                     @ DW_AT_decl_line
	.long	8067                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xac3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	268                     @ DW_AT_decl_line
	.long	8090                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xacb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	269                     @ DW_AT_decl_line
	.long	8161                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xad3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	270                     @ DW_AT_decl_line
	.long	8107                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xadb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	271                     @ DW_AT_decl_line
	.long	8134                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xae3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	272                     @ DW_AT_decl_line
	.long	8183                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xaeb:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	98                      @ DW_AT_decl_line
	.long	8205                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xaf2:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	99                      @ DW_AT_decl_line
	.long	8216                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xaf9:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	101                     @ DW_AT_decl_line
	.long	8240                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb00:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	102                     @ DW_AT_decl_line
	.long	8259                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb07:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	103                     @ DW_AT_decl_line
	.long	8276                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb0e:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	104                     @ DW_AT_decl_line
	.long	8294                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb15:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	105                     @ DW_AT_decl_line
	.long	8312                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb1c:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	106                     @ DW_AT_decl_line
	.long	8329                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb23:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	107                     @ DW_AT_decl_line
	.long	8347                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb2a:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	108                     @ DW_AT_decl_line
	.long	8385                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb31:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	109                     @ DW_AT_decl_line
	.long	8413                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb38:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	110                     @ DW_AT_decl_line
	.long	8436                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb3f:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	111                     @ DW_AT_decl_line
	.long	8460                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb46:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	112                     @ DW_AT_decl_line
	.long	8483                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb4d:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	113                     @ DW_AT_decl_line
	.long	8506                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb54:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	114                     @ DW_AT_decl_line
	.long	8544                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb5b:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	115                     @ DW_AT_decl_line
	.long	8572                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb62:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	116                     @ DW_AT_decl_line
	.long	8596                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb69:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	117                     @ DW_AT_decl_line
	.long	8624                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb70:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	118                     @ DW_AT_decl_line
	.long	8657                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb77:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	119                     @ DW_AT_decl_line
	.long	8675                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb7e:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	120                     @ DW_AT_decl_line
	.long	8713                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb85:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	121                     @ DW_AT_decl_line
	.long	8731                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb8c:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	124                     @ DW_AT_decl_line
	.long	8742                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb93:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	126                     @ DW_AT_decl_line
	.long	8760                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xb9a:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	127                     @ DW_AT_decl_line
	.long	8774                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xba1:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	128                     @ DW_AT_decl_line
	.long	8793                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xba8:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	129                     @ DW_AT_decl_line
	.long	8816                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xbaf:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	130                     @ DW_AT_decl_line
	.long	8833                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xbb6:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	131                     @ DW_AT_decl_line
	.long	8851                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xbbd:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	132                     @ DW_AT_decl_line
	.long	8868                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xbc4:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	133                     @ DW_AT_decl_line
	.long	8890                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xbcb:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	134                     @ DW_AT_decl_line
	.long	8904                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xbd2:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	135                     @ DW_AT_decl_line
	.long	8923                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xbd9:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	136                     @ DW_AT_decl_line
	.long	8942                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xbe0:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	137                     @ DW_AT_decl_line
	.long	8975                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xbe7:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	138                     @ DW_AT_decl_line
	.long	8999                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xbee:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	139                     @ DW_AT_decl_line
	.long	9023                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xbf5:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	141                     @ DW_AT_decl_line
	.long	9034                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xbfc:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	143                     @ DW_AT_decl_line
	.long	9051                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xc03:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	144                     @ DW_AT_decl_line
	.long	9074                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xc0a:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	145                     @ DW_AT_decl_line
	.long	9102                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xc11:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	146                     @ DW_AT_decl_line
	.long	9124                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xc18:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	185                     @ DW_AT_decl_line
	.long	9152                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xc1f:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	186                     @ DW_AT_decl_line
	.long	9181                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xc26:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	187                     @ DW_AT_decl_line
	.long	9209                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xc2d:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	188                     @ DW_AT_decl_line
	.long	9232                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xc34:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	189                     @ DW_AT_decl_line
	.long	9265                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xc3b:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	106                     @ DW_AT_decl_line
	.long	9293                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xc42:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	125                     @ DW_AT_decl_line
	.long	9314                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xc49:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	144                     @ DW_AT_decl_line
	.long	9335                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xc50:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	163                     @ DW_AT_decl_line
	.long	9352                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xc57:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	184                     @ DW_AT_decl_line
	.long	9378                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xc5e:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	203                     @ DW_AT_decl_line
	.long	9395                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xc65:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	222                     @ DW_AT_decl_line
	.long	9412                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0xc6c:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	241                     @ DW_AT_decl_line
	.long	9433                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xc73:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	260                     @ DW_AT_decl_line
	.long	9454                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xc7b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	279                     @ DW_AT_decl_line
	.long	9471                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xc83:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	298                     @ DW_AT_decl_line
	.long	9488                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xc8b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	319                     @ DW_AT_decl_line
	.long	9514                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xc93:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	338                     @ DW_AT_decl_line
	.long	9541                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xc9b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	357                     @ DW_AT_decl_line
	.long	9563                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xca3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	376                     @ DW_AT_decl_line
	.long	9585                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xcab:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	395                     @ DW_AT_decl_line
	.long	9607                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xcb3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	407                     @ DW_AT_decl_line
	.long	9634                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xcbb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	444                     @ DW_AT_decl_line
	.long	9661                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xcc3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	463                     @ DW_AT_decl_line
	.long	9678                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xccb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	482                     @ DW_AT_decl_line
	.long	9700                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xcd3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	501                     @ DW_AT_decl_line
	.long	9722                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xcdb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	520                     @ DW_AT_decl_line
	.long	9739                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xce3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1101                    @ DW_AT_decl_line
	.long	9756                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xceb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1102                    @ DW_AT_decl_line
	.long	9767                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xcf3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1105                    @ DW_AT_decl_line
	.long	9778                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xcfb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1106                    @ DW_AT_decl_line
	.long	9799                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd03:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1107                    @ DW_AT_decl_line
	.long	9820                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd0b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1109                    @ DW_AT_decl_line
	.long	9841                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd13:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1110                    @ DW_AT_decl_line
	.long	9858                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd1b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1111                    @ DW_AT_decl_line
	.long	9875                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd23:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1113                    @ DW_AT_decl_line
	.long	9892                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd2b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1114                    @ DW_AT_decl_line
	.long	9913                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd33:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1115                    @ DW_AT_decl_line
	.long	9934                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd3b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1117                    @ DW_AT_decl_line
	.long	9955                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd43:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1118                    @ DW_AT_decl_line
	.long	9972                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd4b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1119                    @ DW_AT_decl_line
	.long	9989                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd53:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1121                    @ DW_AT_decl_line
	.long	10006                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd5b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1122                    @ DW_AT_decl_line
	.long	10028                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd63:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1123                    @ DW_AT_decl_line
	.long	10050                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd6b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1125                    @ DW_AT_decl_line
	.long	10072                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd73:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1126                    @ DW_AT_decl_line
	.long	10090                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd7b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1127                    @ DW_AT_decl_line
	.long	10108                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd83:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1129                    @ DW_AT_decl_line
	.long	10126                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd8b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1130                    @ DW_AT_decl_line
	.long	10144                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd93:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1131                    @ DW_AT_decl_line
	.long	10162                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xd9b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1133                    @ DW_AT_decl_line
	.long	10180                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xda3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1134                    @ DW_AT_decl_line
	.long	10201                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xdab:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1135                    @ DW_AT_decl_line
	.long	10222                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xdb3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1137                    @ DW_AT_decl_line
	.long	10243                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xdbb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1138                    @ DW_AT_decl_line
	.long	10260                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xdc3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1139                    @ DW_AT_decl_line
	.long	10277                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xdcb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1141                    @ DW_AT_decl_line
	.long	10294                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xdd3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1142                    @ DW_AT_decl_line
	.long	10317                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xddb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1143                    @ DW_AT_decl_line
	.long	10340                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xde3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1145                    @ DW_AT_decl_line
	.long	10363                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xdeb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1146                    @ DW_AT_decl_line
	.long	10391                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xdf3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1147                    @ DW_AT_decl_line
	.long	10419                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xdfb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1149                    @ DW_AT_decl_line
	.long	10447                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe03:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1150                    @ DW_AT_decl_line
	.long	10470                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe0b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1151                    @ DW_AT_decl_line
	.long	10493                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe13:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1153                    @ DW_AT_decl_line
	.long	10516                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe1b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1154                    @ DW_AT_decl_line
	.long	10539                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe23:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1155                    @ DW_AT_decl_line
	.long	10562                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe2b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1157                    @ DW_AT_decl_line
	.long	10585                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe33:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1158                    @ DW_AT_decl_line
	.long	10611                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe3b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1159                    @ DW_AT_decl_line
	.long	10637                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe43:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1161                    @ DW_AT_decl_line
	.long	10663                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe4b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1162                    @ DW_AT_decl_line
	.long	10681                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe53:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1163                    @ DW_AT_decl_line
	.long	10699                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe5b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1165                    @ DW_AT_decl_line
	.long	10717                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe63:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1166                    @ DW_AT_decl_line
	.long	10735                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe6b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1167                    @ DW_AT_decl_line
	.long	10753                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe73:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1169                    @ DW_AT_decl_line
	.long	10771                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe7b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1170                    @ DW_AT_decl_line
	.long	10789                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe83:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1171                    @ DW_AT_decl_line
	.long	10807                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe8b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1173                    @ DW_AT_decl_line
	.long	10825                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe93:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1174                    @ DW_AT_decl_line
	.long	10843                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xe9b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1175                    @ DW_AT_decl_line
	.long	10861                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xea3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1177                    @ DW_AT_decl_line
	.long	10879                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xeab:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1178                    @ DW_AT_decl_line
	.long	10896                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xeb3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1179                    @ DW_AT_decl_line
	.long	10913                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xebb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1181                    @ DW_AT_decl_line
	.long	10930                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xec3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1182                    @ DW_AT_decl_line
	.long	10952                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xecb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1183                    @ DW_AT_decl_line
	.long	10974                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xed3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1185                    @ DW_AT_decl_line
	.long	10996                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xedb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1186                    @ DW_AT_decl_line
	.long	11013                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xee3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1187                    @ DW_AT_decl_line
	.long	11030                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xeeb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1189                    @ DW_AT_decl_line
	.long	11047                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xef3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1190                    @ DW_AT_decl_line
	.long	11065                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xefb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1191                    @ DW_AT_decl_line
	.long	11083                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf03:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1193                    @ DW_AT_decl_line
	.long	11101                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf0b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1194                    @ DW_AT_decl_line
	.long	11119                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf13:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1195                    @ DW_AT_decl_line
	.long	11137                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf1b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1197                    @ DW_AT_decl_line
	.long	11155                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf23:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1198                    @ DW_AT_decl_line
	.long	11172                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf2b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1199                    @ DW_AT_decl_line
	.long	11189                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf33:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1201                    @ DW_AT_decl_line
	.long	11206                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf3b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1202                    @ DW_AT_decl_line
	.long	11224                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf43:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1203                    @ DW_AT_decl_line
	.long	11242                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf4b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1205                    @ DW_AT_decl_line
	.long	11260                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf53:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1206                    @ DW_AT_decl_line
	.long	11283                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf5b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1207                    @ DW_AT_decl_line
	.long	11306                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf63:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1209                    @ DW_AT_decl_line
	.long	11329                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf6b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1210                    @ DW_AT_decl_line
	.long	11352                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf73:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1211                    @ DW_AT_decl_line
	.long	11375                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf7b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1213                    @ DW_AT_decl_line
	.long	11398                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf83:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1214                    @ DW_AT_decl_line
	.long	11425                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf8b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1215                    @ DW_AT_decl_line
	.long	11452                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf93:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1217                    @ DW_AT_decl_line
	.long	11479                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xf9b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1218                    @ DW_AT_decl_line
	.long	11507                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xfa3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1219                    @ DW_AT_decl_line
	.long	11535                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xfab:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1221                    @ DW_AT_decl_line
	.long	11563                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xfb3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1222                    @ DW_AT_decl_line
	.long	11581                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xfbb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1223                    @ DW_AT_decl_line
	.long	11599                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xfc3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1225                    @ DW_AT_decl_line
	.long	11617                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xfcb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1226                    @ DW_AT_decl_line
	.long	11635                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xfd3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1227                    @ DW_AT_decl_line
	.long	11653                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xfdb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1229                    @ DW_AT_decl_line
	.long	11671                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xfe3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1230                    @ DW_AT_decl_line
	.long	11694                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xfeb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1231                    @ DW_AT_decl_line
	.long	11717                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xff3:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1233                    @ DW_AT_decl_line
	.long	11740                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0xffb:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1234                    @ DW_AT_decl_line
	.long	11763                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x1003:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1235                    @ DW_AT_decl_line
	.long	11786                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x100b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1237                    @ DW_AT_decl_line
	.long	11809                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x1013:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1238                    @ DW_AT_decl_line
	.long	11827                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x101b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1239                    @ DW_AT_decl_line
	.long	11845                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x1023:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1241                    @ DW_AT_decl_line
	.long	11863                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x102b:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1242                    @ DW_AT_decl_line
	.long	11881                   @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x1033:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	1243                    @ DW_AT_decl_line
	.long	11899                   @ DW_AT_import
	.byte	0                       @ End Of Children Mark
	.byte	27                      @ Abbrev [27] 0x103c:0xb DW_TAG_typedef
	.long	4167                    @ DW_AT_type
	.long	.Linfo_string75         @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.byte	106                     @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1047:0xb DW_TAG_typedef
	.long	4178                    @ DW_AT_type
	.long	.Linfo_string74         @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.byte	94                      @ DW_AT_decl_line
	.byte	29                      @ Abbrev [29] 0x1052:0x3a DW_TAG_structure_type
	.byte	8                       @ DW_AT_byte_size
	.byte	5                       @ DW_AT_decl_file
	.byte	82                      @ DW_AT_decl_line
	.byte	17                      @ Abbrev [17] 0x1056:0xc DW_TAG_member
	.long	.Linfo_string68         @ DW_AT_name
	.long	58                      @ DW_AT_type
	.byte	5                       @ DW_AT_decl_file
	.byte	84                      @ DW_AT_decl_line
	.byte	0                       @ DW_AT_data_member_location
	.byte	17                      @ Abbrev [17] 0x1062:0xc DW_TAG_member
	.long	.Linfo_string69         @ DW_AT_name
	.long	4206                    @ DW_AT_type
	.byte	5                       @ DW_AT_decl_file
	.byte	93                      @ DW_AT_decl_line
	.byte	4                       @ DW_AT_data_member_location
	.byte	30                      @ Abbrev [30] 0x106e:0x1d DW_TAG_union_type
	.byte	4                       @ DW_AT_byte_size
	.byte	5                       @ DW_AT_decl_file
	.byte	85                      @ DW_AT_decl_line
	.byte	17                      @ Abbrev [17] 0x1072:0xc DW_TAG_member
	.long	.Linfo_string70         @ DW_AT_name
	.long	4236                    @ DW_AT_type
	.byte	5                       @ DW_AT_decl_file
	.byte	88                      @ DW_AT_decl_line
	.byte	0                       @ DW_AT_data_member_location
	.byte	17                      @ Abbrev [17] 0x107e:0xc DW_TAG_member
	.long	.Linfo_string72         @ DW_AT_name
	.long	4243                    @ DW_AT_type
	.byte	5                       @ DW_AT_decl_file
	.byte	92                      @ DW_AT_decl_line
	.byte	0                       @ DW_AT_data_member_location
	.byte	0                       @ End Of Children Mark
	.byte	0                       @ End Of Children Mark
	.byte	3                       @ Abbrev [3] 0x108c:0x7 DW_TAG_base_type
	.long	.Linfo_string71         @ DW_AT_name
	.byte	7                       @ DW_AT_encoding
	.byte	4                       @ DW_AT_byte_size
	.byte	4                       @ Abbrev [4] 0x1093:0xc DW_TAG_array_type
	.long	4255                    @ DW_AT_type
	.byte	5                       @ Abbrev [5] 0x1098:0x6 DW_TAG_subrange_type
	.long	97                      @ DW_AT_type
	.byte	4                       @ DW_AT_count
	.byte	0                       @ End Of Children Mark
	.byte	3                       @ Abbrev [3] 0x109f:0x7 DW_TAG_base_type
	.long	.Linfo_string73         @ DW_AT_name
	.byte	8                       @ DW_AT_encoding
	.byte	1                       @ DW_AT_byte_size
	.byte	27                      @ Abbrev [27] 0x10a6:0xb DW_TAG_typedef
	.long	4236                    @ DW_AT_type
	.long	.Linfo_string76         @ DW_AT_name
	.byte	6                       @ DW_AT_decl_file
	.byte	132                     @ DW_AT_decl_line
	.byte	31                      @ Abbrev [31] 0x10b1:0x12 DW_TAG_subprogram
	.long	.Linfo_string77         @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	391                     @ DW_AT_decl_line
	.long	4262                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x10bd:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x10c3:0x12 DW_TAG_subprogram
	.long	.Linfo_string78         @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	748                     @ DW_AT_decl_line
	.long	4262                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x10cf:0x5 DW_TAG_formal_parameter
	.long	4309                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	32                      @ Abbrev [32] 0x10d5:0x5 DW_TAG_pointer_type
	.long	4314                    @ DW_AT_type
	.byte	27                      @ Abbrev [27] 0x10da:0xb DW_TAG_typedef
	.long	4325                    @ DW_AT_type
	.long	.Linfo_string119        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.byte	64                      @ DW_AT_decl_line
	.byte	33                      @ Abbrev [33] 0x10e5:0x179 DW_TAG_structure_type
	.long	.Linfo_string118        @ DW_AT_name
	.byte	152                     @ DW_AT_byte_size
	.byte	7                       @ DW_AT_decl_file
	.byte	241                     @ DW_AT_decl_line
	.byte	17                      @ Abbrev [17] 0x10ed:0xc DW_TAG_member
	.long	.Linfo_string79         @ DW_AT_name
	.long	58                      @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.byte	242                     @ DW_AT_decl_line
	.byte	0                       @ DW_AT_data_member_location
	.byte	17                      @ Abbrev [17] 0x10f9:0xc DW_TAG_member
	.long	.Linfo_string80         @ DW_AT_name
	.long	4702                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.byte	247                     @ DW_AT_decl_line
	.byte	4                       @ DW_AT_data_member_location
	.byte	17                      @ Abbrev [17] 0x1105:0xc DW_TAG_member
	.long	.Linfo_string81         @ DW_AT_name
	.long	4702                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.byte	248                     @ DW_AT_decl_line
	.byte	8                       @ DW_AT_data_member_location
	.byte	17                      @ Abbrev [17] 0x1111:0xc DW_TAG_member
	.long	.Linfo_string82         @ DW_AT_name
	.long	4702                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.byte	249                     @ DW_AT_decl_line
	.byte	12                      @ DW_AT_data_member_location
	.byte	17                      @ Abbrev [17] 0x111d:0xc DW_TAG_member
	.long	.Linfo_string83         @ DW_AT_name
	.long	4702                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.byte	250                     @ DW_AT_decl_line
	.byte	16                      @ DW_AT_data_member_location
	.byte	17                      @ Abbrev [17] 0x1129:0xc DW_TAG_member
	.long	.Linfo_string84         @ DW_AT_name
	.long	4702                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.byte	251                     @ DW_AT_decl_line
	.byte	20                      @ DW_AT_data_member_location
	.byte	17                      @ Abbrev [17] 0x1135:0xc DW_TAG_member
	.long	.Linfo_string85         @ DW_AT_name
	.long	4702                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.byte	252                     @ DW_AT_decl_line
	.byte	24                      @ DW_AT_data_member_location
	.byte	17                      @ Abbrev [17] 0x1141:0xc DW_TAG_member
	.long	.Linfo_string86         @ DW_AT_name
	.long	4702                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.byte	253                     @ DW_AT_decl_line
	.byte	28                      @ DW_AT_data_member_location
	.byte	17                      @ Abbrev [17] 0x114d:0xc DW_TAG_member
	.long	.Linfo_string87         @ DW_AT_name
	.long	4702                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.byte	254                     @ DW_AT_decl_line
	.byte	32                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x1159:0xd DW_TAG_member
	.long	.Linfo_string88         @ DW_AT_name
	.long	4702                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	256                     @ DW_AT_decl_line
	.byte	36                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x1166:0xd DW_TAG_member
	.long	.Linfo_string89         @ DW_AT_name
	.long	4702                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	257                     @ DW_AT_decl_line
	.byte	40                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x1173:0xd DW_TAG_member
	.long	.Linfo_string90         @ DW_AT_name
	.long	4702                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	258                     @ DW_AT_decl_line
	.byte	44                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x1180:0xd DW_TAG_member
	.long	.Linfo_string91         @ DW_AT_name
	.long	4707                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	260                     @ DW_AT_decl_line
	.byte	48                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x118d:0xd DW_TAG_member
	.long	.Linfo_string93         @ DW_AT_name
	.long	4718                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	262                     @ DW_AT_decl_line
	.byte	52                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x119a:0xd DW_TAG_member
	.long	.Linfo_string94         @ DW_AT_name
	.long	58                      @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	264                     @ DW_AT_decl_line
	.byte	56                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x11a7:0xd DW_TAG_member
	.long	.Linfo_string95         @ DW_AT_name
	.long	58                      @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	268                     @ DW_AT_decl_line
	.byte	60                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x11b4:0xd DW_TAG_member
	.long	.Linfo_string96         @ DW_AT_name
	.long	4723                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	270                     @ DW_AT_decl_line
	.byte	64                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x11c1:0xd DW_TAG_member
	.long	.Linfo_string99         @ DW_AT_name
	.long	4741                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	274                     @ DW_AT_decl_line
	.byte	68                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x11ce:0xd DW_TAG_member
	.long	.Linfo_string101        @ DW_AT_name
	.long	4748                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	275                     @ DW_AT_decl_line
	.byte	70                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x11db:0xd DW_TAG_member
	.long	.Linfo_string103        @ DW_AT_name
	.long	4755                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	276                     @ DW_AT_decl_line
	.byte	71                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x11e8:0xd DW_TAG_member
	.long	.Linfo_string104        @ DW_AT_name
	.long	4767                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	280                     @ DW_AT_decl_line
	.byte	72                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x11f5:0xd DW_TAG_member
	.long	.Linfo_string106        @ DW_AT_name
	.long	4779                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	289                     @ DW_AT_decl_line
	.byte	80                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x1202:0xd DW_TAG_member
	.long	.Linfo_string110        @ DW_AT_name
	.long	4808                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	297                     @ DW_AT_decl_line
	.byte	88                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x120f:0xd DW_TAG_member
	.long	.Linfo_string111        @ DW_AT_name
	.long	4808                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	298                     @ DW_AT_decl_line
	.byte	92                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x121c:0xd DW_TAG_member
	.long	.Linfo_string112        @ DW_AT_name
	.long	4808                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	299                     @ DW_AT_decl_line
	.byte	96                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x1229:0xd DW_TAG_member
	.long	.Linfo_string113        @ DW_AT_name
	.long	4808                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	300                     @ DW_AT_decl_line
	.byte	100                     @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x1236:0xd DW_TAG_member
	.long	.Linfo_string114        @ DW_AT_name
	.long	4809                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	302                     @ DW_AT_decl_line
	.byte	104                     @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x1243:0xd DW_TAG_member
	.long	.Linfo_string116        @ DW_AT_name
	.long	58                      @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	303                     @ DW_AT_decl_line
	.byte	108                     @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x1250:0xd DW_TAG_member
	.long	.Linfo_string117        @ DW_AT_name
	.long	4820                    @ DW_AT_type
	.byte	7                       @ DW_AT_decl_file
	.short	305                     @ DW_AT_decl_line
	.byte	112                     @ DW_AT_data_member_location
	.byte	0                       @ End Of Children Mark
	.byte	32                      @ Abbrev [32] 0x125e:0x5 DW_TAG_pointer_type
	.long	4255                    @ DW_AT_type
	.byte	32                      @ Abbrev [32] 0x1263:0x5 DW_TAG_pointer_type
	.long	4712                    @ DW_AT_type
	.byte	35                      @ Abbrev [35] 0x1268:0x6 DW_TAG_structure_type
	.long	.Linfo_string92         @ DW_AT_name
	.byte	12                      @ DW_AT_byte_size
                                        @ DW_AT_declaration
	.byte	32                      @ Abbrev [32] 0x126e:0x5 DW_TAG_pointer_type
	.long	4325                    @ DW_AT_type
	.byte	27                      @ Abbrev [27] 0x1273:0xb DW_TAG_typedef
	.long	4734                    @ DW_AT_type
	.long	.Linfo_string98         @ DW_AT_name
	.byte	8                       @ DW_AT_decl_file
	.byte	131                     @ DW_AT_decl_line
	.byte	3                       @ Abbrev [3] 0x127e:0x7 DW_TAG_base_type
	.long	.Linfo_string97         @ DW_AT_name
	.byte	5                       @ DW_AT_encoding
	.byte	4                       @ DW_AT_byte_size
	.byte	3                       @ Abbrev [3] 0x1285:0x7 DW_TAG_base_type
	.long	.Linfo_string100        @ DW_AT_name
	.byte	7                       @ DW_AT_encoding
	.byte	2                       @ DW_AT_byte_size
	.byte	3                       @ Abbrev [3] 0x128c:0x7 DW_TAG_base_type
	.long	.Linfo_string102        @ DW_AT_name
	.byte	6                       @ DW_AT_encoding
	.byte	1                       @ DW_AT_byte_size
	.byte	4                       @ Abbrev [4] 0x1293:0xc DW_TAG_array_type
	.long	4255                    @ DW_AT_type
	.byte	5                       @ Abbrev [5] 0x1298:0x6 DW_TAG_subrange_type
	.long	97                      @ DW_AT_type
	.byte	1                       @ DW_AT_count
	.byte	0                       @ End Of Children Mark
	.byte	32                      @ Abbrev [32] 0x129f:0x5 DW_TAG_pointer_type
	.long	4772                    @ DW_AT_type
	.byte	36                      @ Abbrev [36] 0x12a4:0x7 DW_TAG_typedef
	.long	.Linfo_string105        @ DW_AT_name
	.byte	7                       @ DW_AT_decl_file
	.byte	150                     @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x12ab:0xb DW_TAG_typedef
	.long	4790                    @ DW_AT_type
	.long	.Linfo_string109        @ DW_AT_name
	.byte	8                       @ DW_AT_decl_file
	.byte	132                     @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x12b6:0xb DW_TAG_typedef
	.long	4801                    @ DW_AT_type
	.long	.Linfo_string108        @ DW_AT_name
	.byte	8                       @ DW_AT_decl_file
	.byte	55                      @ DW_AT_decl_line
	.byte	3                       @ Abbrev [3] 0x12c1:0x7 DW_TAG_base_type
	.long	.Linfo_string107        @ DW_AT_name
	.byte	5                       @ DW_AT_encoding
	.byte	8                       @ DW_AT_byte_size
	.byte	37                      @ Abbrev [37] 0x12c8:0x1 DW_TAG_pointer_type
	.byte	27                      @ Abbrev [27] 0x12c9:0xb DW_TAG_typedef
	.long	4236                    @ DW_AT_type
	.long	.Linfo_string115        @ DW_AT_name
	.byte	6                       @ DW_AT_decl_file
	.byte	62                      @ DW_AT_decl_line
	.byte	4                       @ Abbrev [4] 0x12d4:0xc DW_TAG_array_type
	.long	4255                    @ DW_AT_type
	.byte	5                       @ Abbrev [5] 0x12d9:0x6 DW_TAG_subrange_type
	.long	97                      @ DW_AT_type
	.byte	40                      @ DW_AT_count
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x12e0:0x1c DW_TAG_subprogram
	.long	.Linfo_string120        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	777                     @ DW_AT_decl_line
	.long	4860                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x12ec:0x5 DW_TAG_formal_parameter
	.long	4872                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x12f1:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x12f6:0x5 DW_TAG_formal_parameter
	.long	4877                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	32                      @ Abbrev [32] 0x12fc:0x5 DW_TAG_pointer_type
	.long	4865                    @ DW_AT_type
	.byte	3                       @ Abbrev [3] 0x1301:0x7 DW_TAG_base_type
	.long	.Linfo_string121        @ DW_AT_name
	.byte	7                       @ DW_AT_encoding
	.byte	4                       @ DW_AT_byte_size
	.byte	38                      @ Abbrev [38] 0x1308:0x5 DW_TAG_restrict_type
	.long	4860                    @ DW_AT_type
	.byte	38                      @ Abbrev [38] 0x130d:0x5 DW_TAG_restrict_type
	.long	4309                    @ DW_AT_type
	.byte	31                      @ Abbrev [31] 0x1312:0x17 DW_TAG_subprogram
	.long	.Linfo_string122        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	762                     @ DW_AT_decl_line
	.long	4262                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x131e:0x5 DW_TAG_formal_parameter
	.long	4865                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1323:0x5 DW_TAG_formal_parameter
	.long	4309                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1329:0x17 DW_TAG_subprogram
	.long	.Linfo_string123        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	784                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1335:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x133a:0x5 DW_TAG_formal_parameter
	.long	4877                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	38                      @ Abbrev [38] 0x1340:0x5 DW_TAG_restrict_type
	.long	4933                    @ DW_AT_type
	.byte	32                      @ Abbrev [32] 0x1345:0x5 DW_TAG_pointer_type
	.long	4938                    @ DW_AT_type
	.byte	39                      @ Abbrev [39] 0x134a:0x5 DW_TAG_const_type
	.long	4865                    @ DW_AT_type
	.byte	31                      @ Abbrev [31] 0x134f:0x17 DW_TAG_subprogram
	.long	.Linfo_string124        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	590                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x135b:0x5 DW_TAG_formal_parameter
	.long	4309                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1360:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1366:0x18 DW_TAG_subprogram
	.long	.Linfo_string125        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	597                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1372:0x5 DW_TAG_formal_parameter
	.long	4877                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1377:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	40                      @ Abbrev [40] 0x137c:0x1 DW_TAG_unspecified_parameters
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x137e:0x18 DW_TAG_subprogram
	.long	.Linfo_string126        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	638                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x138a:0x5 DW_TAG_formal_parameter
	.long	4877                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x138f:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	40                      @ Abbrev [40] 0x1394:0x1 DW_TAG_unspecified_parameters
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1396:0x12 DW_TAG_subprogram
	.long	.Linfo_string127        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	749                     @ DW_AT_decl_line
	.long	4262                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x13a2:0x5 DW_TAG_formal_parameter
	.long	4309                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	41                      @ Abbrev [41] 0x13a8:0xc DW_TAG_subprogram
	.long	.Linfo_string128        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	755                     @ DW_AT_decl_line
	.long	4262                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	31                      @ Abbrev [31] 0x13b4:0x1c DW_TAG_subprogram
	.long	.Linfo_string129        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	402                     @ DW_AT_decl_line
	.long	4809                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x13c0:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x13c5:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x13ca:0x5 DW_TAG_formal_parameter
	.long	5087                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	38                      @ Abbrev [38] 0x13d0:0x5 DW_TAG_restrict_type
	.long	5077                    @ DW_AT_type
	.byte	32                      @ Abbrev [32] 0x13d5:0x5 DW_TAG_pointer_type
	.long	5082                    @ DW_AT_type
	.byte	39                      @ Abbrev [39] 0x13da:0x5 DW_TAG_const_type
	.long	4255                    @ DW_AT_type
	.byte	38                      @ Abbrev [38] 0x13df:0x5 DW_TAG_restrict_type
	.long	5092                    @ DW_AT_type
	.byte	32                      @ Abbrev [32] 0x13e4:0x5 DW_TAG_pointer_type
	.long	4156                    @ DW_AT_type
	.byte	31                      @ Abbrev [31] 0x13e9:0x21 DW_TAG_subprogram
	.long	.Linfo_string130        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	368                     @ DW_AT_decl_line
	.long	4809                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x13f5:0x5 DW_TAG_formal_parameter
	.long	4872                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x13fa:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x13ff:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1404:0x5 DW_TAG_formal_parameter
	.long	5087                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x140a:0x12 DW_TAG_subprogram
	.long	.Linfo_string131        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	364                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1416:0x5 DW_TAG_formal_parameter
	.long	5148                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	32                      @ Abbrev [32] 0x141c:0x5 DW_TAG_pointer_type
	.long	5153                    @ DW_AT_type
	.byte	39                      @ Abbrev [39] 0x1421:0x5 DW_TAG_const_type
	.long	4156                    @ DW_AT_type
	.byte	31                      @ Abbrev [31] 0x1426:0x21 DW_TAG_subprogram
	.long	.Linfo_string132        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	411                     @ DW_AT_decl_line
	.long	4809                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1432:0x5 DW_TAG_formal_parameter
	.long	4872                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1437:0x5 DW_TAG_formal_parameter
	.long	5191                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x143c:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1441:0x5 DW_TAG_formal_parameter
	.long	5087                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	38                      @ Abbrev [38] 0x1447:0x5 DW_TAG_restrict_type
	.long	5196                    @ DW_AT_type
	.byte	32                      @ Abbrev [32] 0x144c:0x5 DW_TAG_pointer_type
	.long	5077                    @ DW_AT_type
	.byte	31                      @ Abbrev [31] 0x1451:0x17 DW_TAG_subprogram
	.long	.Linfo_string133        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	763                     @ DW_AT_decl_line
	.long	4262                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x145d:0x5 DW_TAG_formal_parameter
	.long	4865                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1462:0x5 DW_TAG_formal_parameter
	.long	4309                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1468:0x12 DW_TAG_subprogram
	.long	.Linfo_string134        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	769                     @ DW_AT_decl_line
	.long	4262                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1474:0x5 DW_TAG_formal_parameter
	.long	4865                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x147a:0x1d DW_TAG_subprogram
	.long	.Linfo_string135        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	607                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1486:0x5 DW_TAG_formal_parameter
	.long	4872                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x148b:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1490:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	40                      @ Abbrev [40] 0x1495:0x1 DW_TAG_unspecified_parameters
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1497:0x18 DW_TAG_subprogram
	.long	.Linfo_string136        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	648                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x14a3:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x14a8:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	40                      @ Abbrev [40] 0x14ad:0x1 DW_TAG_unspecified_parameters
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x14af:0x17 DW_TAG_subprogram
	.long	.Linfo_string137        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	792                     @ DW_AT_decl_line
	.long	4262                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x14bb:0x5 DW_TAG_formal_parameter
	.long	4262                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x14c0:0x5 DW_TAG_formal_parameter
	.long	4309                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x14c6:0x1c DW_TAG_subprogram
	.long	.Linfo_string138        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	615                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x14d2:0x5 DW_TAG_formal_parameter
	.long	4877                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x14d7:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x14dc:0x5 DW_TAG_formal_parameter
	.long	5346                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	27                      @ Abbrev [27] 0x14e2:0xb DW_TAG_typedef
	.long	5357                    @ DW_AT_type
	.long	.Linfo_string142        @ DW_AT_name
	.byte	10                      @ DW_AT_decl_file
	.byte	50                      @ DW_AT_decl_line
	.byte	42                      @ Abbrev [42] 0x14ed:0x9 DW_TAG_typedef
	.long	5371                    @ DW_AT_type
	.long	.Linfo_string141        @ DW_AT_name
	.byte	43                      @ Abbrev [43] 0x14f6:0x17 DW_TAG_namespace
	.long	.Linfo_string66         @ DW_AT_name
	.byte	44                      @ Abbrev [44] 0x14fb:0x11 DW_TAG_structure_type
	.long	.Linfo_string140        @ DW_AT_name
	.byte	4                       @ DW_AT_byte_size
	.byte	45                      @ Abbrev [45] 0x1501:0xa DW_TAG_member
	.long	.Linfo_string139        @ DW_AT_name
	.long	4808                    @ DW_AT_type
	.byte	0                       @ DW_AT_data_member_location
	.byte	0                       @ End Of Children Mark
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x150d:0x1c DW_TAG_subprogram
	.long	.Linfo_string143        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	692                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1519:0x5 DW_TAG_formal_parameter
	.long	4877                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x151e:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1523:0x5 DW_TAG_formal_parameter
	.long	5346                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1529:0x21 DW_TAG_subprogram
	.long	.Linfo_string144        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	628                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1535:0x5 DW_TAG_formal_parameter
	.long	4872                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x153a:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x153f:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1544:0x5 DW_TAG_formal_parameter
	.long	5346                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x154a:0x1c DW_TAG_subprogram
	.long	.Linfo_string145        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	704                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1556:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x155b:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1560:0x5 DW_TAG_formal_parameter
	.long	5346                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1566:0x17 DW_TAG_subprogram
	.long	.Linfo_string146        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	623                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1572:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1577:0x5 DW_TAG_formal_parameter
	.long	5346                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x157d:0x17 DW_TAG_subprogram
	.long	.Linfo_string147        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	700                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1589:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x158e:0x5 DW_TAG_formal_parameter
	.long	5346                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1594:0x1c DW_TAG_subprogram
	.long	.Linfo_string148        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	373                     @ DW_AT_decl_line
	.long	4809                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x15a0:0x5 DW_TAG_formal_parameter
	.long	5552                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x15a5:0x5 DW_TAG_formal_parameter
	.long	4865                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x15aa:0x5 DW_TAG_formal_parameter
	.long	5087                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	38                      @ Abbrev [38] 0x15b0:0x5 DW_TAG_restrict_type
	.long	4702                    @ DW_AT_type
	.byte	46                      @ Abbrev [46] 0x15b5:0x16 DW_TAG_subprogram
	.long	.Linfo_string149        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.byte	157                     @ DW_AT_decl_line
	.long	4860                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x15c0:0x5 DW_TAG_formal_parameter
	.long	4872                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x15c5:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x15cb:0x16 DW_TAG_subprogram
	.long	.Linfo_string150        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.byte	166                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x15d6:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x15db:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x15e1:0x16 DW_TAG_subprogram
	.long	.Linfo_string151        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.byte	195                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x15ec:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x15f1:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x15f7:0x16 DW_TAG_subprogram
	.long	.Linfo_string152        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.byte	147                     @ DW_AT_decl_line
	.long	4860                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1602:0x5 DW_TAG_formal_parameter
	.long	4872                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1607:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x160d:0x16 DW_TAG_subprogram
	.long	.Linfo_string153        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.byte	255                     @ DW_AT_decl_line
	.long	4809                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1618:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x161d:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1623:0x21 DW_TAG_subprogram
	.long	.Linfo_string154        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	858                     @ DW_AT_decl_line
	.long	4809                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x162f:0x5 DW_TAG_formal_parameter
	.long	4872                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1634:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1639:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x163e:0x5 DW_TAG_formal_parameter
	.long	5700                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	38                      @ Abbrev [38] 0x1644:0x5 DW_TAG_restrict_type
	.long	5705                    @ DW_AT_type
	.byte	32                      @ Abbrev [32] 0x1649:0x5 DW_TAG_pointer_type
	.long	5710                    @ DW_AT_type
	.byte	39                      @ Abbrev [39] 0x164e:0x5 DW_TAG_const_type
	.long	5715                    @ DW_AT_type
	.byte	47                      @ Abbrev [47] 0x1653:0x5 DW_TAG_structure_type
	.long	.Linfo_string155        @ DW_AT_name
                                        @ DW_AT_declaration
	.byte	31                      @ Abbrev [31] 0x1658:0x12 DW_TAG_subprogram
	.long	.Linfo_string156        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	290                     @ DW_AT_decl_line
	.long	4809                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1664:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x166a:0x1b DW_TAG_subprogram
	.long	.Linfo_string157        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.byte	161                     @ DW_AT_decl_line
	.long	4860                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1675:0x5 DW_TAG_formal_parameter
	.long	4872                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x167a:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x167f:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1685:0x1b DW_TAG_subprogram
	.long	.Linfo_string158        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.byte	169                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1690:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1695:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x169a:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x16a0:0x1b DW_TAG_subprogram
	.long	.Linfo_string159        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.byte	152                     @ DW_AT_decl_line
	.long	4860                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x16ab:0x5 DW_TAG_formal_parameter
	.long	4872                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x16b0:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x16b5:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x16bb:0x21 DW_TAG_subprogram
	.long	.Linfo_string160        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	417                     @ DW_AT_decl_line
	.long	4809                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x16c7:0x5 DW_TAG_formal_parameter
	.long	5552                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x16cc:0x5 DW_TAG_formal_parameter
	.long	5852                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x16d1:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x16d6:0x5 DW_TAG_formal_parameter
	.long	5087                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	38                      @ Abbrev [38] 0x16dc:0x5 DW_TAG_restrict_type
	.long	5857                    @ DW_AT_type
	.byte	32                      @ Abbrev [32] 0x16e1:0x5 DW_TAG_pointer_type
	.long	4933                    @ DW_AT_type
	.byte	31                      @ Abbrev [31] 0x16e6:0x17 DW_TAG_subprogram
	.long	.Linfo_string161        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	259                     @ DW_AT_decl_line
	.long	4809                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x16f2:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x16f7:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x16fd:0x17 DW_TAG_subprogram
	.long	.Linfo_string162        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	453                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1709:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x170e:0x5 DW_TAG_formal_parameter
	.long	5915                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	3                       @ Abbrev [3] 0x1714:0x7 DW_TAG_base_type
	.long	.Linfo_string163        @ DW_AT_name
	.byte	4                       @ DW_AT_encoding
	.byte	8                       @ DW_AT_byte_size
	.byte	38                      @ Abbrev [38] 0x171b:0x5 DW_TAG_restrict_type
	.long	5920                    @ DW_AT_type
	.byte	32                      @ Abbrev [32] 0x1720:0x5 DW_TAG_pointer_type
	.long	4860                    @ DW_AT_type
	.byte	31                      @ Abbrev [31] 0x1725:0x17 DW_TAG_subprogram
	.long	.Linfo_string164        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	460                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1731:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1736:0x5 DW_TAG_formal_parameter
	.long	5915                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x173c:0x1c DW_TAG_subprogram
	.long	.Linfo_string165        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	285                     @ DW_AT_decl_line
	.long	4860                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1748:0x5 DW_TAG_formal_parameter
	.long	4872                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x174d:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1752:0x5 DW_TAG_formal_parameter
	.long	5915                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1758:0x1c DW_TAG_subprogram
	.long	.Linfo_string166        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	471                     @ DW_AT_decl_line
	.long	4734                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1764:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1769:0x5 DW_TAG_formal_parameter
	.long	5915                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x176e:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1774:0x1c DW_TAG_subprogram
	.long	.Linfo_string167        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	476                     @ DW_AT_decl_line
	.long	6032                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1780:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1785:0x5 DW_TAG_formal_parameter
	.long	5915                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x178a:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	3                       @ Abbrev [3] 0x1790:0x7 DW_TAG_base_type
	.long	.Linfo_string168        @ DW_AT_name
	.byte	7                       @ DW_AT_encoding
	.byte	4                       @ DW_AT_byte_size
	.byte	46                      @ Abbrev [46] 0x1797:0x1b DW_TAG_subprogram
	.long	.Linfo_string169        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.byte	199                     @ DW_AT_decl_line
	.long	4809                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x17a2:0x5 DW_TAG_formal_parameter
	.long	4872                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x17a7:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x17ac:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x17b2:0x12 DW_TAG_subprogram
	.long	.Linfo_string170        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	397                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x17be:0x5 DW_TAG_formal_parameter
	.long	4262                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x17c4:0x1c DW_TAG_subprogram
	.long	.Linfo_string171        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	328                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x17d0:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x17d5:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x17da:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x17e0:0x1c DW_TAG_subprogram
	.long	.Linfo_string172        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	332                     @ DW_AT_decl_line
	.long	4860                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x17ec:0x5 DW_TAG_formal_parameter
	.long	4872                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x17f1:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x17f6:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x17fc:0x1c DW_TAG_subprogram
	.long	.Linfo_string173        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	337                     @ DW_AT_decl_line
	.long	4860                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1808:0x5 DW_TAG_formal_parameter
	.long	4860                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x180d:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1812:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1818:0x1c DW_TAG_subprogram
	.long	.Linfo_string174        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	341                     @ DW_AT_decl_line
	.long	4860                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1824:0x5 DW_TAG_formal_parameter
	.long	4860                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1829:0x5 DW_TAG_formal_parameter
	.long	4865                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x182e:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1834:0x13 DW_TAG_subprogram
	.long	.Linfo_string175        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	604                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1840:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	40                      @ Abbrev [40] 0x1845:0x1 DW_TAG_unspecified_parameters
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1847:0x13 DW_TAG_subprogram
	.long	.Linfo_string176        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	645                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1853:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	40                      @ Abbrev [40] 0x1858:0x1 DW_TAG_unspecified_parameters
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x185a:0x16 DW_TAG_subprogram
	.long	.Linfo_string177        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.byte	230                     @ DW_AT_decl_line
	.long	4860                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1865:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x186a:0x5 DW_TAG_formal_parameter
	.long	4865                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1870:0x17 DW_TAG_subprogram
	.long	.Linfo_string178        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	269                     @ DW_AT_decl_line
	.long	4860                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x187c:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1881:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1887:0x16 DW_TAG_subprogram
	.long	.Linfo_string179        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.byte	240                     @ DW_AT_decl_line
	.long	4860                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1892:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1897:0x5 DW_TAG_formal_parameter
	.long	4865                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x189d:0x17 DW_TAG_subprogram
	.long	.Linfo_string180        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	280                     @ DW_AT_decl_line
	.long	4860                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x18a9:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x18ae:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x18b4:0x1c DW_TAG_subprogram
	.long	.Linfo_string181        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	323                     @ DW_AT_decl_line
	.long	4860                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x18c0:0x5 DW_TAG_formal_parameter
	.long	4933                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x18c5:0x5 DW_TAG_formal_parameter
	.long	4865                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x18ca:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	11                      @ Abbrev [11] 0x18d0:0xa9 DW_TAG_namespace
	.long	.Linfo_string182        @ DW_AT_name
	.byte	4                       @ DW_AT_decl_file
	.byte	225                     @ DW_AT_decl_line
	.byte	14                      @ Abbrev [14] 0x18d7:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	248                     @ DW_AT_decl_line
	.long	6521                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x18de:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	257                     @ DW_AT_decl_line
	.long	6551                    @ DW_AT_import
	.byte	15                      @ Abbrev [15] 0x18e6:0x8 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.short	258                     @ DW_AT_decl_line
	.long	6579                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x18ee:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	44                      @ DW_AT_decl_line
	.long	2350                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x18f5:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	45                      @ DW_AT_decl_line
	.long	2361                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x18fc:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	220                     @ DW_AT_decl_line
	.long	7995                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x1903:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	226                     @ DW_AT_decl_line
	.long	8035                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x190a:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	230                     @ DW_AT_decl_line
	.long	8049                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x1911:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	236                     @ DW_AT_decl_line
	.long	8067                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x1918:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	247                     @ DW_AT_decl_line
	.long	8090                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x191f:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	248                     @ DW_AT_decl_line
	.long	8107                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x1926:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	249                     @ DW_AT_decl_line
	.long	8134                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x192d:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	251                     @ DW_AT_decl_line
	.long	8161                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x1934:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	252                     @ DW_AT_decl_line
	.long	8183                    @ DW_AT_import
	.byte	22                      @ Abbrev [22] 0x193b:0x1a DW_TAG_subprogram
	.long	.Linfo_string305        @ DW_AT_linkage_name
	.long	.Linfo_string275        @ DW_AT_name
	.byte	18                      @ DW_AT_decl_file
	.byte	233                     @ DW_AT_decl_line
	.long	7995                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x194a:0x5 DW_TAG_formal_parameter
	.long	4801                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x194f:0x5 DW_TAG_formal_parameter
	.long	4801                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	14                      @ Abbrev [14] 0x1955:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	175                     @ DW_AT_decl_line
	.long	9152                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x195c:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	176                     @ DW_AT_decl_line
	.long	9181                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x1963:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	177                     @ DW_AT_decl_line
	.long	9209                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x196a:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	178                     @ DW_AT_decl_line
	.long	9232                    @ DW_AT_import
	.byte	14                      @ Abbrev [14] 0x1971:0x7 DW_TAG_imported_declaration
	.byte	4                       @ DW_AT_decl_file
	.byte	179                     @ DW_AT_decl_line
	.long	9265                    @ DW_AT_import
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1979:0x17 DW_TAG_subprogram
	.long	.Linfo_string183        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	462                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1985:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x198a:0x5 DW_TAG_formal_parameter
	.long	5915                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	3                       @ Abbrev [3] 0x1990:0x7 DW_TAG_base_type
	.long	.Linfo_string184        @ DW_AT_name
	.byte	4                       @ DW_AT_encoding
	.byte	8                       @ DW_AT_byte_size
	.byte	31                      @ Abbrev [31] 0x1997:0x1c DW_TAG_subprogram
	.long	.Linfo_string185        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	486                     @ DW_AT_decl_line
	.long	4801                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x19a3:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x19a8:0x5 DW_TAG_formal_parameter
	.long	5915                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x19ad:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x19b3:0x1c DW_TAG_subprogram
	.long	.Linfo_string186        @ DW_AT_name
	.byte	5                       @ DW_AT_decl_file
	.short	493                     @ DW_AT_decl_line
	.long	6607                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x19bf:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x19c4:0x5 DW_TAG_formal_parameter
	.long	5915                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x19c9:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	3                       @ Abbrev [3] 0x19cf:0x7 DW_TAG_base_type
	.long	.Linfo_string187        @ DW_AT_name
	.byte	7                       @ DW_AT_encoding
	.byte	8                       @ DW_AT_byte_size
	.byte	27                      @ Abbrev [27] 0x19d6:0xb DW_TAG_typedef
	.long	4748                    @ DW_AT_type
	.long	.Linfo_string188        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	36                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x19e1:0xb DW_TAG_typedef
	.long	6636                    @ DW_AT_type
	.long	.Linfo_string190        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	37                      @ DW_AT_decl_line
	.byte	3                       @ Abbrev [3] 0x19ec:0x7 DW_TAG_base_type
	.long	.Linfo_string189        @ DW_AT_name
	.byte	5                       @ DW_AT_encoding
	.byte	2                       @ DW_AT_byte_size
	.byte	27                      @ Abbrev [27] 0x19f3:0xb DW_TAG_typedef
	.long	58                      @ DW_AT_type
	.long	.Linfo_string191        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	38                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x19fe:0xb DW_TAG_typedef
	.long	4801                    @ DW_AT_type
	.long	.Linfo_string192        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	43                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1a09:0xb DW_TAG_typedef
	.long	4748                    @ DW_AT_type
	.long	.Linfo_string193        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	90                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1a14:0xb DW_TAG_typedef
	.long	58                      @ DW_AT_type
	.long	.Linfo_string194        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	96                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1a1f:0xb DW_TAG_typedef
	.long	58                      @ DW_AT_type
	.long	.Linfo_string195        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	97                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1a2a:0xb DW_TAG_typedef
	.long	4801                    @ DW_AT_type
	.long	.Linfo_string196        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	99                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1a35:0xb DW_TAG_typedef
	.long	4748                    @ DW_AT_type
	.long	.Linfo_string197        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	65                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1a40:0xb DW_TAG_typedef
	.long	6636                    @ DW_AT_type
	.long	.Linfo_string198        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	66                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1a4b:0xb DW_TAG_typedef
	.long	58                      @ DW_AT_type
	.long	.Linfo_string199        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	67                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1a56:0xb DW_TAG_typedef
	.long	4801                    @ DW_AT_type
	.long	.Linfo_string200        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	72                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1a61:0xb DW_TAG_typedef
	.long	4801                    @ DW_AT_type
	.long	.Linfo_string201        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	138                     @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1a6c:0xb DW_TAG_typedef
	.long	58                      @ DW_AT_type
	.long	.Linfo_string202        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	125                     @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1a77:0xb DW_TAG_typedef
	.long	6786                    @ DW_AT_type
	.long	.Linfo_string204        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	48                      @ DW_AT_decl_line
	.byte	3                       @ Abbrev [3] 0x1a82:0x7 DW_TAG_base_type
	.long	.Linfo_string203        @ DW_AT_name
	.byte	8                       @ DW_AT_encoding
	.byte	1                       @ DW_AT_byte_size
	.byte	27                      @ Abbrev [27] 0x1a89:0xb DW_TAG_typedef
	.long	4741                    @ DW_AT_type
	.long	.Linfo_string205        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	49                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1a94:0xb DW_TAG_typedef
	.long	4236                    @ DW_AT_type
	.long	.Linfo_string206        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	51                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1a9f:0xb DW_TAG_typedef
	.long	6607                    @ DW_AT_type
	.long	.Linfo_string207        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	58                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1aaa:0xb DW_TAG_typedef
	.long	6786                    @ DW_AT_type
	.long	.Linfo_string208        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	103                     @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1ab5:0xb DW_TAG_typedef
	.long	4236                    @ DW_AT_type
	.long	.Linfo_string209        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	109                     @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1ac0:0xb DW_TAG_typedef
	.long	4236                    @ DW_AT_type
	.long	.Linfo_string210        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	110                     @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1acb:0xb DW_TAG_typedef
	.long	6607                    @ DW_AT_type
	.long	.Linfo_string211        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	112                     @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1ad6:0xb DW_TAG_typedef
	.long	6786                    @ DW_AT_type
	.long	.Linfo_string212        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	76                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1ae1:0xb DW_TAG_typedef
	.long	4741                    @ DW_AT_type
	.long	.Linfo_string213        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	77                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1aec:0xb DW_TAG_typedef
	.long	4236                    @ DW_AT_type
	.long	.Linfo_string214        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	78                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1af7:0xb DW_TAG_typedef
	.long	6607                    @ DW_AT_type
	.long	.Linfo_string215        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	83                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1b02:0xb DW_TAG_typedef
	.long	6607                    @ DW_AT_type
	.long	.Linfo_string216        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	140                     @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x1b0d:0xb DW_TAG_typedef
	.long	4236                    @ DW_AT_type
	.long	.Linfo_string217        @ DW_AT_name
	.byte	11                      @ DW_AT_decl_file
	.byte	128                     @ DW_AT_decl_line
	.byte	32                      @ Abbrev [32] 0x1b18:0x5 DW_TAG_pointer_type
	.long	2002                    @ DW_AT_type
	.byte	32                      @ Abbrev [32] 0x1b1d:0x5 DW_TAG_pointer_type
	.long	6946                    @ DW_AT_type
	.byte	39                      @ Abbrev [39] 0x1b22:0x5 DW_TAG_const_type
	.long	2002                    @ DW_AT_type
	.byte	48                      @ Abbrev [48] 0x1b27:0x5 DW_TAG_reference_type
	.long	6946                    @ DW_AT_type
	.byte	49                      @ Abbrev [49] 0x1b2c:0x5 DW_TAG_unspecified_type
	.long	.Linfo_string227        @ DW_AT_name
	.byte	50                      @ Abbrev [50] 0x1b31:0x5 DW_TAG_rvalue_reference_type
	.long	2002                    @ DW_AT_type
	.byte	48                      @ Abbrev [48] 0x1b36:0x5 DW_TAG_reference_type
	.long	2002                    @ DW_AT_type
	.byte	3                       @ Abbrev [3] 0x1b3b:0x7 DW_TAG_base_type
	.long	.Linfo_string237        @ DW_AT_name
	.byte	2                       @ DW_AT_encoding
	.byte	1                       @ DW_AT_byte_size
	.byte	32                      @ Abbrev [32] 0x1b42:0x5 DW_TAG_pointer_type
	.long	6983                    @ DW_AT_type
	.byte	39                      @ Abbrev [39] 0x1b47:0x5 DW_TAG_const_type
	.long	2321                    @ DW_AT_type
	.byte	35                      @ Abbrev [35] 0x1b4c:0x6 DW_TAG_structure_type
	.long	.Linfo_string244        @ DW_AT_name
	.byte	56                      @ DW_AT_byte_size
                                        @ DW_AT_declaration
	.byte	46                      @ Abbrev [46] 0x1b52:0x16 DW_TAG_subprogram
	.long	.Linfo_string245        @ DW_AT_name
	.byte	13                      @ DW_AT_decl_file
	.byte	124                     @ DW_AT_decl_line
	.long	4702                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1b5d:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1b62:0x5 DW_TAG_formal_parameter
	.long	5077                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	51                      @ Abbrev [51] 0x1b68:0xb DW_TAG_subprogram
	.long	.Linfo_string246        @ DW_AT_name
	.byte	13                      @ DW_AT_decl_file
	.byte	127                     @ DW_AT_decl_line
	.long	7027                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	32                      @ Abbrev [32] 0x1b73:0x5 DW_TAG_pointer_type
	.long	6988                    @ DW_AT_type
	.byte	46                      @ Abbrev [46] 0x1b78:0x11 DW_TAG_subprogram
	.long	.Linfo_string247        @ DW_AT_name
	.byte	14                      @ DW_AT_decl_file
	.byte	110                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1b83:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1b89:0x11 DW_TAG_subprogram
	.long	.Linfo_string248        @ DW_AT_name
	.byte	14                      @ DW_AT_decl_file
	.byte	111                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1b94:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1b9a:0x11 DW_TAG_subprogram
	.long	.Linfo_string249        @ DW_AT_name
	.byte	14                      @ DW_AT_decl_file
	.byte	112                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1ba5:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1bab:0x11 DW_TAG_subprogram
	.long	.Linfo_string250        @ DW_AT_name
	.byte	14                      @ DW_AT_decl_file
	.byte	113                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1bb6:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1bbc:0x11 DW_TAG_subprogram
	.long	.Linfo_string251        @ DW_AT_name
	.byte	14                      @ DW_AT_decl_file
	.byte	115                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1bc7:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1bcd:0x11 DW_TAG_subprogram
	.long	.Linfo_string252        @ DW_AT_name
	.byte	14                      @ DW_AT_decl_file
	.byte	114                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1bd8:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1bde:0x11 DW_TAG_subprogram
	.long	.Linfo_string253        @ DW_AT_name
	.byte	14                      @ DW_AT_decl_file
	.byte	116                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1be9:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1bef:0x11 DW_TAG_subprogram
	.long	.Linfo_string254        @ DW_AT_name
	.byte	14                      @ DW_AT_decl_file
	.byte	117                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1bfa:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1c00:0x11 DW_TAG_subprogram
	.long	.Linfo_string255        @ DW_AT_name
	.byte	14                      @ DW_AT_decl_file
	.byte	118                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1c0b:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1c11:0x11 DW_TAG_subprogram
	.long	.Linfo_string256        @ DW_AT_name
	.byte	14                      @ DW_AT_decl_file
	.byte	119                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1c1c:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1c22:0x11 DW_TAG_subprogram
	.long	.Linfo_string257        @ DW_AT_name
	.byte	14                      @ DW_AT_decl_file
	.byte	120                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1c2d:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1c33:0x11 DW_TAG_subprogram
	.long	.Linfo_string258        @ DW_AT_name
	.byte	14                      @ DW_AT_decl_file
	.byte	124                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1c3e:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1c44:0x11 DW_TAG_subprogram
	.long	.Linfo_string259        @ DW_AT_name
	.byte	14                      @ DW_AT_decl_file
	.byte	127                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1c4f:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1c55:0x11 DW_TAG_subprogram
	.long	.Linfo_string260        @ DW_AT_name
	.byte	14                      @ DW_AT_decl_file
	.byte	136                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1c60:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	27                      @ Abbrev [27] 0x1c66:0xb DW_TAG_typedef
	.long	7281                    @ DW_AT_type
	.long	.Linfo_string261        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.byte	62                      @ DW_AT_decl_line
	.byte	52                      @ Abbrev [52] 0x1c71:0x2 DW_TAG_structure_type
	.byte	8                       @ DW_AT_byte_size
                                        @ DW_AT_declaration
	.byte	27                      @ Abbrev [27] 0x1c73:0xb DW_TAG_typedef
	.long	7294                    @ DW_AT_type
	.long	.Linfo_string264        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.byte	70                      @ DW_AT_decl_line
	.byte	29                      @ Abbrev [29] 0x1c7e:0x1d DW_TAG_structure_type
	.byte	8                       @ DW_AT_byte_size
	.byte	15                      @ DW_AT_decl_file
	.byte	66                      @ DW_AT_decl_line
	.byte	17                      @ Abbrev [17] 0x1c82:0xc DW_TAG_member
	.long	.Linfo_string262        @ DW_AT_name
	.long	4734                    @ DW_AT_type
	.byte	15                      @ DW_AT_decl_file
	.byte	68                      @ DW_AT_decl_line
	.byte	0                       @ DW_AT_data_member_location
	.byte	17                      @ Abbrev [17] 0x1c8e:0xc DW_TAG_member
	.long	.Linfo_string263        @ DW_AT_name
	.long	4734                    @ DW_AT_type
	.byte	15                      @ DW_AT_decl_file
	.byte	69                      @ DW_AT_decl_line
	.byte	4                       @ DW_AT_data_member_location
	.byte	0                       @ End Of Children Mark
	.byte	53                      @ Abbrev [53] 0x1c9b:0x8 DW_TAG_subprogram
	.long	.Linfo_string265        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	476                     @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	31                      @ Abbrev [31] 0x1ca3:0x12 DW_TAG_subprogram
	.long	.Linfo_string266        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	735                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1caf:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1cb5:0x12 DW_TAG_subprogram
	.long	.Linfo_string267        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	480                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1cc1:0x5 DW_TAG_formal_parameter
	.long	7367                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	32                      @ Abbrev [32] 0x1cc7:0x5 DW_TAG_pointer_type
	.long	7372                    @ DW_AT_type
	.byte	54                      @ Abbrev [54] 0x1ccc:0x1 DW_TAG_subroutine_type
	.byte	31                      @ Abbrev [31] 0x1ccd:0x12 DW_TAG_subprogram
	.long	.Linfo_string268        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	485                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1cd9:0x5 DW_TAG_formal_parameter
	.long	7367                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1cdf:0x11 DW_TAG_subprogram
	.long	.Linfo_string269        @ DW_AT_name
	.byte	16                      @ DW_AT_decl_file
	.byte	26                      @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1cea:0x5 DW_TAG_formal_parameter
	.long	5077                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1cf0:0x11 DW_TAG_subprogram
	.long	.Linfo_string270        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.byte	239                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1cfb:0x5 DW_TAG_formal_parameter
	.long	5077                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1d01:0x11 DW_TAG_subprogram
	.long	.Linfo_string271        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.byte	244                     @ DW_AT_decl_line
	.long	4734                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1d0c:0x5 DW_TAG_formal_parameter
	.long	5077                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1d12:0x25 DW_TAG_subprogram
	.long	.Linfo_string272        @ DW_AT_name
	.byte	17                      @ DW_AT_decl_file
	.byte	20                      @ DW_AT_decl_line
	.long	4808                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1d1d:0x5 DW_TAG_formal_parameter
	.long	7479                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1d22:0x5 DW_TAG_formal_parameter
	.long	7479                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1d27:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1d2c:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1d31:0x5 DW_TAG_formal_parameter
	.long	7485                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	32                      @ Abbrev [32] 0x1d37:0x5 DW_TAG_pointer_type
	.long	7484                    @ DW_AT_type
	.byte	55                      @ Abbrev [55] 0x1d3c:0x1 DW_TAG_const_type
	.byte	56                      @ Abbrev [56] 0x1d3d:0xc DW_TAG_typedef
	.long	7497                    @ DW_AT_type
	.long	.Linfo_string273        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	702                     @ DW_AT_decl_line
	.byte	32                      @ Abbrev [32] 0x1d49:0x5 DW_TAG_pointer_type
	.long	7502                    @ DW_AT_type
	.byte	57                      @ Abbrev [57] 0x1d4e:0x10 DW_TAG_subroutine_type
	.long	58                      @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1d53:0x5 DW_TAG_formal_parameter
	.long	7479                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1d58:0x5 DW_TAG_formal_parameter
	.long	7479                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1d5e:0x17 DW_TAG_subprogram
	.long	.Linfo_string274        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	429                     @ DW_AT_decl_line
	.long	4808                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1d6a:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1d6f:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1d75:0x17 DW_TAG_subprogram
	.long	.Linfo_string275        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	749                     @ DW_AT_decl_line
	.long	7270                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1d81:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1d86:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	58                      @ Abbrev [58] 0x1d8c:0xe DW_TAG_subprogram
	.long	.Linfo_string276        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	504                     @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1d94:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	58                      @ Abbrev [58] 0x1d9a:0xe DW_TAG_subprogram
	.long	.Linfo_string277        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	444                     @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1da2:0x5 DW_TAG_formal_parameter
	.long	4808                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1da8:0x12 DW_TAG_subprogram
	.long	.Linfo_string278        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	525                     @ DW_AT_decl_line
	.long	4702                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1db4:0x5 DW_TAG_formal_parameter
	.long	5077                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1dba:0x12 DW_TAG_subprogram
	.long	.Linfo_string279        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	736                     @ DW_AT_decl_line
	.long	4734                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1dc6:0x5 DW_TAG_formal_parameter
	.long	4734                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1dcc:0x17 DW_TAG_subprogram
	.long	.Linfo_string280        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	751                     @ DW_AT_decl_line
	.long	7283                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1dd8:0x5 DW_TAG_formal_parameter
	.long	4734                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1ddd:0x5 DW_TAG_formal_parameter
	.long	4734                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1de3:0x12 DW_TAG_subprogram
	.long	.Linfo_string281        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	427                     @ DW_AT_decl_line
	.long	4808                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1def:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1df5:0x17 DW_TAG_subprogram
	.long	.Linfo_string282        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	823                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1e01:0x5 DW_TAG_formal_parameter
	.long	5077                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1e06:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1e0c:0x1c DW_TAG_subprogram
	.long	.Linfo_string283        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	834                     @ DW_AT_decl_line
	.long	4809                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1e18:0x5 DW_TAG_formal_parameter
	.long	4872                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1e1d:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1e22:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1e28:0x1c DW_TAG_subprogram
	.long	.Linfo_string284        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	826                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1e34:0x5 DW_TAG_formal_parameter
	.long	4872                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1e39:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1e3e:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	58                      @ Abbrev [58] 0x1e44:0x1d DW_TAG_subprogram
	.long	.Linfo_string285        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	725                     @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1e4c:0x5 DW_TAG_formal_parameter
	.long	4808                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1e51:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1e56:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1e5b:0x5 DW_TAG_formal_parameter
	.long	7485                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	58                      @ Abbrev [58] 0x1e61:0xe DW_TAG_subprogram
	.long	.Linfo_string286        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	510                     @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1e69:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	41                      @ Abbrev [41] 0x1e6f:0xc DW_TAG_subprogram
	.long	.Linfo_string287        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	335                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	31                      @ Abbrev [31] 0x1e7b:0x17 DW_TAG_subprogram
	.long	.Linfo_string288        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	441                     @ DW_AT_decl_line
	.long	4808                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1e87:0x5 DW_TAG_formal_parameter
	.long	4808                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1e8c:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	58                      @ Abbrev [58] 0x1e92:0xe DW_TAG_subprogram
	.long	.Linfo_string289        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	337                     @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1e9a:0x5 DW_TAG_formal_parameter
	.long	4236                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1ea0:0x16 DW_TAG_subprogram
	.long	.Linfo_string290        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.byte	125                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1eab:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1eb0:0x5 DW_TAG_formal_parameter
	.long	7862                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	38                      @ Abbrev [38] 0x1eb6:0x5 DW_TAG_restrict_type
	.long	7867                    @ DW_AT_type
	.byte	32                      @ Abbrev [32] 0x1ebb:0x5 DW_TAG_pointer_type
	.long	4702                    @ DW_AT_type
	.byte	46                      @ Abbrev [46] 0x1ec0:0x1b DW_TAG_subprogram
	.long	.Linfo_string291        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.byte	144                     @ DW_AT_decl_line
	.long	4734                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1ecb:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1ed0:0x5 DW_TAG_formal_parameter
	.long	7862                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1ed5:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1edb:0x1b DW_TAG_subprogram
	.long	.Linfo_string292        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.byte	148                     @ DW_AT_decl_line
	.long	6032                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1ee6:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1eeb:0x5 DW_TAG_formal_parameter
	.long	7862                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1ef0:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1ef6:0x12 DW_TAG_subprogram
	.long	.Linfo_string293        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	677                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1f02:0x5 DW_TAG_formal_parameter
	.long	5077                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1f08:0x1c DW_TAG_subprogram
	.long	.Linfo_string294        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	837                     @ DW_AT_decl_line
	.long	4809                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1f14:0x5 DW_TAG_formal_parameter
	.long	5552                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1f19:0x5 DW_TAG_formal_parameter
	.long	4928                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1f1e:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1f24:0x17 DW_TAG_subprogram
	.long	.Linfo_string295        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	830                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1f30:0x5 DW_TAG_formal_parameter
	.long	4702                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1f35:0x5 DW_TAG_formal_parameter
	.long	4865                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	27                      @ Abbrev [27] 0x1f3b:0xb DW_TAG_typedef
	.long	8006                    @ DW_AT_type
	.long	.Linfo_string296        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.byte	82                      @ DW_AT_decl_line
	.byte	29                      @ Abbrev [29] 0x1f46:0x1d DW_TAG_structure_type
	.byte	16                      @ DW_AT_byte_size
	.byte	15                      @ DW_AT_decl_file
	.byte	78                      @ DW_AT_decl_line
	.byte	17                      @ Abbrev [17] 0x1f4a:0xc DW_TAG_member
	.long	.Linfo_string262        @ DW_AT_name
	.long	4801                    @ DW_AT_type
	.byte	15                      @ DW_AT_decl_file
	.byte	80                      @ DW_AT_decl_line
	.byte	0                       @ DW_AT_data_member_location
	.byte	17                      @ Abbrev [17] 0x1f56:0xc DW_TAG_member
	.long	.Linfo_string263        @ DW_AT_name
	.long	4801                    @ DW_AT_type
	.byte	15                      @ DW_AT_decl_file
	.byte	81                      @ DW_AT_decl_line
	.byte	8                       @ DW_AT_data_member_location
	.byte	0                       @ End Of Children Mark
	.byte	58                      @ Abbrev [58] 0x1f63:0xe DW_TAG_subprogram
	.long	.Linfo_string297        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	518                     @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1f6b:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1f71:0x12 DW_TAG_subprogram
	.long	.Linfo_string298        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	740                     @ DW_AT_decl_line
	.long	4801                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1f7d:0x5 DW_TAG_formal_parameter
	.long	4801                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x1f83:0x17 DW_TAG_subprogram
	.long	.Linfo_string299        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.short	757                     @ DW_AT_decl_line
	.long	7995                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1f8f:0x5 DW_TAG_formal_parameter
	.long	4801                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1f94:0x5 DW_TAG_formal_parameter
	.long	4801                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1f9a:0x11 DW_TAG_subprogram
	.long	.Linfo_string300        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.byte	253                     @ DW_AT_decl_line
	.long	4801                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1fa5:0x5 DW_TAG_formal_parameter
	.long	5077                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1fab:0x1b DW_TAG_subprogram
	.long	.Linfo_string301        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.byte	170                     @ DW_AT_decl_line
	.long	4801                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1fb6:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1fbb:0x5 DW_TAG_formal_parameter
	.long	7862                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1fc0:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1fc6:0x1b DW_TAG_subprogram
	.long	.Linfo_string302        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.byte	175                     @ DW_AT_decl_line
	.long	6607                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1fd1:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1fd6:0x5 DW_TAG_formal_parameter
	.long	7862                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1fdb:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1fe1:0x16 DW_TAG_subprogram
	.long	.Linfo_string303        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.byte	133                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x1fec:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x1ff1:0x5 DW_TAG_formal_parameter
	.long	7862                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x1ff7:0x16 DW_TAG_subprogram
	.long	.Linfo_string304        @ DW_AT_name
	.byte	15                      @ DW_AT_decl_file
	.byte	136                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2002:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2007:0x5 DW_TAG_formal_parameter
	.long	7862                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	27                      @ Abbrev [27] 0x200d:0xb DW_TAG_typedef
	.long	4325                    @ DW_AT_type
	.long	.Linfo_string306        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.byte	48                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x2018:0xb DW_TAG_typedef
	.long	8227                    @ DW_AT_type
	.long	.Linfo_string308        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.byte	112                     @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x2023:0xb DW_TAG_typedef
	.long	8238                    @ DW_AT_type
	.long	.Linfo_string307        @ DW_AT_name
	.byte	19                      @ DW_AT_decl_file
	.byte	25                      @ DW_AT_decl_line
	.byte	52                      @ Abbrev [52] 0x202e:0x2 DW_TAG_structure_type
	.byte	12                      @ DW_AT_byte_size
                                        @ DW_AT_declaration
	.byte	58                      @ Abbrev [58] 0x2030:0xe DW_TAG_subprogram
	.long	.Linfo_string309        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	828                     @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2038:0x5 DW_TAG_formal_parameter
	.long	8254                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	32                      @ Abbrev [32] 0x203e:0x5 DW_TAG_pointer_type
	.long	8205                    @ DW_AT_type
	.byte	46                      @ Abbrev [46] 0x2043:0x11 DW_TAG_subprogram
	.long	.Linfo_string310        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.byte	239                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x204e:0x5 DW_TAG_formal_parameter
	.long	8254                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2054:0x12 DW_TAG_subprogram
	.long	.Linfo_string311        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	830                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2060:0x5 DW_TAG_formal_parameter
	.long	8254                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2066:0x12 DW_TAG_subprogram
	.long	.Linfo_string312        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	832                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2072:0x5 DW_TAG_formal_parameter
	.long	8254                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2078:0x11 DW_TAG_subprogram
	.long	.Linfo_string313        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.byte	244                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2083:0x5 DW_TAG_formal_parameter
	.long	8254                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2089:0x12 DW_TAG_subprogram
	.long	.Linfo_string314        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	533                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2095:0x5 DW_TAG_formal_parameter
	.long	8254                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x209b:0x17 DW_TAG_subprogram
	.long	.Linfo_string315        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	800                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x20a7:0x5 DW_TAG_formal_parameter
	.long	8370                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x20ac:0x5 DW_TAG_formal_parameter
	.long	8375                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	38                      @ Abbrev [38] 0x20b2:0x5 DW_TAG_restrict_type
	.long	8254                    @ DW_AT_type
	.byte	38                      @ Abbrev [38] 0x20b7:0x5 DW_TAG_restrict_type
	.long	8380                    @ DW_AT_type
	.byte	32                      @ Abbrev [32] 0x20bc:0x5 DW_TAG_pointer_type
	.long	8216                    @ DW_AT_type
	.byte	31                      @ Abbrev [31] 0x20c1:0x1c DW_TAG_subprogram
	.long	.Linfo_string316        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	624                     @ DW_AT_decl_line
	.long	4702                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x20cd:0x5 DW_TAG_formal_parameter
	.long	5552                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x20d2:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x20d7:0x5 DW_TAG_formal_parameter
	.long	8370                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x20dd:0x17 DW_TAG_subprogram
	.long	.Linfo_string317        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	274                     @ DW_AT_decl_line
	.long	8254                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x20e9:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x20ee:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x20f4:0x18 DW_TAG_subprogram
	.long	.Linfo_string318        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	358                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2100:0x5 DW_TAG_formal_parameter
	.long	8370                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2105:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	40                      @ Abbrev [40] 0x210a:0x1 DW_TAG_unspecified_parameters
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x210c:0x17 DW_TAG_subprogram
	.long	.Linfo_string319        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	575                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2118:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x211d:0x5 DW_TAG_formal_parameter
	.long	8254                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2123:0x17 DW_TAG_subprogram
	.long	.Linfo_string320        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	691                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x212f:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2134:0x5 DW_TAG_formal_parameter
	.long	8370                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x213a:0x21 DW_TAG_subprogram
	.long	.Linfo_string321        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	711                     @ DW_AT_decl_line
	.long	4809                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2146:0x5 DW_TAG_formal_parameter
	.long	8539                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x214b:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2150:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2155:0x5 DW_TAG_formal_parameter
	.long	8370                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	38                      @ Abbrev [38] 0x215b:0x5 DW_TAG_restrict_type
	.long	4808                    @ DW_AT_type
	.byte	31                      @ Abbrev [31] 0x2160:0x1c DW_TAG_subprogram
	.long	.Linfo_string322        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	280                     @ DW_AT_decl_line
	.long	8254                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x216c:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2171:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2176:0x5 DW_TAG_formal_parameter
	.long	8370                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x217c:0x18 DW_TAG_subprogram
	.long	.Linfo_string323        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	427                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2188:0x5 DW_TAG_formal_parameter
	.long	8370                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x218d:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	40                      @ Abbrev [40] 0x2192:0x1 DW_TAG_unspecified_parameters
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2194:0x1c DW_TAG_subprogram
	.long	.Linfo_string324        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	751                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x21a0:0x5 DW_TAG_formal_parameter
	.long	8254                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x21a5:0x5 DW_TAG_formal_parameter
	.long	4734                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x21aa:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x21b0:0x17 DW_TAG_subprogram
	.long	.Linfo_string325        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	805                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x21bc:0x5 DW_TAG_formal_parameter
	.long	8254                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x21c1:0x5 DW_TAG_formal_parameter
	.long	8647                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	32                      @ Abbrev [32] 0x21c7:0x5 DW_TAG_pointer_type
	.long	8652                    @ DW_AT_type
	.byte	39                      @ Abbrev [39] 0x21cc:0x5 DW_TAG_const_type
	.long	8216                    @ DW_AT_type
	.byte	31                      @ Abbrev [31] 0x21d1:0x12 DW_TAG_subprogram
	.long	.Linfo_string326        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	756                     @ DW_AT_decl_line
	.long	4734                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x21dd:0x5 DW_TAG_formal_parameter
	.long	8254                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x21e3:0x21 DW_TAG_subprogram
	.long	.Linfo_string327        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	717                     @ DW_AT_decl_line
	.long	4809                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x21ef:0x5 DW_TAG_formal_parameter
	.long	8708                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x21f4:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x21f9:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x21fe:0x5 DW_TAG_formal_parameter
	.long	8370                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	38                      @ Abbrev [38] 0x2204:0x5 DW_TAG_restrict_type
	.long	7479                    @ DW_AT_type
	.byte	31                      @ Abbrev [31] 0x2209:0x12 DW_TAG_subprogram
	.long	.Linfo_string328        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	534                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2215:0x5 DW_TAG_formal_parameter
	.long	8254                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	51                      @ Abbrev [51] 0x221b:0xb DW_TAG_subprogram
	.long	.Linfo_string329        @ DW_AT_name
	.byte	20                      @ DW_AT_decl_file
	.byte	44                      @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	31                      @ Abbrev [31] 0x2226:0x12 DW_TAG_subprogram
	.long	.Linfo_string330        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	640                     @ DW_AT_decl_line
	.long	4702                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2232:0x5 DW_TAG_formal_parameter
	.long	4702                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	58                      @ Abbrev [58] 0x2238:0xe DW_TAG_subprogram
	.long	.Linfo_string331        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	848                     @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2240:0x5 DW_TAG_formal_parameter
	.long	5077                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2246:0x13 DW_TAG_subprogram
	.long	.Linfo_string332        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	364                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2252:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	40                      @ Abbrev [40] 0x2257:0x1 DW_TAG_unspecified_parameters
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2259:0x17 DW_TAG_subprogram
	.long	.Linfo_string333        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	576                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2265:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x226a:0x5 DW_TAG_formal_parameter
	.long	8254                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2270:0x11 DW_TAG_subprogram
	.long	.Linfo_string334        @ DW_AT_name
	.byte	20                      @ DW_AT_decl_file
	.byte	79                      @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x227b:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2281:0x12 DW_TAG_subprogram
	.long	.Linfo_string335        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	697                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x228d:0x5 DW_TAG_formal_parameter
	.long	5077                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2293:0x11 DW_TAG_subprogram
	.long	.Linfo_string336        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.byte	180                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x229e:0x5 DW_TAG_formal_parameter
	.long	5077                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x22a4:0x16 DW_TAG_subprogram
	.long	.Linfo_string337        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.byte	182                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x22af:0x5 DW_TAG_formal_parameter
	.long	5077                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x22b4:0x5 DW_TAG_formal_parameter
	.long	5077                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	58                      @ Abbrev [58] 0x22ba:0xe DW_TAG_subprogram
	.long	.Linfo_string338        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	761                     @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x22c2:0x5 DW_TAG_formal_parameter
	.long	8254                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x22c8:0x13 DW_TAG_subprogram
	.long	.Linfo_string339        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	433                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x22d4:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	40                      @ Abbrev [40] 0x22d9:0x1 DW_TAG_unspecified_parameters
	.byte	0                       @ End Of Children Mark
	.byte	58                      @ Abbrev [58] 0x22db:0x13 DW_TAG_subprogram
	.long	.Linfo_string340        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	334                     @ DW_AT_decl_line
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x22e3:0x5 DW_TAG_formal_parameter
	.long	8370                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x22e8:0x5 DW_TAG_formal_parameter
	.long	5552                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x22ee:0x21 DW_TAG_subprogram
	.long	.Linfo_string341        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	338                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x22fa:0x5 DW_TAG_formal_parameter
	.long	8370                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x22ff:0x5 DW_TAG_formal_parameter
	.long	5552                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2304:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2309:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x230f:0x18 DW_TAG_subprogram
	.long	.Linfo_string342        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	366                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x231b:0x5 DW_TAG_formal_parameter
	.long	5552                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2320:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	40                      @ Abbrev [40] 0x2325:0x1 DW_TAG_unspecified_parameters
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2327:0x18 DW_TAG_subprogram
	.long	.Linfo_string343        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	435                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2333:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2338:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	40                      @ Abbrev [40] 0x233d:0x1 DW_TAG_unspecified_parameters
	.byte	0                       @ End Of Children Mark
	.byte	51                      @ Abbrev [51] 0x233f:0xb DW_TAG_subprogram
	.long	.Linfo_string344        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.byte	197                     @ DW_AT_decl_line
	.long	8254                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	46                      @ Abbrev [46] 0x234a:0x11 DW_TAG_subprogram
	.long	.Linfo_string345        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.byte	211                     @ DW_AT_decl_line
	.long	4702                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2355:0x5 DW_TAG_formal_parameter
	.long	4702                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x235b:0x17 DW_TAG_subprogram
	.long	.Linfo_string346        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	704                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2367:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x236c:0x5 DW_TAG_formal_parameter
	.long	8254                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2372:0x1c DW_TAG_subprogram
	.long	.Linfo_string347        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	373                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x237e:0x5 DW_TAG_formal_parameter
	.long	8370                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2383:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2388:0x5 DW_TAG_formal_parameter
	.long	5346                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x238e:0x16 DW_TAG_subprogram
	.long	.Linfo_string348        @ DW_AT_name
	.byte	20                      @ DW_AT_decl_file
	.byte	36                      @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2399:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x239e:0x5 DW_TAG_formal_parameter
	.long	5346                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x23a4:0x1c DW_TAG_subprogram
	.long	.Linfo_string349        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	381                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x23b0:0x5 DW_TAG_formal_parameter
	.long	5552                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x23b5:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x23ba:0x5 DW_TAG_formal_parameter
	.long	5346                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x23c0:0x1d DW_TAG_subprogram
	.long	.Linfo_string350        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	388                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x23cc:0x5 DW_TAG_formal_parameter
	.long	5552                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x23d1:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x23d6:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	40                      @ Abbrev [40] 0x23db:0x1 DW_TAG_unspecified_parameters
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x23dd:0x1c DW_TAG_subprogram
	.long	.Linfo_string351        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	473                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x23e9:0x5 DW_TAG_formal_parameter
	.long	8370                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x23ee:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x23f3:0x5 DW_TAG_formal_parameter
	.long	5346                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x23f9:0x17 DW_TAG_subprogram
	.long	.Linfo_string352        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	481                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2405:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x240a:0x5 DW_TAG_formal_parameter
	.long	5346                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2410:0x21 DW_TAG_subprogram
	.long	.Linfo_string353        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	392                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x241c:0x5 DW_TAG_formal_parameter
	.long	5552                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2421:0x5 DW_TAG_formal_parameter
	.long	4809                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2426:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x242b:0x5 DW_TAG_formal_parameter
	.long	5346                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2431:0x1c DW_TAG_subprogram
	.long	.Linfo_string354        @ DW_AT_name
	.byte	9                       @ DW_AT_decl_file
	.short	485                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x243d:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2442:0x5 DW_TAG_formal_parameter
	.long	5072                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2447:0x5 DW_TAG_formal_parameter
	.long	5346                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x244d:0x15 DW_TAG_subprogram
	.long	.Linfo_string355        @ DW_AT_linkage_name
	.long	.Linfo_string356        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	24                      @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x245c:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x2462:0x15 DW_TAG_subprogram
	.long	.Linfo_string357        @ DW_AT_linkage_name
	.long	.Linfo_string358        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	52                      @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2471:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2477:0x11 DW_TAG_subprogram
	.long	.Linfo_string359        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	58                      @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2482:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x2488:0x1a DW_TAG_subprogram
	.long	.Linfo_string360        @ DW_AT_linkage_name
	.long	.Linfo_string361        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	65                      @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2497:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x249c:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x24a2:0x11 DW_TAG_subprogram
	.long	.Linfo_string362        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	178                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x24ad:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x24b3:0x11 DW_TAG_subprogram
	.long	.Linfo_string363        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	63                      @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x24be:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x24c4:0x15 DW_TAG_subprogram
	.long	.Linfo_string364        @ DW_AT_linkage_name
	.long	.Linfo_string365        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	95                      @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x24d3:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x24d9:0x15 DW_TAG_subprogram
	.long	.Linfo_string366        @ DW_AT_linkage_name
	.long	.Linfo_string367        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	108                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x24e8:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x24ee:0x11 DW_TAG_subprogram
	.long	.Linfo_string368        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	181                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x24f9:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x24ff:0x11 DW_TAG_subprogram
	.long	.Linfo_string369        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	184                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x250a:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x2510:0x1a DW_TAG_subprogram
	.long	.Linfo_string370        @ DW_AT_linkage_name
	.long	.Linfo_string371        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	158                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x251f:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2524:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x252a:0x16 DW_TAG_subprogram
	.long	.Linfo_string372        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	103                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2535:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x253a:0x5 DW_TAG_formal_parameter
	.long	9536                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	32                      @ Abbrev [32] 0x2540:0x5 DW_TAG_pointer_type
	.long	58                      @ DW_AT_type
	.byte	46                      @ Abbrev [46] 0x2545:0x16 DW_TAG_subprogram
	.long	.Linfo_string373        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	106                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2550:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2555:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	59                      @ Abbrev [59] 0x255b:0x16 DW_TAG_subprogram
	.long	.Linfo_string374        @ DW_AT_linkage_name
	.long	.Linfo_string375        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.short	363                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x256b:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	59                      @ Abbrev [59] 0x2571:0x16 DW_TAG_subprogram
	.long	.Linfo_string376        @ DW_AT_linkage_name
	.long	.Linfo_string377        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.short	376                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2581:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2587:0x16 DW_TAG_subprogram
	.long	.Linfo_string378        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	115                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2592:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2597:0x5 DW_TAG_formal_parameter
	.long	9629                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	32                      @ Abbrev [32] 0x259d:0x5 DW_TAG_pointer_type
	.long	5908                    @ DW_AT_type
	.byte	59                      @ Abbrev [59] 0x25a2:0x1b DW_TAG_subprogram
	.long	.Linfo_string379        @ DW_AT_linkage_name
	.long	.Linfo_string380        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.short	402                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x25b2:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x25b7:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x25bd:0x11 DW_TAG_subprogram
	.long	.Linfo_string381        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	65                      @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x25c8:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	59                      @ Abbrev [59] 0x25ce:0x16 DW_TAG_subprogram
	.long	.Linfo_string382        @ DW_AT_linkage_name
	.long	.Linfo_string383        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.short	452                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x25de:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	59                      @ Abbrev [59] 0x25e4:0x16 DW_TAG_subprogram
	.long	.Linfo_string384        @ DW_AT_linkage_name
	.long	.Linfo_string385        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.short	465                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x25f4:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x25fa:0x11 DW_TAG_subprogram
	.long	.Linfo_string386        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	67                      @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2605:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x260b:0x11 DW_TAG_subprogram
	.long	.Linfo_string387        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	76                      @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2616:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	27                      @ Abbrev [27] 0x261c:0xb DW_TAG_typedef
	.long	5908                    @ DW_AT_type
	.long	.Linfo_string388        @ DW_AT_name
	.byte	23                      @ DW_AT_decl_file
	.byte	28                      @ DW_AT_decl_line
	.byte	27                      @ Abbrev [27] 0x2627:0xb DW_TAG_typedef
	.long	262                     @ DW_AT_type
	.long	.Linfo_string389        @ DW_AT_name
	.byte	23                      @ DW_AT_decl_file
	.byte	26                      @ DW_AT_decl_line
	.byte	22                      @ Abbrev [22] 0x2632:0x15 DW_TAG_subprogram
	.long	.Linfo_string390        @ DW_AT_linkage_name
	.long	.Linfo_string391        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	38                      @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2641:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x2647:0x15 DW_TAG_subprogram
	.long	.Linfo_string392        @ DW_AT_linkage_name
	.long	.Linfo_string393        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	41                      @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2656:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x265c:0x15 DW_TAG_subprogram
	.long	.Linfo_string390        @ DW_AT_linkage_name
	.long	.Linfo_string394        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	44                      @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x266b:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2671:0x11 DW_TAG_subprogram
	.long	.Linfo_string395        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	90                      @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x267c:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2682:0x11 DW_TAG_subprogram
	.long	.Linfo_string396        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	90                      @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x268d:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2693:0x11 DW_TAG_subprogram
	.long	.Linfo_string397        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	90                      @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x269e:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x26a4:0x15 DW_TAG_subprogram
	.long	.Linfo_string398        @ DW_AT_linkage_name
	.long	.Linfo_string399        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	81                      @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x26b3:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x26b9:0x15 DW_TAG_subprogram
	.long	.Linfo_string400        @ DW_AT_linkage_name
	.long	.Linfo_string401        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	84                      @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x26c8:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x26ce:0x15 DW_TAG_subprogram
	.long	.Linfo_string398        @ DW_AT_linkage_name
	.long	.Linfo_string402        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	87                      @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x26dd:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x26e3:0x11 DW_TAG_subprogram
	.long	.Linfo_string403        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	169                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x26ee:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x26f4:0x11 DW_TAG_subprogram
	.long	.Linfo_string404        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	169                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x26ff:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2705:0x11 DW_TAG_subprogram
	.long	.Linfo_string405        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	169                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2710:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2716:0x16 DW_TAG_subprogram
	.long	.Linfo_string406        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	221                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2721:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2726:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x272c:0x16 DW_TAG_subprogram
	.long	.Linfo_string407        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	221                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2737:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x273c:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2742:0x16 DW_TAG_subprogram
	.long	.Linfo_string408        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	221                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x274d:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2752:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2758:0x12 DW_TAG_subprogram
	.long	.Linfo_string409        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	259                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2764:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x276a:0x12 DW_TAG_subprogram
	.long	.Linfo_string410        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	259                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2776:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x277c:0x12 DW_TAG_subprogram
	.long	.Linfo_string411        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	259                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2788:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x278e:0x12 DW_TAG_subprogram
	.long	.Linfo_string412        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	260                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x279a:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x27a0:0x12 DW_TAG_subprogram
	.long	.Linfo_string413        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	260                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x27ac:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x27b2:0x12 DW_TAG_subprogram
	.long	.Linfo_string414        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	260                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x27be:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x27c4:0x15 DW_TAG_subprogram
	.long	.Linfo_string415        @ DW_AT_linkage_name
	.long	.Linfo_string416        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	146                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x27d3:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x27d9:0x15 DW_TAG_subprogram
	.long	.Linfo_string417        @ DW_AT_linkage_name
	.long	.Linfo_string418        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	147                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x27e8:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x27ee:0x15 DW_TAG_subprogram
	.long	.Linfo_string415        @ DW_AT_linkage_name
	.long	.Linfo_string419        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	150                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x27fd:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2803:0x11 DW_TAG_subprogram
	.long	.Linfo_string420        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	128                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x280e:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2814:0x11 DW_TAG_subprogram
	.long	.Linfo_string421        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	128                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x281f:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2825:0x11 DW_TAG_subprogram
	.long	.Linfo_string422        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	128                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2830:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2836:0x17 DW_TAG_subprogram
	.long	.Linfo_string423        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	354                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2842:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2847:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x284d:0x17 DW_TAG_subprogram
	.long	.Linfo_string424        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	354                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2859:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x285e:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2864:0x17 DW_TAG_subprogram
	.long	.Linfo_string425        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	354                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2870:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2875:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x287b:0x1c DW_TAG_subprogram
	.long	.Linfo_string426        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	373                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2887:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x288c:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2891:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2897:0x1c DW_TAG_subprogram
	.long	.Linfo_string427        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	373                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x28a3:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x28a8:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x28ad:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x28b3:0x1c DW_TAG_subprogram
	.long	.Linfo_string428        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	373                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x28bf:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x28c4:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x28c9:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x28cf:0x17 DW_TAG_subprogram
	.long	.Linfo_string429        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	357                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x28db:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x28e0:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x28e6:0x17 DW_TAG_subprogram
	.long	.Linfo_string430        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	357                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x28f2:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x28f7:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x28fd:0x17 DW_TAG_subprogram
	.long	.Linfo_string431        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	357                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2909:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x290e:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2914:0x17 DW_TAG_subprogram
	.long	.Linfo_string432        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	360                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2920:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2925:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x292b:0x17 DW_TAG_subprogram
	.long	.Linfo_string433        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	360                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2937:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x293c:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2942:0x17 DW_TAG_subprogram
	.long	.Linfo_string434        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	360                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x294e:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2953:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x2959:0x1a DW_TAG_subprogram
	.long	.Linfo_string435        @ DW_AT_linkage_name
	.long	.Linfo_string436        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	174                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2968:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x296d:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x2973:0x1a DW_TAG_subprogram
	.long	.Linfo_string437        @ DW_AT_linkage_name
	.long	.Linfo_string438        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	177                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2982:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2987:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	22                      @ Abbrev [22] 0x298d:0x1a DW_TAG_subprogram
	.long	.Linfo_string435        @ DW_AT_linkage_name
	.long	.Linfo_string439        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.byte	180                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x299c:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x29a1:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x29a7:0x12 DW_TAG_subprogram
	.long	.Linfo_string440        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	313                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x29b3:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x29b9:0x12 DW_TAG_subprogram
	.long	.Linfo_string441        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	313                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x29c5:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x29cb:0x12 DW_TAG_subprogram
	.long	.Linfo_string442        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	313                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x29d7:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x29dd:0x12 DW_TAG_subprogram
	.long	.Linfo_string443        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.short	308                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x29e9:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x29ef:0x12 DW_TAG_subprogram
	.long	.Linfo_string444        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.short	319                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x29fb:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2a01:0x12 DW_TAG_subprogram
	.long	.Linfo_string445        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.short	329                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2a0d:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2a13:0x12 DW_TAG_subprogram
	.long	.Linfo_string446        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	344                     @ DW_AT_decl_line
	.long	4801                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2a1f:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2a25:0x12 DW_TAG_subprogram
	.long	.Linfo_string447        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	344                     @ DW_AT_decl_line
	.long	4801                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2a31:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2a37:0x12 DW_TAG_subprogram
	.long	.Linfo_string448        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	344                     @ DW_AT_decl_line
	.long	4801                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2a43:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2a49:0x12 DW_TAG_subprogram
	.long	.Linfo_string449        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	350                     @ DW_AT_decl_line
	.long	4801                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2a55:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2a5b:0x12 DW_TAG_subprogram
	.long	.Linfo_string450        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	350                     @ DW_AT_decl_line
	.long	4801                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2a67:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2a6d:0x12 DW_TAG_subprogram
	.long	.Linfo_string451        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	350                     @ DW_AT_decl_line
	.long	4801                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2a79:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2a7f:0x11 DW_TAG_subprogram
	.long	.Linfo_string452        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	131                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2a8a:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2a90:0x11 DW_TAG_subprogram
	.long	.Linfo_string453        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	131                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2a9b:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2aa1:0x11 DW_TAG_subprogram
	.long	.Linfo_string454        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	131                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2aac:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	59                      @ Abbrev [59] 0x2ab2:0x16 DW_TAG_subprogram
	.long	.Linfo_string455        @ DW_AT_linkage_name
	.long	.Linfo_string456        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.short	390                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2ac2:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	59                      @ Abbrev [59] 0x2ac8:0x16 DW_TAG_subprogram
	.long	.Linfo_string457        @ DW_AT_linkage_name
	.long	.Linfo_string458        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.short	391                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2ad8:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	59                      @ Abbrev [59] 0x2ade:0x16 DW_TAG_subprogram
	.long	.Linfo_string455        @ DW_AT_linkage_name
	.long	.Linfo_string459        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.short	394                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2aee:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2af4:0x11 DW_TAG_subprogram
	.long	.Linfo_string460        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	134                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2aff:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2b05:0x11 DW_TAG_subprogram
	.long	.Linfo_string461        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	134                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2b10:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2b16:0x11 DW_TAG_subprogram
	.long	.Linfo_string462        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	134                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2b21:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2b27:0x12 DW_TAG_subprogram
	.long	.Linfo_string463        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	342                     @ DW_AT_decl_line
	.long	4734                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2b33:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2b39:0x12 DW_TAG_subprogram
	.long	.Linfo_string464        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	342                     @ DW_AT_decl_line
	.long	4734                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2b45:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2b4b:0x12 DW_TAG_subprogram
	.long	.Linfo_string465        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	342                     @ DW_AT_decl_line
	.long	4734                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2b57:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2b5d:0x12 DW_TAG_subprogram
	.long	.Linfo_string466        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	348                     @ DW_AT_decl_line
	.long	4734                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2b69:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2b6f:0x12 DW_TAG_subprogram
	.long	.Linfo_string467        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	348                     @ DW_AT_decl_line
	.long	4734                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2b7b:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2b81:0x12 DW_TAG_subprogram
	.long	.Linfo_string468        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	348                     @ DW_AT_decl_line
	.long	4734                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2b8d:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2b93:0x11 DW_TAG_subprogram
	.long	.Linfo_string469        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	228                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2b9e:0x5 DW_TAG_formal_parameter
	.long	5077                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2ba4:0x11 DW_TAG_subprogram
	.long	.Linfo_string470        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	228                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2baf:0x5 DW_TAG_formal_parameter
	.long	5077                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	46                      @ Abbrev [46] 0x2bb5:0x11 DW_TAG_subprogram
	.long	.Linfo_string471        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.byte	228                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2bc0:0x5 DW_TAG_formal_parameter
	.long	5077                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2bc6:0x12 DW_TAG_subprogram
	.long	.Linfo_string472        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	322                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2bd2:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2bd8:0x12 DW_TAG_subprogram
	.long	.Linfo_string473        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	322                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2be4:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2bea:0x12 DW_TAG_subprogram
	.long	.Linfo_string474        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	322                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2bf6:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2bfc:0x17 DW_TAG_subprogram
	.long	.Linfo_string475        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	292                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2c08:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2c0d:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2c13:0x17 DW_TAG_subprogram
	.long	.Linfo_string476        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	292                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2c1f:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2c24:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2c2a:0x17 DW_TAG_subprogram
	.long	.Linfo_string477        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	292                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2c36:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2c3b:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2c41:0x17 DW_TAG_subprogram
	.long	.Linfo_string478        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	294                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2c4d:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2c52:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2c58:0x17 DW_TAG_subprogram
	.long	.Linfo_string479        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	294                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2c64:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2c69:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2c6f:0x17 DW_TAG_subprogram
	.long	.Linfo_string480        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	294                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2c7b:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2c80:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	59                      @ Abbrev [59] 0x2c86:0x1b DW_TAG_subprogram
	.long	.Linfo_string481        @ DW_AT_linkage_name
	.long	.Linfo_string482        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.short	418                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2c96:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2c9b:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	59                      @ Abbrev [59] 0x2ca1:0x1b DW_TAG_subprogram
	.long	.Linfo_string483        @ DW_AT_linkage_name
	.long	.Linfo_string484        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.short	421                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2cb1:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2cb6:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	59                      @ Abbrev [59] 0x2cbc:0x1b DW_TAG_subprogram
	.long	.Linfo_string481        @ DW_AT_linkage_name
	.long	.Linfo_string485        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.short	424                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2ccc:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2cd1:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2cd7:0x1c DW_TAG_subprogram
	.long	.Linfo_string486        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	335                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2ce3:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2ce8:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2ced:0x5 DW_TAG_formal_parameter
	.long	9536                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2cf3:0x1c DW_TAG_subprogram
	.long	.Linfo_string487        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	335                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2cff:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2d04:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2d09:0x5 DW_TAG_formal_parameter
	.long	9536                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2d0f:0x1c DW_TAG_subprogram
	.long	.Linfo_string488        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	335                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2d1b:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2d20:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2d25:0x5 DW_TAG_formal_parameter
	.long	9536                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2d2b:0x12 DW_TAG_subprogram
	.long	.Linfo_string489        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	289                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2d37:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2d3d:0x12 DW_TAG_subprogram
	.long	.Linfo_string490        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	289                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2d49:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2d4f:0x12 DW_TAG_subprogram
	.long	.Linfo_string491        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	289                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2d5b:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2d61:0x12 DW_TAG_subprogram
	.long	.Linfo_string492        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	326                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2d6d:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2d73:0x12 DW_TAG_subprogram
	.long	.Linfo_string493        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	326                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2d7f:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2d85:0x12 DW_TAG_subprogram
	.long	.Linfo_string494        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	326                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2d91:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2d97:0x17 DW_TAG_subprogram
	.long	.Linfo_string495        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	318                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2da3:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2da8:0x5 DW_TAG_formal_parameter
	.long	4734                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2dae:0x17 DW_TAG_subprogram
	.long	.Linfo_string496        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	318                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2dba:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2dbf:0x5 DW_TAG_formal_parameter
	.long	4734                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2dc5:0x17 DW_TAG_subprogram
	.long	.Linfo_string497        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	318                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2dd1:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2dd6:0x5 DW_TAG_formal_parameter
	.long	4734                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2ddc:0x17 DW_TAG_subprogram
	.long	.Linfo_string498        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	309                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2de8:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2ded:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2df3:0x17 DW_TAG_subprogram
	.long	.Linfo_string499        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	309                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2dff:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2e04:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2e0a:0x17 DW_TAG_subprogram
	.long	.Linfo_string500        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	309                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2e16:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	20                      @ Abbrev [20] 0x2e1b:0x5 DW_TAG_formal_parameter
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2e21:0x12 DW_TAG_subprogram
	.long	.Linfo_string501        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.short	480                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2e2d:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2e33:0x12 DW_TAG_subprogram
	.long	.Linfo_string502        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.short	487                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2e3f:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2e45:0x12 DW_TAG_subprogram
	.long	.Linfo_string503        @ DW_AT_name
	.byte	21                      @ DW_AT_decl_file
	.short	495                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2e51:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2e57:0x12 DW_TAG_subprogram
	.long	.Linfo_string504        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	330                     @ DW_AT_decl_line
	.long	5908                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2e63:0x5 DW_TAG_formal_parameter
	.long	5908                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2e69:0x12 DW_TAG_subprogram
	.long	.Linfo_string505        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	330                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2e75:0x5 DW_TAG_formal_parameter
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	31                      @ Abbrev [31] 0x2e7b:0x12 DW_TAG_subprogram
	.long	.Linfo_string506        @ DW_AT_name
	.byte	22                      @ DW_AT_decl_file
	.short	330                     @ DW_AT_decl_line
	.long	6544                    @ DW_AT_type
                                        @ DW_AT_declaration
                                        @ DW_AT_external
	.byte	20                      @ Abbrev [20] 0x2e87:0x5 DW_TAG_formal_parameter
	.long	6544                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	60                      @ Abbrev [60] 0x2e8d:0x4a DW_TAG_subprogram
	.long	.Linfo_string507        @ DW_AT_linkage_name
	.long	.Linfo_string508        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1467                    @ DW_AT_decl_line
	.byte	1                       @ DW_AT_inline
	.byte	61                      @ Abbrev [61] 0x2e9a:0xc DW_TAG_formal_parameter
	.long	.Linfo_string509        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1467                    @ DW_AT_decl_line
	.long	11991                   @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x2ea6:0xc DW_TAG_formal_parameter
	.long	.Linfo_string535        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1467                    @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x2eb2:0xc DW_TAG_formal_parameter
	.long	.Linfo_string536        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1467                    @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x2ebe:0xc DW_TAG_formal_parameter
	.long	.Linfo_string537        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1467                    @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	62                      @ Abbrev [62] 0x2eca:0xc DW_TAG_variable
	.long	.Linfo_string538        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1468                    @ DW_AT_decl_line
	.long	4236                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	32                      @ Abbrev [32] 0x2ed7:0x5 DW_TAG_pointer_type
	.long	11996                   @ DW_AT_type
	.byte	56                      @ Abbrev [56] 0x2edc:0xc DW_TAG_typedef
	.long	12008                   @ DW_AT_type
	.long	.Linfo_string534        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	410                     @ DW_AT_decl_line
	.byte	63                      @ Abbrev [63] 0x2ee8:0x13d DW_TAG_structure_type
	.short	352                     @ DW_AT_byte_size
	.byte	2                       @ DW_AT_decl_file
	.byte	220                     @ DW_AT_decl_line
	.byte	17                      @ Abbrev [17] 0x2eed:0xc DW_TAG_member
	.long	.Linfo_string510        @ DW_AT_name
	.long	12325                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.byte	238                     @ DW_AT_decl_line
	.byte	0                       @ DW_AT_data_member_location
	.byte	17                      @ Abbrev [17] 0x2ef9:0xc DW_TAG_member
	.long	.Linfo_string511        @ DW_AT_name
	.long	12340                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.byte	253                     @ DW_AT_decl_line
	.byte	4                       @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2f05:0xd DW_TAG_member
	.long	.Linfo_string512        @ DW_AT_name
	.long	12325                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	272                     @ DW_AT_decl_line
	.byte	8                       @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2f12:0xd DW_TAG_member
	.long	.Linfo_string513        @ DW_AT_name
	.long	12340                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	287                     @ DW_AT_decl_line
	.byte	12                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2f1f:0xd DW_TAG_member
	.long	.Linfo_string514        @ DW_AT_name
	.long	12350                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	297                     @ DW_AT_decl_line
	.byte	16                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2f2c:0xd DW_TAG_member
	.long	.Linfo_string515        @ DW_AT_name
	.long	12360                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	311                     @ DW_AT_decl_line
	.byte	20                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2f39:0xd DW_TAG_member
	.long	.Linfo_string516        @ DW_AT_name
	.long	12360                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	313                     @ DW_AT_decl_line
	.byte	24                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2f46:0xd DW_TAG_member
	.long	.Linfo_string517        @ DW_AT_name
	.long	12360                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	315                     @ DW_AT_decl_line
	.byte	28                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2f53:0xd DW_TAG_member
	.long	.Linfo_string518        @ DW_AT_name
	.long	12335                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	317                     @ DW_AT_decl_line
	.byte	32                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2f60:0xd DW_TAG_member
	.long	.Linfo_string519        @ DW_AT_name
	.long	12360                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	330                     @ DW_AT_decl_line
	.byte	36                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2f6d:0xd DW_TAG_member
	.long	.Linfo_string520        @ DW_AT_name
	.long	12360                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	335                     @ DW_AT_decl_line
	.byte	40                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2f7a:0xd DW_TAG_member
	.long	.Linfo_string521        @ DW_AT_name
	.long	12360                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	340                     @ DW_AT_decl_line
	.byte	44                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2f87:0xd DW_TAG_member
	.long	.Linfo_string522        @ DW_AT_name
	.long	12335                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	351                     @ DW_AT_decl_line
	.byte	48                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2f94:0xd DW_TAG_member
	.long	.Linfo_string523        @ DW_AT_name
	.long	12360                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	354                     @ DW_AT_decl_line
	.byte	52                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2fa1:0xd DW_TAG_member
	.long	.Linfo_string524        @ DW_AT_name
	.long	12360                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	358                     @ DW_AT_decl_line
	.byte	56                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2fae:0xd DW_TAG_member
	.long	.Linfo_string525        @ DW_AT_name
	.long	12335                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	360                     @ DW_AT_decl_line
	.byte	60                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2fbb:0xd DW_TAG_member
	.long	.Linfo_string526        @ DW_AT_name
	.long	12365                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	368                     @ DW_AT_decl_line
	.byte	64                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2fc8:0xd DW_TAG_member
	.long	.Linfo_string527        @ DW_AT_name
	.long	12360                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	374                     @ DW_AT_decl_line
	.byte	72                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2fd5:0xd DW_TAG_member
	.long	.Linfo_string528        @ DW_AT_name
	.long	12360                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	380                     @ DW_AT_decl_line
	.byte	76                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2fe2:0xd DW_TAG_member
	.long	.Linfo_string529        @ DW_AT_name
	.long	12325                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	387                     @ DW_AT_decl_line
	.byte	80                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2fef:0xd DW_TAG_member
	.long	.Linfo_string530        @ DW_AT_name
	.long	12360                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	393                     @ DW_AT_decl_line
	.byte	84                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x2ffc:0xd DW_TAG_member
	.long	.Linfo_string531        @ DW_AT_name
	.long	12360                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	403                     @ DW_AT_decl_line
	.byte	88                      @ DW_AT_data_member_location
	.byte	34                      @ Abbrev [34] 0x3009:0xd DW_TAG_member
	.long	.Linfo_string532        @ DW_AT_name
	.long	12370                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	406                     @ DW_AT_decl_line
	.byte	92                      @ DW_AT_data_member_location
	.byte	64                      @ Abbrev [64] 0x3016:0xe DW_TAG_member
	.long	.Linfo_string533        @ DW_AT_name
	.long	12383                   @ DW_AT_type
	.byte	2                       @ DW_AT_decl_file
	.short	409                     @ DW_AT_decl_line
	.short	348                     @ DW_AT_data_member_location
	.byte	0                       @ End Of Children Mark
	.byte	39                      @ Abbrev [39] 0x3025:0x5 DW_TAG_const_type
	.long	12330                   @ DW_AT_type
	.byte	32                      @ Abbrev [32] 0x302a:0x5 DW_TAG_pointer_type
	.long	12335                   @ DW_AT_type
	.byte	39                      @ Abbrev [39] 0x302f:0x5 DW_TAG_const_type
	.long	262                     @ DW_AT_type
	.byte	39                      @ Abbrev [39] 0x3034:0x5 DW_TAG_const_type
	.long	12345                   @ DW_AT_type
	.byte	32                      @ Abbrev [32] 0x3039:0x5 DW_TAG_pointer_type
	.long	262                     @ DW_AT_type
	.byte	39                      @ Abbrev [39] 0x303e:0x5 DW_TAG_const_type
	.long	12355                   @ DW_AT_type
	.byte	32                      @ Abbrev [32] 0x3043:0x5 DW_TAG_pointer_type
	.long	6804                    @ DW_AT_type
	.byte	39                      @ Abbrev [39] 0x3048:0x5 DW_TAG_const_type
	.long	6804                    @ DW_AT_type
	.byte	39                      @ Abbrev [39] 0x304d:0x5 DW_TAG_const_type
	.long	6815                    @ DW_AT_type
	.byte	4                       @ Abbrev [4] 0x3052:0xd DW_TAG_array_type
	.long	4255                    @ DW_AT_type
	.byte	8                       @ Abbrev [8] 0x3057:0x7 DW_TAG_subrange_type
	.long	97                      @ DW_AT_type
	.short	256                     @ DW_AT_count
	.byte	0                       @ End Of Children Mark
	.byte	39                      @ Abbrev [39] 0x305f:0x5 DW_TAG_const_type
	.long	4236                    @ DW_AT_type
	.byte	65                      @ Abbrev [65] 0x3064:0xb1 DW_TAG_subprogram
	.long	.Lfunc_begin0           @ DW_AT_low_pc
	.long	.Lfunc_end0-.Lfunc_begin0 @ DW_AT_high_pc
	.byte	1                       @ DW_AT_frame_base
	.byte	91
	.long	.Linfo_string559        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	98                      @ DW_AT_decl_line
	.long	6971                    @ DW_AT_type
                                        @ DW_AT_external
	.byte	66                      @ Abbrev [66] 0x3079:0xf DW_TAG_formal_parameter
	.long	.Ldebug_loc0            @ DW_AT_location
	.long	.Linfo_string509        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	98                      @ DW_AT_decl_line
	.long	11991                   @ DW_AT_type
	.byte	66                      @ Abbrev [66] 0x3088:0xf DW_TAG_formal_parameter
	.long	.Ldebug_loc1            @ DW_AT_location
	.long	.Linfo_string562        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	98                      @ DW_AT_decl_line
	.long	4808                    @ DW_AT_type
	.byte	67                      @ Abbrev [67] 0x3097:0x16 DW_TAG_lexical_block
	.long	.Ltmp17                 @ DW_AT_low_pc
	.long	.Ltmp27-.Ltmp17         @ DW_AT_high_pc
	.byte	68                      @ Abbrev [68] 0x30a0:0xc DW_TAG_variable
	.byte	0                       @ DW_AT_const_value
	.long	.Linfo_string563        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	102                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	69                      @ Abbrev [69] 0x30ad:0x21 DW_TAG_inlined_subroutine
	.long	11917                   @ DW_AT_abstract_origin
	.long	.Ldebug_ranges0         @ DW_AT_ranges
	.byte	1                       @ DW_AT_call_file
	.byte	107                     @ DW_AT_call_line
	.byte	70                      @ Abbrev [70] 0x30b8:0x6 DW_TAG_formal_parameter
	.byte	0                       @ DW_AT_const_value
	.long	11942                   @ DW_AT_abstract_origin
	.byte	70                      @ Abbrev [70] 0x30be:0x6 DW_TAG_formal_parameter
	.byte	0                       @ DW_AT_const_value
	.long	11966                   @ DW_AT_abstract_origin
	.byte	71                      @ Abbrev [71] 0x30c4:0x9 DW_TAG_variable
	.long	.Ldebug_loc2            @ DW_AT_location
	.long	11978                   @ DW_AT_abstract_origin
	.byte	0                       @ End Of Children Mark
	.byte	69                      @ Abbrev [69] 0x30ce:0x21 DW_TAG_inlined_subroutine
	.long	11917                   @ DW_AT_abstract_origin
	.long	.Ldebug_ranges1         @ DW_AT_ranges
	.byte	1                       @ DW_AT_call_file
	.byte	108                     @ DW_AT_call_line
	.byte	70                      @ Abbrev [70] 0x30d9:0x6 DW_TAG_formal_parameter
	.byte	0                       @ DW_AT_const_value
	.long	11942                   @ DW_AT_abstract_origin
	.byte	70                      @ Abbrev [70] 0x30df:0x6 DW_TAG_formal_parameter
	.byte	0                       @ DW_AT_const_value
	.long	11966                   @ DW_AT_abstract_origin
	.byte	71                      @ Abbrev [71] 0x30e5:0x9 DW_TAG_variable
	.long	.Ldebug_loc3            @ DW_AT_location
	.long	11978                   @ DW_AT_abstract_origin
	.byte	0                       @ End Of Children Mark
	.byte	72                      @ Abbrev [72] 0x30ef:0x25 DW_TAG_inlined_subroutine
	.long	11917                   @ DW_AT_abstract_origin
	.long	.Ltmp37                 @ DW_AT_low_pc
	.long	.Ltmp42-.Ltmp37         @ DW_AT_high_pc
	.byte	1                       @ DW_AT_call_file
	.byte	109                     @ DW_AT_call_line
	.byte	70                      @ Abbrev [70] 0x30fe:0x6 DW_TAG_formal_parameter
	.byte	0                       @ DW_AT_const_value
	.long	11942                   @ DW_AT_abstract_origin
	.byte	70                      @ Abbrev [70] 0x3104:0x6 DW_TAG_formal_parameter
	.byte	0                       @ DW_AT_const_value
	.long	11966                   @ DW_AT_abstract_origin
	.byte	71                      @ Abbrev [71] 0x310a:0x9 DW_TAG_variable
	.long	.Ldebug_loc4            @ DW_AT_location
	.long	11978                   @ DW_AT_abstract_origin
	.byte	0                       @ End Of Children Mark
	.byte	0                       @ End Of Children Mark
	.byte	73                      @ Abbrev [73] 0x3115:0x48 DW_TAG_subprogram
	.long	.Linfo_string539        @ DW_AT_linkage_name
	.long	.Linfo_string540        @ DW_AT_name
	.byte	24                      @ DW_AT_decl_file
	.byte	71                      @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	1                       @ DW_AT_inline
	.byte	74                      @ Abbrev [74] 0x3125:0xb DW_TAG_formal_parameter
	.long	.Linfo_string541        @ DW_AT_name
	.byte	24                      @ DW_AT_decl_file
	.byte	71                      @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	74                      @ Abbrev [74] 0x3130:0xb DW_TAG_formal_parameter
	.long	.Linfo_string542        @ DW_AT_name
	.byte	24                      @ DW_AT_decl_file
	.byte	71                      @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	74                      @ Abbrev [74] 0x313b:0xb DW_TAG_formal_parameter
	.long	.Linfo_string543        @ DW_AT_name
	.byte	24                      @ DW_AT_decl_file
	.byte	71                      @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	74                      @ Abbrev [74] 0x3146:0xb DW_TAG_formal_parameter
	.long	.Linfo_string544        @ DW_AT_name
	.byte	24                      @ DW_AT_decl_file
	.byte	71                      @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	74                      @ Abbrev [74] 0x3151:0xb DW_TAG_formal_parameter
	.long	.Linfo_string545        @ DW_AT_name
	.byte	24                      @ DW_AT_decl_file
	.byte	71                      @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	75                      @ Abbrev [75] 0x315d:0x36 DW_TAG_subprogram
	.long	.Linfo_string546        @ DW_AT_linkage_name
	.long	.Linfo_string547        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1437                    @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	1                       @ DW_AT_inline
	.byte	61                      @ Abbrev [61] 0x316e:0xc DW_TAG_formal_parameter
	.long	.Linfo_string509        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1437                    @ DW_AT_decl_line
	.long	11991                   @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x317a:0xc DW_TAG_formal_parameter
	.long	.Linfo_string535        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1437                    @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x3186:0xc DW_TAG_formal_parameter
	.long	.Linfo_string536        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1437                    @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	60                      @ Abbrev [60] 0x3193:0x4a DW_TAG_subprogram
	.long	.Linfo_string548        @ DW_AT_linkage_name
	.long	.Linfo_string549        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1422                    @ DW_AT_decl_line
	.byte	1                       @ DW_AT_inline
	.byte	61                      @ Abbrev [61] 0x31a0:0xc DW_TAG_formal_parameter
	.long	.Linfo_string509        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1422                    @ DW_AT_decl_line
	.long	11991                   @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x31ac:0xc DW_TAG_formal_parameter
	.long	.Linfo_string535        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1422                    @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x31b8:0xc DW_TAG_formal_parameter
	.long	.Linfo_string536        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1422                    @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x31c4:0xc DW_TAG_formal_parameter
	.long	.Linfo_string550        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1422                    @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	62                      @ Abbrev [62] 0x31d0:0xc DW_TAG_variable
	.long	.Linfo_string538        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1423                    @ DW_AT_decl_line
	.long	4236                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	60                      @ Abbrev [60] 0x31dd:0x3e DW_TAG_subprogram
	.long	.Linfo_string551        @ DW_AT_linkage_name
	.long	.Linfo_string552        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1410                    @ DW_AT_decl_line
	.byte	1                       @ DW_AT_inline
	.byte	61                      @ Abbrev [61] 0x31ea:0xc DW_TAG_formal_parameter
	.long	.Linfo_string509        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1410                    @ DW_AT_decl_line
	.long	11991                   @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x31f6:0xc DW_TAG_formal_parameter
	.long	.Linfo_string535        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1410                    @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x3202:0xc DW_TAG_formal_parameter
	.long	.Linfo_string536        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1410                    @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x320e:0xc DW_TAG_formal_parameter
	.long	.Linfo_string550        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1410                    @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	75                      @ Abbrev [75] 0x321b:0x36 DW_TAG_subprogram
	.long	.Linfo_string553        @ DW_AT_linkage_name
	.long	.Linfo_string554        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1399                    @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	1                       @ DW_AT_inline
	.byte	61                      @ Abbrev [61] 0x322c:0xc DW_TAG_formal_parameter
	.long	.Linfo_string509        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1399                    @ DW_AT_decl_line
	.long	11991                   @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x3238:0xc DW_TAG_formal_parameter
	.long	.Linfo_string535        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1399                    @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x3244:0xc DW_TAG_formal_parameter
	.long	.Linfo_string536        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1399                    @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	75                      @ Abbrev [75] 0x3251:0x36 DW_TAG_subprogram
	.long	.Linfo_string555        @ DW_AT_linkage_name
	.long	.Linfo_string556        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1377                    @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	1                       @ DW_AT_inline
	.byte	61                      @ Abbrev [61] 0x3262:0xc DW_TAG_formal_parameter
	.long	.Linfo_string509        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1377                    @ DW_AT_decl_line
	.long	11991                   @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x326e:0xc DW_TAG_formal_parameter
	.long	.Linfo_string535        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1377                    @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x327a:0xc DW_TAG_formal_parameter
	.long	.Linfo_string536        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1377                    @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	60                      @ Abbrev [60] 0x3287:0x3e DW_TAG_subprogram
	.long	.Linfo_string557        @ DW_AT_linkage_name
	.long	.Linfo_string558        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1388                    @ DW_AT_decl_line
	.byte	1                       @ DW_AT_inline
	.byte	61                      @ Abbrev [61] 0x3294:0xc DW_TAG_formal_parameter
	.long	.Linfo_string509        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1388                    @ DW_AT_decl_line
	.long	11991                   @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x32a0:0xc DW_TAG_formal_parameter
	.long	.Linfo_string535        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1388                    @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x32ac:0xc DW_TAG_formal_parameter
	.long	.Linfo_string536        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1388                    @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	61                      @ Abbrev [61] 0x32b8:0xc DW_TAG_formal_parameter
	.long	.Linfo_string550        @ DW_AT_name
	.byte	2                       @ DW_AT_decl_file
	.short	1388                    @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	76                      @ Abbrev [76] 0x32c5:0x418 DW_TAG_subprogram
	.long	.Lfunc_begin1           @ DW_AT_low_pc
	.long	.Lfunc_end1-.Lfunc_begin1 @ DW_AT_high_pc
	.byte	1                       @ DW_AT_frame_base
	.byte	91
	.long	.Linfo_string560        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	117                     @ DW_AT_decl_line
                                        @ DW_AT_external
	.byte	66                      @ Abbrev [66] 0x32d6:0xf DW_TAG_formal_parameter
	.long	.Ldebug_loc5            @ DW_AT_location
	.long	.Linfo_string509        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	117                     @ DW_AT_decl_line
	.long	11991                   @ DW_AT_type
	.byte	66                      @ Abbrev [66] 0x32e5:0xf DW_TAG_formal_parameter
	.long	.Ldebug_loc6            @ DW_AT_location
	.long	.Linfo_string562        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	117                     @ DW_AT_decl_line
	.long	4808                    @ DW_AT_type
	.byte	67                      @ Abbrev [67] 0x32f4:0x3e8 DW_TAG_lexical_block
	.long	.Ltmp63                 @ DW_AT_low_pc
	.long	.Ltmp347-.Ltmp63        @ DW_AT_high_pc
	.byte	77                      @ Abbrev [77] 0x32fd:0xf DW_TAG_variable
	.long	.Ldebug_loc7            @ DW_AT_location
	.long	.Linfo_string564        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	120                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	78                      @ Abbrev [78] 0x330c:0x1d DW_TAG_lexical_block
	.long	.Ldebug_ranges3         @ DW_AT_ranges
	.byte	68                      @ Abbrev [68] 0x3311:0xc DW_TAG_variable
	.byte	1                       @ DW_AT_const_value
	.long	.Linfo_string565        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	140                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	79                      @ Abbrev [79] 0x331d:0xb DW_TAG_inlined_subroutine
	.long	12827                   @ DW_AT_abstract_origin
	.long	.Ldebug_ranges2         @ DW_AT_ranges
	.byte	1                       @ DW_AT_call_file
	.byte	142                     @ DW_AT_call_line
	.byte	0                       @ End Of Children Mark
	.byte	69                      @ Abbrev [69] 0x3329:0x30 DW_TAG_inlined_subroutine
	.long	12691                   @ DW_AT_abstract_origin
	.long	.Ldebug_ranges4         @ DW_AT_ranges
	.byte	1                       @ DW_AT_call_file
	.byte	148                     @ DW_AT_call_line
	.byte	80                      @ Abbrev [80] 0x3334:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc12           @ DW_AT_location
	.long	12704                   @ DW_AT_abstract_origin
	.byte	70                      @ Abbrev [70] 0x333d:0x6 DW_TAG_formal_parameter
	.byte	0                       @ DW_AT_const_value
	.long	12728                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x3343:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc11           @ DW_AT_location
	.long	12740                   @ DW_AT_abstract_origin
	.byte	81                      @ Abbrev [81] 0x334c:0xc DW_TAG_inlined_subroutine
	.long	12765                   @ DW_AT_abstract_origin
	.long	.Ldebug_ranges5         @ DW_AT_ranges
	.byte	2                       @ DW_AT_call_file
	.short	1425                    @ DW_AT_call_line
	.byte	0                       @ End Of Children Mark
	.byte	82                      @ Abbrev [82] 0x3359:0xf DW_TAG_inlined_subroutine
	.long	12637                   @ DW_AT_abstract_origin
	.long	.Ltmp107                @ DW_AT_low_pc
	.long	.Ltmp108-.Ltmp107       @ DW_AT_high_pc
	.byte	1                       @ DW_AT_call_file
	.byte	138                     @ DW_AT_call_line
	.byte	78                      @ Abbrev [78] 0x3368:0xcd DW_TAG_lexical_block
	.long	.Ldebug_ranges10        @ DW_AT_ranges
	.byte	77                      @ Abbrev [77] 0x336d:0xf DW_TAG_variable
	.long	.Ldebug_loc13           @ DW_AT_location
	.long	.Linfo_string566        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	155                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	77                      @ Abbrev [77] 0x337c:0xf DW_TAG_variable
	.long	.Ldebug_loc32           @ DW_AT_location
	.long	.Linfo_string567        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	164                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	77                      @ Abbrev [77] 0x338b:0xf DW_TAG_variable
	.long	.Ldebug_loc38           @ DW_AT_location
	.long	.Linfo_string568        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	172                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	69                      @ Abbrev [69] 0x339a:0x39 DW_TAG_inlined_subroutine
	.long	12565                   @ DW_AT_abstract_origin
	.long	.Ldebug_ranges6         @ DW_AT_ranges
	.byte	1                       @ DW_AT_call_file
	.byte	177                     @ DW_AT_call_line
	.byte	80                      @ Abbrev [80] 0x33a5:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc39           @ DW_AT_location
	.long	12581                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x33ae:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc40           @ DW_AT_location
	.long	12592                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x33b7:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc41           @ DW_AT_location
	.long	12603                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x33c0:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc42           @ DW_AT_location
	.long	12614                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x33c9:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc43           @ DW_AT_location
	.long	12625                   @ DW_AT_abstract_origin
	.byte	0                       @ End Of Children Mark
	.byte	83                      @ Abbrev [83] 0x33d3:0xc DW_TAG_inlined_subroutine
	.long	12827                   @ DW_AT_abstract_origin
	.long	.Ldebug_ranges7         @ DW_AT_ranges
	.byte	1                       @ DW_AT_call_file
	.byte	155                     @ DW_AT_call_line
	.byte	1                       @ DW_AT_GNU_discriminator
	.byte	83                      @ Abbrev [83] 0x33df:0xc DW_TAG_inlined_subroutine
	.long	12827                   @ DW_AT_abstract_origin
	.long	.Ldebug_ranges8         @ DW_AT_ranges
	.byte	1                       @ DW_AT_call_file
	.byte	164                     @ DW_AT_call_line
	.byte	1                       @ DW_AT_GNU_discriminator
	.byte	72                      @ Abbrev [72] 0x33eb:0x3d DW_TAG_inlined_subroutine
	.long	12565                   @ DW_AT_abstract_origin
	.long	.Ltmp223                @ DW_AT_low_pc
	.long	.Ltmp224-.Ltmp223       @ DW_AT_high_pc
	.byte	1                       @ DW_AT_call_file
	.byte	169                     @ DW_AT_call_line
	.byte	80                      @ Abbrev [80] 0x33fa:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc33           @ DW_AT_location
	.long	12581                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x3403:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc34           @ DW_AT_location
	.long	12592                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x340c:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc35           @ DW_AT_location
	.long	12603                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x3415:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc36           @ DW_AT_location
	.long	12614                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x341e:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc37           @ DW_AT_location
	.long	12625                   @ DW_AT_abstract_origin
	.byte	0                       @ End Of Children Mark
	.byte	83                      @ Abbrev [83] 0x3428:0xc DW_TAG_inlined_subroutine
	.long	12827                   @ DW_AT_abstract_origin
	.long	.Ldebug_ranges9         @ DW_AT_ranges
	.byte	1                       @ DW_AT_call_file
	.byte	172                     @ DW_AT_call_line
	.byte	1                       @ DW_AT_GNU_discriminator
	.byte	0                       @ End Of Children Mark
	.byte	78                      @ Abbrev [78] 0x3435:0x102 DW_TAG_lexical_block
	.long	.Ldebug_ranges13        @ DW_AT_ranges
	.byte	77                      @ Abbrev [77] 0x343a:0xf DW_TAG_variable
	.long	.Ldebug_loc14           @ DW_AT_location
	.long	.Linfo_string566        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	180                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	77                      @ Abbrev [77] 0x3449:0xf DW_TAG_variable
	.long	.Ldebug_loc20           @ DW_AT_location
	.long	.Linfo_string567        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	188                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	77                      @ Abbrev [77] 0x3458:0xf DW_TAG_variable
	.long	.Ldebug_loc26           @ DW_AT_location
	.long	.Linfo_string568        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	196                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	72                      @ Abbrev [72] 0x3467:0x3d DW_TAG_inlined_subroutine
	.long	12565                   @ DW_AT_abstract_origin
	.long	.Ltmp176                @ DW_AT_low_pc
	.long	.Ltmp177-.Ltmp176       @ DW_AT_high_pc
	.byte	1                       @ DW_AT_call_file
	.byte	185                     @ DW_AT_call_line
	.byte	80                      @ Abbrev [80] 0x3476:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc15           @ DW_AT_location
	.long	12581                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x347f:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc16           @ DW_AT_location
	.long	12592                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x3488:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc17           @ DW_AT_location
	.long	12603                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x3491:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc18           @ DW_AT_location
	.long	12614                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x349a:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc19           @ DW_AT_location
	.long	12625                   @ DW_AT_abstract_origin
	.byte	0                       @ End Of Children Mark
	.byte	83                      @ Abbrev [83] 0x34a4:0xc DW_TAG_inlined_subroutine
	.long	12827                   @ DW_AT_abstract_origin
	.long	.Ldebug_ranges11        @ DW_AT_ranges
	.byte	1                       @ DW_AT_call_file
	.byte	188                     @ DW_AT_call_line
	.byte	1                       @ DW_AT_GNU_discriminator
	.byte	72                      @ Abbrev [72] 0x34b0:0x3d DW_TAG_inlined_subroutine
	.long	12565                   @ DW_AT_abstract_origin
	.long	.Ltmp190                @ DW_AT_low_pc
	.long	.Ltmp191-.Ltmp190       @ DW_AT_high_pc
	.byte	1                       @ DW_AT_call_file
	.byte	193                     @ DW_AT_call_line
	.byte	80                      @ Abbrev [80] 0x34bf:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc21           @ DW_AT_location
	.long	12581                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x34c8:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc22           @ DW_AT_location
	.long	12592                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x34d1:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc23           @ DW_AT_location
	.long	12603                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x34da:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc24           @ DW_AT_location
	.long	12614                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x34e3:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc25           @ DW_AT_location
	.long	12625                   @ DW_AT_abstract_origin
	.byte	0                       @ End Of Children Mark
	.byte	83                      @ Abbrev [83] 0x34ed:0xc DW_TAG_inlined_subroutine
	.long	12827                   @ DW_AT_abstract_origin
	.long	.Ldebug_ranges12        @ DW_AT_ranges
	.byte	1                       @ DW_AT_call_file
	.byte	196                     @ DW_AT_call_line
	.byte	1                       @ DW_AT_GNU_discriminator
	.byte	72                      @ Abbrev [72] 0x34f9:0x3d DW_TAG_inlined_subroutine
	.long	12565                   @ DW_AT_abstract_origin
	.long	.Ltmp204                @ DW_AT_low_pc
	.long	.Ltmp205-.Ltmp204       @ DW_AT_high_pc
	.byte	1                       @ DW_AT_call_file
	.byte	201                     @ DW_AT_call_line
	.byte	80                      @ Abbrev [80] 0x3508:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc27           @ DW_AT_location
	.long	12581                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x3511:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc28           @ DW_AT_location
	.long	12592                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x351a:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc29           @ DW_AT_location
	.long	12603                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x3523:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc30           @ DW_AT_location
	.long	12614                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x352c:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc31           @ DW_AT_location
	.long	12625                   @ DW_AT_abstract_origin
	.byte	0                       @ End Of Children Mark
	.byte	0                       @ End Of Children Mark
	.byte	82                      @ Abbrev [82] 0x3537:0xf DW_TAG_inlined_subroutine
	.long	12637                   @ DW_AT_abstract_origin
	.long	.Ltmp83                 @ DW_AT_low_pc
	.long	.Ltmp84-.Ltmp83         @ DW_AT_high_pc
	.byte	1                       @ DW_AT_call_file
	.byte	128                     @ DW_AT_call_line
	.byte	69                      @ Abbrev [69] 0x3546:0x39 DW_TAG_inlined_subroutine
	.long	12691                   @ DW_AT_abstract_origin
	.long	.Ldebug_ranges14        @ DW_AT_ranges
	.byte	1                       @ DW_AT_call_file
	.byte	131                     @ DW_AT_call_line
	.byte	80                      @ Abbrev [80] 0x3551:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc10           @ DW_AT_location
	.long	12704                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x355a:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc8            @ DW_AT_location
	.long	12716                   @ DW_AT_abstract_origin
	.byte	70                      @ Abbrev [70] 0x3563:0x6 DW_TAG_formal_parameter
	.byte	2                       @ DW_AT_const_value
	.long	12728                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x3569:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc9            @ DW_AT_location
	.long	12740                   @ DW_AT_abstract_origin
	.byte	81                      @ Abbrev [81] 0x3572:0xc DW_TAG_inlined_subroutine
	.long	12765                   @ DW_AT_abstract_origin
	.long	.Ldebug_ranges15        @ DW_AT_ranges
	.byte	2                       @ DW_AT_call_file
	.short	1425                    @ DW_AT_call_line
	.byte	0                       @ End Of Children Mark
	.byte	78                      @ Abbrev [78] 0x357f:0x13a DW_TAG_lexical_block
	.long	.Ldebug_ranges19        @ DW_AT_ranges
	.byte	77                      @ Abbrev [77] 0x3584:0xf DW_TAG_variable
	.long	.Ldebug_loc45           @ DW_AT_location
	.long	.Linfo_string536        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	222                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	78                      @ Abbrev [78] 0x3593:0x125 DW_TAG_lexical_block
	.long	.Ldebug_ranges18        @ DW_AT_ranges
	.byte	84                      @ Abbrev [84] 0x3598:0x10 DW_TAG_variable
	.long	.Ldebug_loc47           @ DW_AT_location
	.long	.Linfo_string569        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.short	274                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	77                      @ Abbrev [77] 0x35a8:0xf DW_TAG_variable
	.long	.Ldebug_loc48           @ DW_AT_location
	.long	.Linfo_string570        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	241                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	77                      @ Abbrev [77] 0x35b7:0xf DW_TAG_variable
	.long	.Ldebug_loc49           @ DW_AT_location
	.long	.Linfo_string571        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	240                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	77                      @ Abbrev [77] 0x35c6:0xf DW_TAG_variable
	.long	.Ldebug_loc50           @ DW_AT_location
	.long	.Linfo_string572        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.byte	239                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	62                      @ Abbrev [62] 0x35d5:0xc DW_TAG_variable
	.long	.Linfo_string573        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.short	267                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	84                      @ Abbrev [84] 0x35e1:0x10 DW_TAG_variable
	.long	.Ldebug_loc55           @ DW_AT_location
	.long	.Linfo_string574        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.short	268                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	62                      @ Abbrev [62] 0x35f1:0xc DW_TAG_variable
	.long	.Linfo_string575        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.short	270                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	84                      @ Abbrev [84] 0x35fd:0x10 DW_TAG_variable
	.long	.Ldebug_loc56           @ DW_AT_location
	.long	.Linfo_string576        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.short	271                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	62                      @ Abbrev [62] 0x360d:0xc DW_TAG_variable
	.long	.Linfo_string577        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.short	273                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	62                      @ Abbrev [62] 0x3619:0xc DW_TAG_variable
	.long	.Linfo_string578        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.short	276                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	84                      @ Abbrev [84] 0x3625:0x10 DW_TAG_variable
	.long	.Ldebug_loc57           @ DW_AT_location
	.long	.Linfo_string579        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.short	277                     @ DW_AT_decl_line
	.long	262                     @ DW_AT_type
	.byte	69                      @ Abbrev [69] 0x3635:0x39 DW_TAG_inlined_subroutine
	.long	12565                   @ DW_AT_abstract_origin
	.long	.Ldebug_ranges16        @ DW_AT_ranges
	.byte	1                       @ DW_AT_call_file
	.byte	253                     @ DW_AT_call_line
	.byte	80                      @ Abbrev [80] 0x3640:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc54           @ DW_AT_location
	.long	12581                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x3649:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc53           @ DW_AT_location
	.long	12592                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x3652:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc52           @ DW_AT_location
	.long	12603                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x365b:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc44           @ DW_AT_location
	.long	12614                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x3664:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc51           @ DW_AT_location
	.long	12625                   @ DW_AT_abstract_origin
	.byte	0                       @ End Of Children Mark
	.byte	79                      @ Abbrev [79] 0x366e:0xb DW_TAG_inlined_subroutine
	.long	12881                   @ DW_AT_abstract_origin
	.long	.Ldebug_ranges17        @ DW_AT_ranges
	.byte	1                       @ DW_AT_call_file
	.byte	223                     @ DW_AT_call_line
	.byte	85                      @ Abbrev [85] 0x3679:0x3e DW_TAG_inlined_subroutine
	.long	12565                   @ DW_AT_abstract_origin
	.long	.Ltmp297                @ DW_AT_low_pc
	.long	.Ltmp298-.Ltmp297       @ DW_AT_high_pc
	.byte	1                       @ DW_AT_call_file
	.short	288                     @ DW_AT_call_line
	.byte	80                      @ Abbrev [80] 0x3689:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc46           @ DW_AT_location
	.long	12581                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x3692:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc58           @ DW_AT_location
	.long	12592                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x369b:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc59           @ DW_AT_location
	.long	12603                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x36a4:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc60           @ DW_AT_location
	.long	12614                   @ DW_AT_abstract_origin
	.byte	80                      @ Abbrev [80] 0x36ad:0x9 DW_TAG_formal_parameter
	.long	.Ldebug_loc61           @ DW_AT_location
	.long	12625                   @ DW_AT_abstract_origin
	.byte	0                       @ End Of Children Mark
	.byte	0                       @ End Of Children Mark
	.byte	0                       @ End Of Children Mark
	.byte	78                      @ Abbrev [78] 0x36b9:0x22 DW_TAG_lexical_block
	.long	.Ldebug_ranges21        @ DW_AT_ranges
	.byte	84                      @ Abbrev [84] 0x36be:0x10 DW_TAG_variable
	.long	.Ldebug_loc62           @ DW_AT_location
	.long	.Linfo_string536        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.short	328                     @ DW_AT_decl_line
	.long	58                      @ DW_AT_type
	.byte	81                      @ Abbrev [81] 0x36ce:0xc DW_TAG_inlined_subroutine
	.long	12935                   @ DW_AT_abstract_origin
	.long	.Ldebug_ranges20        @ DW_AT_ranges
	.byte	1                       @ DW_AT_call_file
	.short	329                     @ DW_AT_call_line
	.byte	0                       @ End Of Children Mark
	.byte	0                       @ End Of Children Mark
	.byte	0                       @ End Of Children Mark
	.byte	86                      @ Abbrev [86] 0x36dd:0x2f DW_TAG_subprogram
	.long	.Lfunc_begin2           @ DW_AT_low_pc
	.long	.Lfunc_end2-.Lfunc_begin2 @ DW_AT_high_pc
	.byte	1                       @ DW_AT_frame_base
	.byte	93
	.long	.Linfo_string561        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.short	335                     @ DW_AT_decl_line
                                        @ DW_AT_external
	.byte	87                      @ Abbrev [87] 0x36ef:0xe DW_TAG_formal_parameter
	.byte	1                       @ DW_AT_location
	.byte	80
	.long	.Linfo_string509        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.short	335                     @ DW_AT_decl_line
	.long	11991                   @ DW_AT_type
	.byte	87                      @ Abbrev [87] 0x36fd:0xe DW_TAG_formal_parameter
	.byte	1                       @ DW_AT_location
	.byte	81
	.long	.Linfo_string562        @ DW_AT_name
	.byte	1                       @ DW_AT_decl_file
	.short	335                     @ DW_AT_decl_line
	.long	4808                    @ DW_AT_type
	.byte	0                       @ End Of Children Mark
	.byte	0                       @ End Of Children Mark
	.section	.debug_ranges,"",%progbits
.Ldebug_range:
.Ldebug_ranges0:
	.long	.Ltmp27-.Lfunc_begin0
	.long	.Ltmp28-.Lfunc_begin0
	.long	.Ltmp29-.Lfunc_begin0
	.long	.Ltmp32-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges1:
	.long	.Ltmp28-.Lfunc_begin0
	.long	.Ltmp29-.Lfunc_begin0
	.long	.Ltmp32-.Lfunc_begin0
	.long	.Ltmp37-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges2:
	.long	.Ltmp115-.Lfunc_begin0
	.long	.Ltmp116-.Lfunc_begin0
	.long	.Ltmp117-.Lfunc_begin0
	.long	.Ltmp118-.Lfunc_begin0
	.long	.Ltmp123-.Lfunc_begin0
	.long	.Ltmp124-.Lfunc_begin0
	.long	.Ltmp125-.Lfunc_begin0
	.long	.Ltmp126-.Lfunc_begin0
	.long	.Ltmp131-.Lfunc_begin0
	.long	.Ltmp132-.Lfunc_begin0
	.long	.Ltmp133-.Lfunc_begin0
	.long	.Ltmp134-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges3:
	.long	.Ltmp66-.Lfunc_begin0
	.long	.Ltmp67-.Lfunc_begin0
	.long	.Ltmp71-.Lfunc_begin0
	.long	.Ltmp72-.Lfunc_begin0
	.long	.Ltmp110-.Lfunc_begin0
	.long	.Ltmp135-.Lfunc_begin0
	.long	.Ltmp138-.Lfunc_begin0
	.long	.Ltmp139-.Lfunc_begin0
	.long	.Ltmp155-.Lfunc_begin0
	.long	.Ltmp156-.Lfunc_begin0
	.long	.Ltmp210-.Lfunc_begin0
	.long	.Ltmp211-.Lfunc_begin0
	.long	.Ltmp225-.Lfunc_begin0
	.long	.Ltmp226-.Lfunc_begin0
	.long	.Ltmp227-.Lfunc_begin0
	.long	.Ltmp228-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges4:
	.long	.Ltmp141-.Lfunc_begin0
	.long	.Ltmp143-.Lfunc_begin0
	.long	.Ltmp144-.Lfunc_begin0
	.long	.Ltmp152-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges5:
	.long	.Ltmp148-.Lfunc_begin0
	.long	.Ltmp149-.Lfunc_begin0
	.long	.Ltmp150-.Lfunc_begin0
	.long	.Ltmp151-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges6:
	.long	.Ltmp68-.Lfunc_begin0
	.long	.Ltmp69-.Lfunc_begin0
	.long	.Ltmp237-.Lfunc_begin0
	.long	.Ltmp238-.Lfunc_begin0
	.long	.Ltmp312-.Lfunc_begin0
	.long	.Ltmp313-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges7:
	.long	.Ltmp162-.Lfunc_begin0
	.long	.Ltmp163-.Lfunc_begin0
	.long	.Ltmp164-.Lfunc_begin0
	.long	.Ltmp165-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges8:
	.long	.Ltmp212-.Lfunc_begin0
	.long	.Ltmp213-.Lfunc_begin0
	.long	.Ltmp214-.Lfunc_begin0
	.long	.Ltmp215-.Lfunc_begin0
	.long	.Ltmp216-.Lfunc_begin0
	.long	.Ltmp217-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges9:
	.long	.Ltmp226-.Lfunc_begin0
	.long	.Ltmp227-.Lfunc_begin0
	.long	.Ltmp229-.Lfunc_begin0
	.long	.Ltmp230-.Lfunc_begin0
	.long	.Ltmp231-.Lfunc_begin0
	.long	.Ltmp232-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges10:
	.long	.Ltmp67-.Lfunc_begin0
	.long	.Ltmp69-.Lfunc_begin0
	.long	.Ltmp154-.Lfunc_begin0
	.long	.Ltmp155-.Lfunc_begin0
	.long	.Ltmp156-.Lfunc_begin0
	.long	.Ltmp166-.Lfunc_begin0
	.long	.Ltmp170-.Lfunc_begin0
	.long	.Ltmp175-.Lfunc_begin0
	.long	.Ltmp186-.Lfunc_begin0
	.long	.Ltmp187-.Lfunc_begin0
	.long	.Ltmp200-.Lfunc_begin0
	.long	.Ltmp201-.Lfunc_begin0
	.long	.Ltmp206-.Lfunc_begin0
	.long	.Ltmp210-.Lfunc_begin0
	.long	.Ltmp211-.Lfunc_begin0
	.long	.Ltmp225-.Lfunc_begin0
	.long	.Ltmp226-.Lfunc_begin0
	.long	.Ltmp227-.Lfunc_begin0
	.long	.Ltmp228-.Lfunc_begin0
	.long	.Ltmp239-.Lfunc_begin0
	.long	.Ltmp311-.Lfunc_begin0
	.long	.Ltmp313-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges11:
	.long	.Ltmp179-.Lfunc_begin0
	.long	.Ltmp180-.Lfunc_begin0
	.long	.Ltmp181-.Lfunc_begin0
	.long	.Ltmp182-.Lfunc_begin0
	.long	.Ltmp183-.Lfunc_begin0
	.long	.Ltmp184-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges12:
	.long	.Ltmp193-.Lfunc_begin0
	.long	.Ltmp194-.Lfunc_begin0
	.long	.Ltmp195-.Lfunc_begin0
	.long	.Ltmp196-.Lfunc_begin0
	.long	.Ltmp197-.Lfunc_begin0
	.long	.Ltmp198-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges13:
	.long	.Ltmp167-.Lfunc_begin0
	.long	.Ltmp170-.Lfunc_begin0
	.long	.Ltmp175-.Lfunc_begin0
	.long	.Ltmp186-.Lfunc_begin0
	.long	.Ltmp187-.Lfunc_begin0
	.long	.Ltmp200-.Lfunc_begin0
	.long	.Ltmp201-.Lfunc_begin0
	.long	.Ltmp206-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges14:
	.long	.Ltmp88-.Lfunc_begin0
	.long	.Ltmp89-.Lfunc_begin0
	.long	.Ltmp91-.Lfunc_begin0
	.long	.Ltmp93-.Lfunc_begin0
	.long	.Ltmp94-.Lfunc_begin0
	.long	.Ltmp102-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges15:
	.long	.Ltmp98-.Lfunc_begin0
	.long	.Ltmp99-.Lfunc_begin0
	.long	.Ltmp100-.Lfunc_begin0
	.long	.Ltmp101-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges16:
	.long	.Ltmp250-.Lfunc_begin0
	.long	.Ltmp251-.Lfunc_begin0
	.long	.Ltmp257-.Lfunc_begin0
	.long	.Ltmp258-.Lfunc_begin0
	.long	.Ltmp276-.Lfunc_begin0
	.long	.Ltmp277-.Lfunc_begin0
	.long	.Ltmp278-.Lfunc_begin0
	.long	.Ltmp279-.Lfunc_begin0
	.long	.Ltmp280-.Lfunc_begin0
	.long	.Ltmp281-.Lfunc_begin0
	.long	.Ltmp290-.Lfunc_begin0
	.long	.Ltmp291-.Lfunc_begin0
	.long	.Ltmp295-.Lfunc_begin0
	.long	.Ltmp296-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges17:
	.long	.Ltmp265-.Lfunc_begin0
	.long	.Ltmp266-.Lfunc_begin0
	.long	.Ltmp267-.Lfunc_begin0
	.long	.Ltmp268-.Lfunc_begin0
	.long	.Ltmp269-.Lfunc_begin0
	.long	.Ltmp270-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges18:
	.long	.Ltmp245-.Lfunc_begin0
	.long	.Ltmp246-.Lfunc_begin0
	.long	.Ltmp247-.Lfunc_begin0
	.long	.Ltmp248-.Lfunc_begin0
	.long	.Ltmp249-.Lfunc_begin0
	.long	.Ltmp251-.Lfunc_begin0
	.long	.Ltmp253-.Lfunc_begin0
	.long	.Ltmp254-.Lfunc_begin0
	.long	.Ltmp255-.Lfunc_begin0
	.long	.Ltmp256-.Lfunc_begin0
	.long	.Ltmp257-.Lfunc_begin0
	.long	.Ltmp259-.Lfunc_begin0
	.long	.Ltmp260-.Lfunc_begin0
	.long	.Ltmp261-.Lfunc_begin0
	.long	.Ltmp263-.Lfunc_begin0
	.long	.Ltmp271-.Lfunc_begin0
	.long	.Ltmp272-.Lfunc_begin0
	.long	.Ltmp273-.Lfunc_begin0
	.long	.Ltmp274-.Lfunc_begin0
	.long	.Ltmp296-.Lfunc_begin0
	.long	.Ltmp297-.Lfunc_begin0
	.long	.Ltmp301-.Lfunc_begin0
	.long	.Ltmp302-.Lfunc_begin0
	.long	.Ltmp303-.Lfunc_begin0
	.long	.Ltmp304-.Lfunc_begin0
	.long	.Ltmp305-.Lfunc_begin0
	.long	.Ltmp306-.Lfunc_begin0
	.long	.Ltmp307-.Lfunc_begin0
	.long	.Ltmp308-.Lfunc_begin0
	.long	.Ltmp309-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges19:
	.long	.Ltmp244-.Lfunc_begin0
	.long	.Ltmp246-.Lfunc_begin0
	.long	.Ltmp247-.Lfunc_begin0
	.long	.Ltmp248-.Lfunc_begin0
	.long	.Ltmp249-.Lfunc_begin0
	.long	.Ltmp251-.Lfunc_begin0
	.long	.Ltmp253-.Lfunc_begin0
	.long	.Ltmp254-.Lfunc_begin0
	.long	.Ltmp255-.Lfunc_begin0
	.long	.Ltmp259-.Lfunc_begin0
	.long	.Ltmp260-.Lfunc_begin0
	.long	.Ltmp271-.Lfunc_begin0
	.long	.Ltmp272-.Lfunc_begin0
	.long	.Ltmp273-.Lfunc_begin0
	.long	.Ltmp274-.Lfunc_begin0
	.long	.Ltmp296-.Lfunc_begin0
	.long	.Ltmp297-.Lfunc_begin0
	.long	.Ltmp301-.Lfunc_begin0
	.long	.Ltmp302-.Lfunc_begin0
	.long	.Ltmp309-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges20:
	.long	.Ltmp338-.Lfunc_begin0
	.long	.Ltmp339-.Lfunc_begin0
	.long	.Ltmp341-.Lfunc_begin0
	.long	.Ltmp342-.Lfunc_begin0
	.long	0
	.long	0
.Ldebug_ranges21:
	.long	.Ltmp329-.Lfunc_begin0
	.long	.Ltmp330-.Lfunc_begin0
	.long	.Ltmp331-.Lfunc_begin0
	.long	.Ltmp332-.Lfunc_begin0
	.long	.Ltmp333-.Lfunc_begin0
	.long	.Ltmp334-.Lfunc_begin0
	.long	.Ltmp335-.Lfunc_begin0
	.long	.Ltmp344-.Lfunc_begin0
	.long	0
	.long	0
	.section	.debug_macinfo,"",%progbits
.Ldebug_macinfo:
.Lcu_macro_begin0:
	.byte	0                       @ End Of Macro List Mark
	.section	.debug_pubnames,"",%progbits
	.long	.LpubNames_end0-.LpubNames_begin0 @ Length of Public Names Info
.LpubNames_begin0:
	.short	2                       @ DWARF Version
	.long	.Lcu_begin0             @ Offset of Compilation Unit Info
	.long	14093                   @ Compilation Unit Length
	.long	399                     @ DIE offset
	.asciz	"delay_s"               @ External Name
	.long	689                     @ DIE offset
	.asciz	"lfo"                   @ External Name
	.long	1306                    @ DIE offset
	.asciz	"std::__debug"          @ External Name
	.long	1066                    @ DIE offset
	.asciz	"phaser_w1"             @ External Name
	.long	1086                    @ DIE offset
	.asciz	"phaser_w2"             @ External Name
	.long	1106                    @ DIE offset
	.asciz	"phaser_w3"             @ External Name
	.long	1126                    @ DIE offset
	.asciz	"phaser_w4"             @ External Name
	.long	379                     @ DIE offset
	.asciz	"delay_write_ptr"       @ External Name
	.long	419                     @ DIE offset
	.asciz	"delay_desired"         @ External Name
	.long	1046                    @ DIE offset
	.asciz	"tremolo_mix"           @ External Name
	.long	5366                    @ DIE offset
	.asciz	"std"                   @ External Name
	.long	749                     @ DIE offset
	.asciz	"lfo_frequency_vib"     @ External Name
	.long	906                     @ DIE offset
	.asciz	"log_frequency"         @ External Name
	.long	619                     @ DIE offset
	.asciz	"lpf_in_n1"             @ External Name
	.long	104                     @ DIE offset
	.asciz	"debouncer"             @ External Name
	.long	599                     @ DIE offset
	.asciz	"lpf_in"                @ External Name
	.long	659                     @ DIE offset
	.asciz	"sinetable"             @ External Name
	.long	11917                   @ DIE offset
	.asciz	"pinMode"               @ External Name
	.long	124                     @ DIE offset
	.asciz	"on_state"              @ External Name
	.long	164                     @ DIE offset
	.asciz	"shift"                 @ External Name
	.long	12565                   @ DIE offset
	.asciz	"map"                   @ External Name
	.long	12637                   @ DIE offset
	.asciz	"digitalRead"           @ External Name
	.long	1995                    @ DIE offset
	.asciz	"std::__exception_ptr"  @ External Name
	.long	886                     @ DIE offset
	.asciz	"log_lfo"               @ External Name
	.long	12691                   @ DIE offset
	.asciz	"analogWrite"           @ External Name
	.long	12765                   @ DIE offset
	.asciz	"analogWriteOnce"       @ External Name
	.long	12997                   @ DIE offset
	.asciz	"render"                @ External Name
	.long	14045                   @ DIE offset
	.asciz	"cleanup"               @ External Name
	.long	38                      @ DIE offset
	.asciz	"device_on"             @ External Name
	.long	499                     @ DIE offset
	.asciz	"drywetmix"             @ External Name
	.long	12881                   @ DIE offset
	.asciz	"audioRead"             @ External Name
	.long	12388                   @ DIE offset
	.asciz	"setup"                 @ External Name
	.long	709                     @ DIE offset
	.asciz	"lfo_frequency"         @ External Name
	.long	769                     @ DIE offset
	.asciz	"lfo_ptr_vib"           @ External Name
	.long	1246                    @ DIE offset
	.asciz	"phaser_depth"          @ External Name
	.long	301                     @ DIE offset
	.asciz	"out"                   @ External Name
	.long	204                     @ DIE offset
	.asciz	"shift_prev_state"      @ External Name
	.long	6352                    @ DIE offset
	.asciz	"__gnu_cxx"             @ External Name
	.long	184                     @ DIE offset
	.asciz	"shift_state"           @ External Name
	.long	479                     @ DIE offset
	.asciz	"feedback_gain"         @ External Name
	.long	559                     @ DIE offset
	.asciz	"delay_in"              @ External Name
	.long	65                      @ DIE offset
	.asciz	"lock"                  @ External Name
	.long	869                     @ DIE offset
	.asciz	"logtable"              @ External Name
	.long	849                     @ DIE offset
	.asciz	"lfo_dif"               @ External Name
	.long	439                     @ DIE offset
	.asciz	"timeAlpha"             @ External Name
	.long	729                     @ DIE offset
	.asciz	"lfo_ptr"               @ External Name
	.long	639                     @ DIE offset
	.asciz	"lpf_out_n1"            @ External Name
	.long	789                     @ DIE offset
	.asciz	"vib_depth"             @ External Name
	.long	946                     @ DIE offset
	.asciz	"log_ptr_direction"     @ External Name
	.long	519                     @ DIE offset
	.asciz	"drymix"                @ External Name
	.long	809                     @ DIE offset
	.asciz	"lfo_n0"                @ External Name
	.long	359                     @ DIE offset
	.asciz	"delay_read_ptr"        @ External Name
	.long	829                     @ DIE offset
	.asciz	"lfo_n1"                @ External Name
	.long	986                     @ DIE offset
	.asciz	"log_n1"                @ External Name
	.long	966                     @ DIE offset
	.asciz	"log_n0"                @ External Name
	.long	1026                    @ DIE offset
	.asciz	"tremolo"               @ External Name
	.long	1206                    @ DIE offset
	.asciz	"apf3y_n1"              @ External Name
	.long	1166                    @ DIE offset
	.asciz	"apf1y_n1"              @ External Name
	.long	1186                    @ DIE offset
	.asciz	"apf2y_n1"              @ External Name
	.long	12935                   @ DIE offset
	.asciz	"audioWrite"            @ External Name
	.long	1226                    @ DIE offset
	.asciz	"apf4y_n1"              @ External Name
	.long	321                     @ DIE offset
	.asciz	"delay_buf"             @ External Name
	.long	459                     @ DIE offset
	.asciz	"delay_desired_vib"     @ External Name
	.long	1284                    @ DIE offset
	.asciz	"__gnu_debug"           @ External Name
	.long	12827                   @ DIE offset
	.asciz	"analogRead"            @ External Name
	.long	1006                    @ DIE offset
	.asciz	"log_dif"               @ External Name
	.long	269                     @ DIE offset
	.asciz	"in"                    @ External Name
	.long	926                     @ DIE offset
	.asciz	"log_ptr"               @ External Name
	.long	144                     @ DIE offset
	.asciz	"on_prev_state"         @ External Name
	.long	539                     @ DIE offset
	.asciz	"eta"                   @ External Name
	.long	579                     @ DIE offset
	.asciz	"lpf_out"               @ External Name
	.long	1146                    @ DIE offset
	.asciz	"apf1x_n1"              @ External Name
	.long	224                     @ DIE offset
	.asciz	"saved_readings"        @ External Name
	.long	0                       @ End Mark
.LpubNames_end0:
	.section	.debug_pubtypes,"",%progbits
	.long	.LpubTypes_end0-.LpubTypes_begin0 @ Length of Public Types Info
.LpubTypes_begin0:
	.short	2                       @ DWARF Version
	.long	.Lcu_begin0             @ Offset of Compilation Unit Info
	.long	14093                   @ Compilation Unit Length
	.long	6643                    @ DIE offset
	.asciz	"int32_t"               @ External Name
	.long	6665                    @ DIE offset
	.asciz	"int_fast8_t"           @ External Name
	.long	6753                    @ DIE offset
	.asciz	"intmax_t"              @ External Name
	.long	2310                    @ DIE offset
	.asciz	"std::nullptr_t"        @ External Name
	.long	6837                    @ DIE offset
	.asciz	"uint_fast16_t"         @ External Name
	.long	6775                    @ DIE offset
	.asciz	"uint8_t"               @ External Name
	.long	4723                    @ DIE offset
	.asciz	"__off_t"               @ External Name
	.long	6971                    @ DIE offset
	.asciz	"bool"                  @ External Name
	.long	2350                    @ DIE offset
	.asciz	"std::size_t"           @ External Name
	.long	8216                    @ DIE offset
	.asciz	"fpos_t"                @ External Name
	.long	6676                    @ DIE offset
	.asciz	"int_fast16_t"          @ External Name
	.long	4262                    @ DIE offset
	.asciz	"wint_t"                @ External Name
	.long	6032                    @ DIE offset
	.asciz	"long unsigned int"     @ External Name
	.long	6793                    @ DIE offset
	.asciz	"uint16_t"              @ External Name
	.long	4734                    @ DIE offset
	.asciz	"long int"              @ External Name
	.long	6731                    @ DIE offset
	.asciz	"int_least32_t"         @ External Name
	.long	6956                    @ DIE offset
	.asciz	"decltype(nullptr)"     @ External Name
	.long	8205                    @ DIE offset
	.asciz	"FILE"                  @ External Name
	.long	7485                    @ DIE offset
	.asciz	"__compar_fn_t"         @ External Name
	.long	6654                    @ DIE offset
	.asciz	"int64_t"               @ External Name
	.long	6925                    @ DIE offset
	.asciz	"uintptr_t"             @ External Name
	.long	9767                    @ DIE offset
	.asciz	"float_t"               @ External Name
	.long	6544                    @ DIE offset
	.asciz	"long double"           @ External Name
	.long	7995                    @ DIE offset
	.asciz	"lldiv_t"               @ External Name
	.long	6636                    @ DIE offset
	.asciz	"short"                 @ External Name
	.long	6892                    @ DIE offset
	.asciz	"uint_least32_t"        @ External Name
	.long	6764                    @ DIE offset
	.asciz	"intptr_t"              @ External Name
	.long	4325                    @ DIE offset
	.asciz	"_IO_FILE"              @ External Name
	.long	11996                   @ DIE offset
	.asciz	"BelaContext"           @ External Name
	.long	5346                    @ DIE offset
	.asciz	"__gnuc_va_list"        @ External Name
	.long	4314                    @ DIE offset
	.asciz	"__FILE"                @ External Name
	.long	6742                    @ DIE offset
	.asciz	"int_least64_t"         @ External Name
	.long	4167                    @ DIE offset
	.asciz	"__mbstate_t"           @ External Name
	.long	2361                    @ DIE offset
	.asciz	"std::ptrdiff_t"        @ External Name
	.long	7283                    @ DIE offset
	.asciz	"ldiv_t"                @ External Name
	.long	6625                    @ DIE offset
	.asciz	"int16_t"               @ External Name
	.long	5357                    @ DIE offset
	.asciz	"__builtin_va_list"     @ External Name
	.long	4236                    @ DIE offset
	.asciz	"unsigned int"          @ External Name
	.long	4865                    @ DIE offset
	.asciz	"wchar_t"               @ External Name
	.long	58                      @ DIE offset
	.asciz	"int"                   @ External Name
	.long	4809                    @ DIE offset
	.asciz	"size_t"                @ External Name
	.long	6848                    @ DIE offset
	.asciz	"uint_fast32_t"         @ External Name
	.long	6687                    @ DIE offset
	.asciz	"int_fast32_t"          @ External Name
	.long	9756                    @ DIE offset
	.asciz	"double_t"              @ External Name
	.long	4772                    @ DIE offset
	.asciz	"_IO_lock_t"            @ External Name
	.long	6903                    @ DIE offset
	.asciz	"uint_least64_t"        @ External Name
	.long	6614                    @ DIE offset
	.asciz	"int8_t"                @ External Name
	.long	7270                    @ DIE offset
	.asciz	"div_t"                 @ External Name
	.long	4748                    @ DIE offset
	.asciz	"signed char"           @ External Name
	.long	6804                    @ DIE offset
	.asciz	"uint32_t"              @ External Name
	.long	2002                    @ DIE offset
	.asciz	"std::__exception_ptr::exception_ptr" @ External Name
	.long	5908                    @ DIE offset
	.asciz	"double"                @ External Name
	.long	6720                    @ DIE offset
	.asciz	"int_least16_t"         @ External Name
	.long	4255                    @ DIE offset
	.asciz	"char"                  @ External Name
	.long	6826                    @ DIE offset
	.asciz	"uint_fast8_t"          @ External Name
	.long	4156                    @ DIE offset
	.asciz	"mbstate_t"             @ External Name
	.long	6870                    @ DIE offset
	.asciz	"uint_least8_t"         @ External Name
	.long	6607                    @ DIE offset
	.asciz	"long long unsigned int" @ External Name
	.long	6859                    @ DIE offset
	.asciz	"uint_fast64_t"         @ External Name
	.long	4801                    @ DIE offset
	.asciz	"long long int"         @ External Name
	.long	4741                    @ DIE offset
	.asciz	"unsigned short"        @ External Name
	.long	6709                    @ DIE offset
	.asciz	"int_least8_t"          @ External Name
	.long	5371                    @ DIE offset
	.asciz	"std::__va_list"        @ External Name
	.long	8227                    @ DIE offset
	.asciz	"_G_fpos_t"             @ External Name
	.long	6881                    @ DIE offset
	.asciz	"uint_least16_t"        @ External Name
	.long	6698                    @ DIE offset
	.asciz	"int_fast64_t"          @ External Name
	.long	262                     @ DIE offset
	.asciz	"float"                 @ External Name
	.long	4779                    @ DIE offset
	.asciz	"__off64_t"             @ External Name
	.long	6914                    @ DIE offset
	.asciz	"uintmax_t"             @ External Name
	.long	6786                    @ DIE offset
	.asciz	"unsigned char"         @ External Name
	.long	4790                    @ DIE offset
	.asciz	"__quad_t"              @ External Name
	.long	6815                    @ DIE offset
	.asciz	"uint64_t"              @ External Name
	.long	0                       @ End Mark
.LpubTypes_end0:
	.cfi_sections .debug_frame

	.globl	device_on
device_on = .L_MergedGlobals
	.size	device_on, 4
	.globl	debouncer
debouncer = .L_MergedGlobals+4
	.size	debouncer, 4
	.globl	on_state
on_state = .L_MergedGlobals+8
	.size	on_state, 4
	.globl	on_prev_state
on_prev_state = .L_MergedGlobals+12
	.size	on_prev_state, 4
	.globl	shift
shift = .L_MergedGlobals+16
	.size	shift, 4
	.globl	shift_state
shift_state = .L_MergedGlobals+20
	.size	shift_state, 4
	.globl	shift_prev_state
shift_prev_state = .L_MergedGlobals+24
	.size	shift_prev_state, 4
	.globl	delay_read_ptr
delay_read_ptr = .L_MergedGlobals+28
	.size	delay_read_ptr, 4
	.globl	delay_write_ptr
delay_write_ptr = .L_MergedGlobals+32
	.size	delay_write_ptr, 4
	.globl	delay_s
delay_s = .L_MergedGlobals+36
	.size	delay_s, 4
	.globl	delay_desired
delay_desired = .L_MergedGlobals+40
	.size	delay_desired, 4
	.globl	timeAlpha
timeAlpha = .L_MergedGlobals+44
	.size	timeAlpha, 4
	.globl	delay_desired_vib
delay_desired_vib = .L_MergedGlobals+48
	.size	delay_desired_vib, 4
	.globl	feedback_gain
feedback_gain = .L_MergedGlobals+52
	.size	feedback_gain, 4
	.globl	drywetmix
drywetmix = .L_MergedGlobals+56
	.size	drywetmix, 4
	.globl	drymix
drymix = .L_MergedGlobals+60
	.size	drymix, 4
	.globl	eta
eta = .L_MergedGlobals+64
	.size	eta, 4
	.globl	lfo
lfo = .L_MergedGlobals+68
	.size	lfo, 4
	.globl	lfo_frequency
lfo_frequency = .L_MergedGlobals+72
	.size	lfo_frequency, 4
	.globl	lfo_ptr
lfo_ptr = .L_MergedGlobals+76
	.size	lfo_ptr, 4
	.globl	lfo_frequency_vib
lfo_frequency_vib = .L_MergedGlobals+80
	.size	lfo_frequency_vib, 4
	.globl	lfo_ptr_vib
lfo_ptr_vib = .L_MergedGlobals+84
	.size	lfo_ptr_vib, 4
	.globl	vib_depth
vib_depth = .L_MergedGlobals+88
	.size	vib_depth, 4
	.globl	lfo_n0
lfo_n0 = .L_MergedGlobals+92
	.size	lfo_n0, 4
	.globl	lfo_n1
lfo_n1 = .L_MergedGlobals+96
	.size	lfo_n1, 4
	.globl	lfo_dif
lfo_dif = .L_MergedGlobals+100
	.size	lfo_dif, 4
	.globl	log_lfo
log_lfo = .L_MergedGlobals+104
	.size	log_lfo, 4
	.globl	log_frequency
log_frequency = .L_MergedGlobals+108
	.size	log_frequency, 4
	.globl	log_ptr
log_ptr = .L_MergedGlobals+112
	.size	log_ptr, 4
	.globl	log_ptr_direction
log_ptr_direction = .L_MergedGlobals+116
	.size	log_ptr_direction, 4
	.globl	log_n0
log_n0 = .L_MergedGlobals+120
	.size	log_n0, 4
	.globl	log_n1
log_n1 = .L_MergedGlobals.6
	.size	log_n1, 4
	.globl	log_dif
log_dif = .L_MergedGlobals.6+4
	.size	log_dif, 4
	.globl	tremolo
tremolo = .L_MergedGlobals.6+8
	.size	tremolo, 4
	.globl	tremolo_mix
tremolo_mix = .L_MergedGlobals.6+12
	.size	tremolo_mix, 4
	.globl	phaser_w1
phaser_w1 = .L_MergedGlobals.6+16
	.size	phaser_w1, 4
	.globl	phaser_w2
phaser_w2 = .L_MergedGlobals.6+20
	.size	phaser_w2, 4
	.globl	phaser_w3
phaser_w3 = .L_MergedGlobals.6+24
	.size	phaser_w3, 4
	.globl	phaser_w4
phaser_w4 = .L_MergedGlobals.6+28
	.size	phaser_w4, 4
	.globl	apf1x_n1
apf1x_n1 = .L_MergedGlobals.6+32
	.size	apf1x_n1, 4
	.globl	apf1y_n1
apf1y_n1 = .L_MergedGlobals.6+36
	.size	apf1y_n1, 4
	.globl	apf2y_n1
apf2y_n1 = .L_MergedGlobals.6+40
	.size	apf2y_n1, 4
	.globl	apf3y_n1
apf3y_n1 = .L_MergedGlobals.6+44
	.size	apf3y_n1, 4
	.globl	apf4y_n1
apf4y_n1 = .L_MergedGlobals.6+48
	.size	apf4y_n1, 4
	.globl	phaser_depth
phaser_depth = .L_MergedGlobals.6+52
	.size	phaser_depth, 4
	.globl	in
in = .L_MergedGlobals.6+56
	.size	in, 8
	.globl	out
out = .L_MergedGlobals.6+64
	.size	out, 8
	.globl	delay_in
delay_in = .L_MergedGlobals.6+72
	.size	delay_in, 8
	.globl	lpf_out
lpf_out = .L_MergedGlobals.6+80
	.size	lpf_out, 8
	.globl	lpf_in
lpf_in = .L_MergedGlobals.6+88
	.size	lpf_in, 8
	.globl	lpf_in_n1
lpf_in_n1 = .L_MergedGlobals.6+96
	.size	lpf_in_n1, 8
	.globl	lpf_out_n1
lpf_out_n1 = .L_MergedGlobals.6+104
	.size	lpf_out_n1, 8
	.globl	lock
lock = .L_MergedGlobals.6+112
	.size	lock, 12
	.globl	saved_readings
saved_readings = .L_MergedGlobals.7
	.size	saved_readings, 24
	.ident	"clang version 3.9.1-9 (tags/RELEASE_391/rc2)"
	.section	".note.GNU-stack","",%progbits
	.eabi_attribute	30, 2	@ Tag_ABI_optimization_goals
	.section	.debug_line,"",%progbits
.Lline_table_start0:
